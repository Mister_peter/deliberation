<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('login');
    return redirect('login');;
});

    Route::get('login', 'LoginController@index');
	Route::post('login', 'LoginController@connexion');
	Route::get('logout', 'LoginController@logout');
    Route::get('dashboard', 'MainController@dashboard');
    Route::get('statistique', 'MainController@statistique');
    Route::get('matiere', 'MainController@matiere');
    Route::post('matiere', 'MainController@showmatiere');

    Route::get('user', 'MainController@liste_user');
    Route::post('user', 'MainController@add_user');
    Route::post('get_user/{id}', 'MainController@get_user');
    Route::post('delete_user/{id}', 'MainController@delete_user');

    Route::get('etudiant', 'MainController@etudiant');
    Route::post('etudiant', 'MainController@showetudiant');
    Route::get('note_devoir', 'MainController@note_devoir');
    Route::get('note_examen', 'MainController@note_examen');
    Route::get('note_finale', 'MainController@note_finale');
    Route::get('note', 'MainController@note_finale2');
    Route::get('global', 'MainController@global');
    Route::get('reset_global', 'MainController@reset_moy_global');
    Route::get('delib_global', 'MainController@delib_global');
    Route::get('delib_auto', 'MainController@deliberation_automatique');

    Route::get('pv_final', 'MainController@pv_final');
    Route::get('show_pv_final', 'MainController@show_pv_final');

    Route::get('show_note', 'MainController@showNoteEcue'); //ancien traitement
    Route::get('show_note_examen', 'MainController@showNoteExamen'); //ancien

    Route::get('show_note_finale', 'MainController@showNoteFinale');
    Route::get('show_notes', 'MainController@showNoteFinale2');
    Route::post('check_presence', 'MainController@check_presence');
    Route::post('validate_presence', 'MainController@validate_presence');

    Route::post('bonus_devoir', 'MainController@bonus_devoir');
    Route::post('bonus_examen', 'MainController@bonus_examen');
    Route::post('delete_examen', 'MainController@delete_examen');

    Route::post('option_note', 'MainController@set_option_note');
    Route::post('option_annuler', 'MainController@reset_option_note');
    Route::post('get_note/{id}', 'MainController@getIdNote');
    Route::post('get_note_devoir/{id}/{ids}', 'MainController@getIdNote_devoir');
    Route::post('get_note_finale/{id}', 'MainController@getIdNoteFinale');
    Route::post('update_note', 'MainController@update_note');
    Route::post('update_note_examen', 'MainController@update_note_examen');
    Route::post('update_user_coef', 'MainController@update_user_coef');

    Route::post('update_all_mga', 'MainController@update_all_mga');
    Route::post('reset_all_mga', 'MainController@reset_all_mga');
    Route::post('update_mga', 'MainController@update_mga');

    Route::post('processus_devoir', 'MainController@processus_note_devoir');
    Route::post('processus_global', 'MainController@processus_global');
    Route::post('reset_processus_devoir', 'MainController@reset_processus_devoir');
    Route::post('reset_global', 'MainController@reset_global');

    Route::post('processus_examen', 'MainController@processus_note_examen');
    Route::post('reset_processus_examen', 'MainController@reset_processus_examen');

    Route::post('valider_pv', 'MainController@valider_pv');
    Route::post('reset_pv', 'MainController@reset_pv');

    //validation definitive de la deliberation
    Route::get('validation', 'MainController@validation');
    Route::post('valider', 'MainController@validation_deliberation');

    //envoi des notes vers la scolarité
    Route::post('envoyer', 'MainController@send_note_finale');
    //Calcule de la note en UE
    Route::post('calcul_ue', 'MainController@calcul_moy_ue');
    //Calcule de la moyenne semestrielle
    Route::post('calcul_semestrielle', 'MainController@calcul_moy_semestrielle');
    //Calcule de la moyenne annuelle
    Route::post('calcul_annuel', 'MainController@calcul_moy_annuelle');
    //Calcule de la decision
    Route::post('get_decision', 'MainController@get_decision');

    Route::post('synchronize', 'SynchroController@licence_septembre'); //RENTREE DE SPETEMBRE

    Route::get('synchro', 'SynchroController@index_licence');
    Route::get('synchros', 'SynchronisationController@index_licences');
    Route::get('septembre', 'SynchroController@septembre');
    // Route::get('synchro_l2', 'SynchroController@index_l1');
    // Route::get('synchro_l3', 'SynchroController@index_l3');

    //LICENCE
	Route::post('synchro_process1_Licence', 'SynchroController@process1_licence');
	Route::post('synchro_process2_Licence', 'SynchroController@process2_licence');
	Route::post('synchro_process3_Licence', 'SynchroController@process3_licence');

    Route::post('fusion1_Licence', 'SynchroController@load_table_note'); //session 1
    Route::post('fusion2_Licence', 'SynchroController@load_table_note2'); //session 2

    //Plateformes semestrielles
    Route::post('synchro_process1_Licences', 'SynchronisationController@process1_licences');
	Route::post('synchro_process2_Licences', 'SynchronisationController@process2_licences');
	Route::post('synchro_process3_Licences', 'SynchronisationController@process3_licences');
    Route::post('fusion_Licences', 'SynchronisationController@load_table_note_new'); //plateforme semestrielle

    //CRON EXECUTION
    Route::get('synchro_cron_process1', 'CronController@cron_process1');
    Route::get('synchro_cron_process2', 'CronController@cron_process2');
    Route::get('synchro_cron_process3', 'CronController@cron_process3');

	// Route::post('synchro_licence2', 'SynchroController@licence2');
	// Route::post('synchro_licence3', 'SynchroController@licence3');


Route::post('get_niveau', 'CheckController@getNiveau');
Route::post('getMatiere', 'CheckController@getMatiere');
Route::post('getMatieres', 'CheckController@getMatieres'); //pour les matieres avec  code sans le champ select
Route::post('getSemestre', 'CheckController@getSemestre');
Route::post('getTableau', 'CheckController@getTableau');

Route::post('get_note_ajax', 'CheckController@getNote');
Route::get('update_coef', 'MainController@update_coef');
Route::get('update_coeff', 'SynchroController@update_coef');
Route::get('load_table_note/{n}', 'SynchroController@load_table_note');

Route::get('write_ecue', 'SynchroController@write_ecue');
Route::get('fill_miror', 'SynchronisationController@fill_miror_new');

//Route::get('{any}', 'MainController@index');




