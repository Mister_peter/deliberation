<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nano;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SynchronisationController extends Controller
{
    protected $nano;

	public function __construct()
	{
        if(!Session::get('user_id')) return redirect('login')->send();

		$this->nano = new Nano;
	}

    public function index_licences()
    {
        $data["specialite"] = DB::table('parcours')->where('code_niveau', session('niveau'))->get();

        $data["etudiant"] = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        $data["temp"] = DB::table('delib_devoir_temp')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["devoir"] = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["examen"] = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["fusion"] = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        //Affichage la vue
        return view('synchro_semestre', $data);
    }

    /*********************** PLATEFORME SEMESTRIELLE ********************* */
    //recuperation des etudiants et cours
    public function process1_licences()
    {
        set_time_limit(0);

        $session_var = session('session_compo');
        $niveau_lien = str_replace(' ', '',  strtolower(session('niveau')));
        $sem_lien = strtolower(session('semestre'));


        //LES ETUDIANTS A RECUPERER
        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //on vide la table delib_etudiant
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
            $etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto
                                            FROM note_ecue WHERE niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" '));

            foreach($etudiant as $e)
            {
                //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
                //recuperer tout le contenu du webservice
                $result = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/get_student/'.session('rentree').'/'.session('semestre').'/'.$e->id_auto, false);
                $json   = json_decode($result, true);

                //boucle des données avec la clé (etudiants) du resultat_json
                if(!empty($json['etudiants']))
                {
                    $nb_etudiant = 0;
                    foreach($json['etudiants'] as $d)
                    {
                        //var_dump($d['id_cours']);
                        $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                        'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                        'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                        'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                        //enregistrement des données dans la table matiere
                        $this->nano->saveData('delib_etudiant', $datas);
                        $nb_etudiant++;
                    }
                }
                else
                {
                    $nb_etudiant = 0;
                }
            }
        }
        else
        {
            $nb_etudiant=0;
        }


        //LES MATIERES(ECUE) ACTIVES SUR CAMPUS
        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //Recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        $semestre = $d['semestre'];
                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);
                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }
        }
        else
        {
            $nb_cours=0;
        }

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_etudiant')
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))
                    ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        if(!empty($info))
        {
            echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>$nb_cours, "info"=> "Synchronisé le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
        }
        else
        {
            echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>$nb_cours, "info"=> "Aucune ligne synchronisée."));
        }
    }

    //recuperation des notes de devoirs et compo
    public function process2_licences()
    {
        set_time_limit(0);

        $session_var = session('session_compo');
        $sem_lien    = strtolower(session('semestre'));
        $specialite = $_POST['specialite'];

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            if($specialite == "TOUS")
            {
                //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
                //on vide la table
                $this->nano->deleteDatas4_("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");
                $this->nano->deleteDatas4_("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");

                //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
                $sql = DB::table('delib_matiere')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var, false);
                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                //on verifie si l'inscription pedagogique  existe
                                $pedagogie = DB::selectOne('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                                FROM note_ecue WHERE id_auto="'.$d['id_auto'].'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                                                AND ue_valide=0 AND reference_ecue="'.$value->reference.'" ');
                                if(!empty($pedagogie))
                                {
                                    // on check pour voir si le libelle commence par D ou S
                                    if(substr(trim($d['num_devoir']),0,1) == "D")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                    }
                                    elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                    }

                                    if($devoir !=  "SESSION 2")
                                    {
                                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'), "reference"=> $value->reference,
                                                        'id_auto'=> $d['id_auto'], 'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                        //enregistrement des données dans la table note
                                        $save = $this->nano->saveData('delib_devoir_temp', $datas);

                                        if($save == 1)
                                        {
                                            $etat = true;
                                            $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                        }
                                    }
                                }
                                else
                                {
                                    $etat = 2;
                                    $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                }
                            }
                        }
                    }
                }

                //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
                //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
                $this->fill_miror_new();
            }
            else
            {
                //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE et PAR SPECIALITE
                //on vide la table selon la specialite
                $this->nano->deleteDatas5_("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);
                $this->nano->deleteDatas5_("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);

                //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
                $sql = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('specialite', $specialite)
                            ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var.'/'.$specialite, false);
                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                $pedagogie = DB::selectOne('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                            FROM note_ecue WHERE id_auto="'.$d['id_auto'].'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                                            AND ue_valide=0 AND code_specialite="'.$specialite.'" AND reference_ecue="'.$value->reference.'" ');

                                if(!empty($pedagogie))
                                {
                                    // on check pour voir si le libelle commence par D ou S
                                    if(substr(trim($d['num_devoir']),0,1) == "D")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                    }
                                    elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                    }

                                    if($devoir !=  "SESSION 2" && $specialite == $d['specialite'])
                                    {
                                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'), "reference"=> $value->reference,
                                                            'id_auto'=> $d['id_auto'], 'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                        //enregistrement des données dans la table note
                                        $save = $this->nano->saveData('delib_devoir_temp', $datas);

                                        if($save == 1)
                                        {
                                            $etat = true;
                                            $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                        }
                                    }
                                }
                                else
                                {
                                    $etat = 2;
                                    $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                }
                            }
                        }
                    }
                }

                //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
                //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
                $this->fill_miror_new2($specialite);
            }
        }
        else //POUR LA SESSION 2
        {
            if($specialite == "TOUS")
            {
                //on vide les tables
                $this->nano->deleteDatas4("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");
                $this->nano->deleteDatas4("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");

                //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
                $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var, false);
                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                //on verifie si l'inscription pedagogique  existe
                                $pedagogie = DB::selectOne('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                            FROM note_ecue WHERE id_auto="'.$d['id_auto'].'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                            AND ue_valide=0 AND reference_ecue="'.$value->reference.'" AND moyenne<10 ');

                                if(!empty($pedagogie))
                                {
                                    // on check pour voir si le libelle commence par D ou S
                                    if(substr(trim($d['num_devoir']),0,1) == "D")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                    }
                                    elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                    }

                                    if($devoir ==  "SESSION 2")
                                    {
                                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'), "reference"=> $value->reference,
                                                        'id_auto'=> $d['id_auto'], 'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                        //enregistrement des données dans la table matiere
                                        $this->nano->saveData('delib_devoir_temp', $datas);
                                        $save = $this->nano->saveData('delib_devoir', $datas);

                                        if($save == 1)
                                        {
                                            $etat = true;
                                            $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                        }
                                    }
                                }
                                else
                                {
                                    $etat = 2;
                                    $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //on vide les tables
                $this->nano->deleteDatas5("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);
                $this->nano->deleteDatas5("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);

                //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
                $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('specialite', $specialite)
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var.'/'.$specialite, false);
                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                //on verifie si l'inscription pedagogique  existe
                                $pedagogie = DB::selectOne('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                            FROM note_ecue WHERE id_auto="'.$d['id_auto'].'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                                            AND ue_valide=0 AND code_specialite="'.$specialite.'" AND reference_ecue="'.$value->reference.'" AND moyenne<10 ');

                                if(!empty($pedagogie))
                                {
                                    // on check pour voir si le libelle commence par D ou S
                                    if(substr(trim($d['num_devoir']),0,1) == "D")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                    }
                                    elseif(substr(trim($d['num_devoir']), 0, 1) == "S")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                    }

                                    if($devoir ==  "SESSION 2" && $specialite == $d['specialite'] )
                                    {
                                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'), "reference"=> $value->reference,
                                                        'id_auto'=> $d['id_auto'], 'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                        //enregistrement des données dans la table matiere
                                        $this->nano->saveData('delib_devoir_temp', $datas);
                                        $save = $this->nano->saveData('delib_devoir', $datas);

                                        if($save == 1)
                                        {
                                            $etat = true;
                                            $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                        }
                                    }
                                }
                                else
                                {
                                    $etat = 2;
                                    $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.session('rentree').'/'.$etat, false);
                                }
                            }
                        }
                    }
                }
            }
        }

        //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
        $info = DB::table('delib_devoir_temp')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('evaluation', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        if($specialite == "TOUS")
        {
            $note = DB::table('delib_devoir_temp')
                        ->where(function($query) {
                                            $query->where('evaluation', session('session_compo'))
                                                ->orWhere('evaluation', 'LIKE', '%Devoir%');
                                })
                        ->where('code_rentree', session('rentree'))
                        ->where('niveau', session('niveau'))
                        ->where('semestre', session('semestre'))
                        ->get()->count();
        }
        else
        {
            $note = DB::table('delib_devoir_temp')
                        ->where(function($query) {
                                            $query->where('evaluation', session('session_compo'))
                                                ->orWhere('evaluation', 'LIKE', '%Devoir%');
                                })
                        ->where('code_rentree', session('rentree'))
                        ->where('specialite', $specialite)
                        ->where('niveau', session('niveau'))
                        ->where('semestre', session('semestre'))
                        ->get()->count();
        }

        if(!empty($info))
        {
            echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Synchronisé le ".date('d-m-Y à H:i:s', strtotime($info->date_enreg)) ));
        }
        else
        {
            echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Aucune ligne synchronisée. ") );
        }

    }
    /******************************************** */

    public function process3_licences()
    {
        $specialite = $_POST['specialite'];

        if(session('session_compo') == "SESSION 1")
        {
            if($specialite == "TOUS")
            {
                $this->fill_miror_new();
            }
            else
            {
                $this->fill_miror_new2($specialite);
            }

            //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
            $info = DB::table('delib_devoir')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('evaluation', session('session_compo'))
                        ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

            if($specialite == "TOUS")
            {
                $note = DB::table('delib_devoir')
                            ->where(function($query) {
                                                $query->where('evaluation', session('session_compo'))
                                                    ->orWhere('evaluation', 'LIKE', '%Devoir%');
                                    })
                            ->where('code_rentree', session('rentree'))
                            ->where('niveau', session('niveau'))
                            ->where('semestre', session('semestre'))
                            ->get()->count();
            }
            else
            {
                $note = DB::table('delib_devoir')
                            ->where(function($query) {
                                                $query->where('evaluation', session('session_compo'))
                                                    ->orWhere('evaluation', 'LIKE', '%Devoir%');
                                    })
                            ->where('code_rentree', session('rentree'))
                            ->where('specialite', $specialite)
                            ->where('niveau', session('niveau'))
                            ->where('semestre', session('semestre'))
                            ->get()->count();
            }

            if(!empty($info))
            {
                echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Synchronisé le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
            }
            else
            {
                echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Aucune ligne traitée. ") );
            }
        }
    }

    public function fill_miror_new()
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
            $rek = DB::select(DB::raw('SELECT id_note, annee_univ, id_auto, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation, specialite
                                FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" AND evaluation in (
                                        SELECT evaluation
                                            FROM delib_devoir_temp
                                            WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                        GROUP BY  evaluation, code_user
                                ) GROUP BY evaluation, code_user'));

            foreach($rek as $i => $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=>session('semestre'), "reference"=> $matiere->reference, "id_auto"=> $d->id_auto,
                                'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> session('niveau'), 'specialite'=> $d->specialite, 'annee_univ'=>session('annee_univ'));

                if($d->evaluation != "SESSION 2")
                {
                    //on verifie si la ligne existe deja dans la table
                    $check = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('evaluation', $d->evaluation)
                                ->where('code_matiere', $matiere->code_matiere)->where('id_auto', $d->id_uato)
                                ->first();

                    if(!empty($check))
                    {
                        //enregistrement des données dans la table
                        $this->nano->saveData('delib_devoir', $datas);
                    }
                    else
                    {
                        if($d->note > $check->note)
                        {
                            $check = DB::table('delib_devoir')
                                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                    ->where('semestre', session('semestre'))->where('evaluation', $d->evaluation)
                                    ->where('code_matiere', $matiere->code_matiere)->where('id_auto', $d->id_uato)
                                    ->update($datas);
                        }
                    }
                }
            }
        }
    }

    //pour la prise en compte des specialités
    public function fill_miror_new2($specialite)
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('specialite', $specialite)
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
            $rek = DB::select(DB::raw('SELECT id_note, annee_univ, id_auto, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation, specialite
                                FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$specialite.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" AND evaluation in (
                                        SELECT evaluation
                                            FROM delib_devoir_temp
                                            WHERE code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$specialite.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                        GROUP BY  evaluation, code_user
                                ) GROUP BY evaluation, code_user'));

            foreach($rek as $i => $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=>session('semestre'), "reference"=> $matiere->reference, "id_auto"=> $d->id_auto,
                                'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> session('niveau'), 'specialite'=> $d->specialite, 'annee_univ'=>session('annee_univ'));

                if($d->evaluation != "SESSION 2")
                {
                    //enregistrement des données dans la table
                    $this->nano->saveData('delib_devoir', $datas);
                }
            }
        }
    }

    //DERNIERE ETAPE 3
    public function load_table_note_new()
    {
        set_time_limit(0);

        $session = session('session_compo');
        $specialite = $_POST['specialite'];

        if($session == "SESSION 1")
        {
            if($specialite == "TOUS")
            {
                //SUPRESSION DES LIGNES
                $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);
                $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);

                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                FROM note_ecue WHERE niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND
                                                code_rentree="'.session('rentree').'" AND reference_ecue<>"" '));
            }
            else
            {
                //SUPRESSION DES LIGNES
                $this->nano->deleteDatas5("delib_note", "niveau", "code_rentree", "semestre", "session", "specialite", session('niveau'), session('rentree'), session('semestre'), $session, $specialite);
                $this->nano->deleteDatas5("delib_note_finale", "niveau", "code_rentree", "semestre", "session", "specialite", session('niveau'), session('rentree'), session('semestre'), $session, $specialite);

                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                FROM note_ecue WHERE niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND
                                                code_rentree="'.session('rentree').'" AND code_specialite="'.$specialite.'" AND recuperation=1 AND reference_ecue<>"" '));
            }
            //var_dump($etudiant); exit;

            foreach($etudiant as $etu)
            {
                $d = DB::table('v_etudiant_matiere')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))->where('specialite', $etu->code_specialite)
                                    ->where('id_auto', $etu->id_auto)->where('reference', $etu->reference_ecue)
                                    ->first();

                if(!empty($d))
                {
                    $specialite = $d->specialite; $matiere = $d->libelle;

                    //on va recuperer la note d'examen de l'etudiant
                    $examen = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('evaluation', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $etu->id_auto)->first();

                    //var_dump($examen); exit;
                    //coefficient (nbre de note de devoir)
                    $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.session('rentree').'"
                                            AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'"
                                            AND evaluation LIKE "%Devoir%" AND etat=0 GROUP BY code_user) AS nb');
                    //var_dump($coef); exit;

                    $note_examen = (!empty($examen)) ? $examen->note : 0;
                    $etat_examen = (!empty($examen)) ? false : true;
                    $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                    $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                    $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                    //NOTE DE DEVOIR
                    $note = DB::selectOne('SELECT SUM(note) AS note, code_matiere FROM delib_devoir WHERE code_rentree="'.session('rentree').'"
                                                    AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND id_auto="'.$etu->id_auto.'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'"
                                                    AND evaluation LIKE "%Devoir%" GROUP BY code_matiere, id_auto');

                    $notes = (!empty($note)) ? (floor(($note->note*100))/100)/$coeff : 0;

                    $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                    "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                    "note_devoir"=>$notes, "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "coef"=>$coeff, "reference"=> $d->reference );

                    $assoc = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $etu->id_auto)->first();

                    if(empty($assoc))
                    {
                        //on enregistre la ligne
                        $this->nano->saveData('delib_note', $datas);
                    }
                }
            }

        }
        else
        {
            if($specialite == "TOUS")
            {
                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                FROM note_ecue WHERE niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND
                                                code_rentree="'.session('rentree').'"  AND recuperation=1 AND reference_ecue<>"" AND moyenne<10 '));
            }
            else
            {
                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                FROM note_ecue WHERE niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND
                                                code_rentree="'.session('rentree').'" AND code_specialite="'.$specialite.'" AND recuperation=1 AND reference_ecue<>"" AND moyenne<10 '));
            }

            foreach($etudiant as $etu)
            {
                $d = DB::table('v_etudiant_matiere')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))->where('specialite', $etu->code_specialite)
                                    ->where('id_auto', $etu->id_auto)->where('reference', $etu->reference_ecue)
                                    ->first();

                if(!empty($d))
                {
                    $specialite = $d->specialite; $matiere = $d->libelle;

                    //on va recuperer la note d'examen de l'etudiant
                    $examen = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('evaluation', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $etu->id_auto)->first();

                    $note_examen = (!empty($examen)) ? $examen->note : 0;
                    $etat_examen = (!empty($examen)) ? false : true;

                    $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                    $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                    $datas = array("session"=> $session_examen, "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, );

                    if($note_examen != "")
                    {
                        $assoc = DB::table('delib_note')
                                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_matiere', $d->code_matiere)->where('specialite', $specialite)->where('id_auto', $etu->id_auto)
                                    ->update($datas);
                    }

                }
            }
        }

        //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
        $info = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        if($specialite == "TOUS")
        {
            $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('session', session('session_compo'))
                                ->where('semestre', session('semestre'))->get()->count();
        }
        else
        {
            $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('session', session('session_compo'))->where('specialite', $specialite)
                                ->where('semestre', session('semestre'))->get()->count();
        }

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Fusionné le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
    }


    //pour la session 1
    public function load_table_note_new_old()
    {
        set_time_limit(0);

        $session = session('session_compo');
        $specialite = $_POST['specialite'];

        if($session == "SESSION 1")
        {
            if($specialite == "TOUS")
            {
                //SUPRESSION DES LIGNES
                $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);
                $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);

                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion1', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();
            }
            else
            {
                //SUPRESSION DES LIGNES
                $this->nano->deleteDatas5("delib_note", "niveau", "code_rentree", "semestre", "session", "specialite", session('niveau'), session('rentree'), session('semestre'), $session, $specialite);
                $this->nano->deleteDatas5("delib_note_finale", "niveau", "code_rentree", "semestre", "session", "specialite", session('niveau'), session('rentree'), session('semestre'), $session, $specialite);

                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))->where('specialite', $specialite)
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion1', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))->where('specialite', $specialite)
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();

            }

            foreach($liste_etudiant as $etu)
            {
                $etudiant = DB::table('v_etudiant_matiere')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_etudiant', $etu->code_etudiant)
                                    ->get();

                foreach($etudiant as $d)
                {
                    $specialite = $d->specialite; $matiere = $d->libelle;

                    //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
                    $nb_devoir = DB::table('v_note_devoir')
                                        ->where('niveau', session('niveau'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

                    if($nb_devoir >= 1 )
                    {
                        //on va enregistrer la ligne de l'étudiant
                        $examen = DB::table('v_note_devoir')
                                        ->where('niveau', session('niveau'))->where('evaluation', session('session_compo'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('code_matiere', $d->code_matiere)
                                        ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->first();

                        //coefficient (nbre de note de devoir)
                        $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.session('rentree').'"
                                                AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'"
                                                AND evaluation LIKE "%Devoir%" AND etat=0 GROUP BY code_user) AS nb');

                        //var_dump($coef); exit;

                        $note_examen = (!empty($examen)) ? $examen->note : "";
                        $etat_examen = (!empty($examen)) ? false : true;
                        $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                        $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                        $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                        //NOTES DE DEVOIR
                        //$note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t1.evaluation LIKE "%Devoir%" GROUP BY t1.code_matiere, t1.code_user');
                        $note = DB::selectOne('SELECT GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 WHERE t1.code_rentree="'.session('rentree').'"
                                                AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t1.specialite="'.$specialite.'"
                                                AND t1.evaluation LIKE "%Devoir%" GROUP BY t1.code_matiere, t1.code_user');

                        if(!empty($note))
                        {
                            $notes = explode(",", $note->note);
                            $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                            $merge= array(array_combine($dev, $notes));

                            $note_devoir1 = (array_key_exists("Devoir01", $merge[0])) ? (floor(($merge[0]["Devoir01"]*100))/100) : "";
                            $note_devoir2 = (array_key_exists("Devoir02", $merge[0])) ? (floor(($merge[0]["Devoir02"]*100))/100) : "";
                            $note_devoir3 = (array_key_exists("Devoir03", $merge[0])) ? (floor(($merge[0]["Devoir03"]*100))/100) : "";
                            $note_devoir4 = (array_key_exists("Devoir04", $merge[0])) ? (floor(($merge[0]["Devoir04"]*100))/100) : "";
                            $note_devoir5 = (array_key_exists("Devoir05", $merge[0])) ? (floor(($merge[0]["Devoir05"]*100))/100) : "";
                            $note_devoir6 = (array_key_exists("Devoir06", $merge[0])) ? (floor(($merge[0]["Devoir06"]*100))/100) : "";
                        }
                        else
                        {
                            $note_devoir1=""; $note_devoir2=""; $note_devoir3=""; $note_devoir4=""; $note_devoir5=""; $note_devoir6="";
                        }

                        //var_dump($d->code_etudiant." | ".$d->code_matiere." | ".$note_devoir1." | ".$note_devoir2." | ".$note_devoir3." | ".$note_devoir4." | ".$note_devoir5. " | ".$note_devoir6); exit;

                        $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                        "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                        "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff,
                                        "devoir1"=> $note_devoir1, "devoir2"=> $note_devoir2, "devoir3"=> $note_devoir3,
                                        "devoir4"=> $note_devoir4, "devoir5"=> $note_devoir5, "devoir6"=> $note_devoir6, );
                        //var_dump($datas);

                        $assoc = DB::table('delib_note')
                                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                    ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                        if(empty($assoc))
                        {
                            //on enregistre la ligne
                            $this->nano->saveData('delib_note', $datas);
                        }
                    }
                }

                $datas = array("etat_fusion1" => true);

                DB::table('delib_etudiant')
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))
                    ->where('code_etudiant', $etu->code_etudiant)->update($datas);
            }
        }
        else
        {
            if($specialite == "TOUS")
            {
                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion2', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();
            }
            else
            {
                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))->where('specialite', $specialite)
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion2', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))->where('specialite', $specialite)
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();
            }

            foreach($liste_etudiant as $etu)
            {
                $etudiant = DB::table('v_etudiant_matiere')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_etudiant', $etu->code_etudiant)
                                    ->get();

                foreach($etudiant as $d)
                {
                    $specialite = $d->specialite; $matiere = $d->libelle;

                    //on va verifier si l'etudiant a une ligne
                    $nb_devoir = DB::table('delib_note')
                                        ->where('niveau', session('niveau'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('id_auto', $d->id_auto)->where('code_matiere', $d->code_matiere)->count();

                    if($nb_devoir >= 1 )
                    {
                        //on va enregistrer la ligne de l'étudiant
                        $examen = DB::table('v_note_devoir')
                                        ->where('niveau', session('niveau'))->where('evaluation', session('session_compo'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('code_matiere', $d->code_matiere)
                                        ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->first();

                        $note_examen = (!empty($examen)) ? $examen->note : "";
                        $etat_examen = (!empty($examen)) ? false : true;

                        $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                        $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                        //var_dump($d->code_etudiant." | ".$d->code_matiere." | ".$note_devoir1." | ".$note_devoir2." | ".$note_devoir3." | ".$note_devoir4." | ".$note_devoir5. " | ".$note_devoir6); exit;

                        $datas = array("session"=>$session, "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, );
                        //var_dump($datas);

                        if($note_examen != "")
                        {
                            $assoc = DB::table('delib_note')
                                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_matiere', $d->code_matiere)->where('specialite', $specialite)->where('id_auto', $d->id_auto)
                                    ->update($datas);
                        }

                    }
                }

                $datas = array("etat_fusion2" => true);

                DB::table('delib_etudiant')
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))
                    ->where('code_etudiant', $etu->code_etudiant)->update($datas);

            }
        }


        //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
        $info = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        if($specialite == "TOUS")
        {
            $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('session', session('session_compo'))
                                ->where('semestre', session('semestre'))->get()->count();
        }
        else
        {
            $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('session', session('session_compo'))->where('specialite', $specialite)
                                ->where('semestre', session('semestre'))->get()->count();
        }

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Fusionné le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
    }

}
