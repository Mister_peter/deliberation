<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nano;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Hash;

class MainController extends Controller
{
    protected $nano;

    public function __construct()
    {
        if (!Session::has('user_id')) return redirect('login')->send();

        $this->nano = new Nano;
    }

    public function index($id)
    {
        if (view()->exists($id)) {
            return view($id);
        } else {
            return view('pages-404');
        }
    }

    public function dashboard()
    {
        $data["niveau"] = DB::table('delib_etudiant')
            ->where('niveau', session('niveau'))
            ->where('annee_univ', session('annee_univ'))
            ->where('code_rentree', session('rentree'))
            ->whereRaw('id_auto <> "" ')->count();

        $data['nb_exam'] = DB::table('v_note_examen')
            ->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))
            ->where('annee_univ', session('annee_univ'))
            ->groupBy('email')->get();

        $data["spec"] = DB::table('parcours')
            ->join('specialite', 'parcours.code_specialite', '=', 'specialite.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->groupBy('specialite.code_specialite')->get();


        return view('dashboard', $data);
    }

    public function statistique()
    {
        $data["niveau"] = DB::table('delib_etudiant')
            ->where('niveau', session('niveau'))
            ->where('annee_univ', session('annee_univ'))
            ->where('code_rentree', session('rentree'))
            ->whereRaw('id_auto <> "" ')->count();

        $data["nb_etudiant"] = DB::table('delib_note')
            ->where('niveau', session('niveau'))
            ->where('annee_univ', session('annee_univ'))
            ->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->groupBy('id_auto')->get();

        $data["spec"] = DB::table('parcours')
            ->join('specialite', 'parcours.code_specialite', '=', 'specialite.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->groupBy('specialite.code_specialite')->get();


        return view('statistique', $data);
    }

    public function etudiant()
    {
        $data["annee"] = $this->nano->getAllId("annee_universitaire", "etat_annee", true);

        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        return view('licence-etudiant', $data);
    }

    public function matiere()
    {
        $data["matiere"] = DB::table('delib_matiere')
            ->where('niveau', session('niveau'))
            ->where('annee_univ', session('annee_univ'))
            ->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('etat', true)
            ->groupBy('code_matiere')->get();

        return view('licence-matiere', $data);
    }

    public function liste_user()
    {
        $data["user"] = $this->nano->getAll('delib_user');

        return view('user', $data);
    }

    public function note_devoir()
    {
        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        return view('licence-note', $data);
    }

    public function note_examen()
    {
        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        return view('licence-examen', $data);
    }

    public function note_finale()
    {
        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        return view('licence-finale', $data);
    }

    public function note_finale2()
    {
        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        return view('devoir-examen', $data);
    }

    public function global()
    {
        return view('calcul_note_global');
    }

    public function validation()
    {
        return view('validation_definitive');
    }

    public function pv_final()
    {
        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        // on verifie si le jury validé le pv
        $data['pv'] = 0;

        return view('pv-final', $data);
    }

    public function showNoteEcue(Request $request)
    {
        $data['menu'] = "niveau";
        $data['sous_menu'] = "devoir";

        $specialite = $request->input('specialite');
        $matiere = $request->input('matiere');

        $data['note'] = DB::select('SELECT t2.*, t1.coef, t1.moy1, t1.taux, t1.moy2, GROUP_CONCAT(t1.note) AS note, GROUP_CONCAT(t1.evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="' . session('rentree') . '" AND t1.niveau="' . session('niveau') . '" AND t1.semestre="' . session('semestre') . '" AND t1.code_matiere="' . $matiere . '" AND t2.specialite="' . $specialite . '" AND t1.etat=0 GROUP BY t1.code_matiere, t1.code_user');
        //var_dump($data['note']); exit;

        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        $data["etudiant"] = DB::table('delib_etudiant')
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)
            ->whereRaw('id_auto <> "" ')->get();

        $data["devoir"] = DB::table('delib_devoir')
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('code_matiere', $matiere)
            ->orderBy('evaluation')->groupBy('evaluation')->get();

        $data['matiere'] = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('specialite', $specialite)->where('code_matiere', $matiere)->groupBy('code_matiere')->first();
        $data['spec'] = $specialite;
        $data['option'] = $this->nano->getAll('delib_option');
        $data['visible'] = DB::table('v_note_devoir')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('code_matiere', $matiere)->where('specialite', $specialite)->groupBy('code_matiere')->groupBy('specialite')->first();

        //on recupere le maxi du nbre de devoirs
        $data['nbr'] = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE delib_devoir.code_rentree="' . session('rentree') . '" AND delib_devoir.niveau="' . session('niveau') . '" AND delib_devoir.semestre="' . session('semestre') . '" AND delib_devoir.code_matiere="' . $matiere . '" AND delib_etudiant.specialite="' . $specialite . '" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');
        $data['liste_devoir'] = DB::select('SELECT GROUP_CONCAT(t1.evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="' . session('rentree') . '" AND t1.niveau="' . session('niveau') . '" AND t1.semestre="' . session('semestre') . '" AND t1.code_matiere="' . $matiere . '" AND t2.specialite="' . $specialite . '" AND t1.code_user="' . $data['nbr']->code_user . '" AND t1.etat=0 GROUP BY t1.code_matiere, t1.code_user');

        $data['pv'] = DB::table('delib_note_finale')->where('code_rentree', session('rentree'))
            ->where('niveau', session('niveau'))->where('etat', true)->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $data['matiere']->libelle)
            ->count();

        //var_dump($data['liste_devoir']); exit;

        if (!empty($data["note"])) {
            return view('licence-note', $data);
        } else {
            return back()->withError('Aucune note trouvée pour cette requête');
        }
    }

    public function showNoteExamen(Request $request)
    {
        $data['menu'] = "niveau";
        $data['sous_menu'] = "examen";

        $specialite = $request->input('specialite');
        $matiere = $request->input('matiere');

        $data['note'] = DB::table('v_note_examen')->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)
            ->where('nom_matiere', $matiere)
            ->get();

        //var_dump($data['note']); exit;

        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        $data["etudiant"] = DB::table('delib_etudiant')
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)
            ->whereRaw('id_auto <> "" ')->get();

        $data['matiere'] = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('specialite', $specialite)->where('libelle', $matiere)->groupBy('code_matiere')->first();
        $data['spec'] = $specialite;

        $data['pv'] = DB::table('delib_note_finale')->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('etat', true)
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        //var_dump($data['note']); exit;

        if (!$data["note"]->isEmpty()) {
            $data['visible'] = DB::table('v_note_examen')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('nom_matiere', $matiere)->where('specialite', $specialite)->groupBy('nom_matiere')->groupBy('specialite')->first();
            $data['nbr'] = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE delib_devoir.code_rentree="' . session('rentree') . '" AND delib_devoir.niveau="' . session('niveau') . '" AND delib_devoir.semestre="' . session('semestre') . '" AND delib_devoir.code_matiere="' . $matiere . '" AND delib_etudiant.specialite="' . $specialite . '" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');

            return view('licence-examen', $data);
        } else {
            return back()->withError('Aucune note trouvée pour cette requête');
        }
    }

    //affichage des notes finales pour la deliberation des enseignants
    public function showNoteFinale(Request $request)
    {
        $data['menu'] = "niveau";
        $data['sous_menu'] = "finale";

        $specialite = $request->input('specialite');
        $matiere = $request->input('matiere');

        $matx = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('code_matiere', $matiere)
            ->groupBy('code_matiere')->first();
        $matiere = $matx->libelle;

        $data["jury"] = DB::table("delib_jury")->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))
            ->where('code_rentree', session('rentree'))->get();

        $data["jour"] = DB::table("delib_jour")->where('niveau', session('niveau'))->where('semestre', session('semestre'))
            ->where('code_rentree', session('rentree'))->first();

        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        $data['etudiant'] = DB::table('delib_note_finale')->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->get();

        $data['nbre_etudiant'] = DB::table('delib_note_finale')->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['nb_etudiant'] = DB::table('delib_etudiant')->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('specialite', $specialite)
            ->count();

        $data['compo'] = DB::table('v_note_examen')->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['admis'] = DB::table('delib_note_finale')->where('code_rentree', session('rentree'))
            ->where('niveau', session('niveau'))->where('decision', "ADMIS")->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count(); //var_dump($data['admis']->count());

        $data['echec'] = DB::table('delib_note_finale')->where('code_rentree', session('rentree'))
            ->where('niveau', session('niveau'))->where('decision', "REFUSE")->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['total'] = DB::table('delib_note_finale')->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        // on verifie si le jury validé le pv
        $data['pv'] = DB::table('delib_note_finale')->where('code_rentree', session('rentree'))
            ->where('niveau', session('niveau'))->where('etat', true)->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['taux'] = DB::table('delib_note')->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))
            ->where('code_rentree', session('rentree'))->where('nom_matiere', $matiere)
            ->where('specialite', $specialite)->groupBy('code_matiere')->first();

        $data['matiere'] = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('specialite', $specialite)->where('libelle', $matiere)->groupBy('code_matiere')->first();
        $data['spec'] = $specialite;

        if (!$data["etudiant"]->isEmpty()) {
            return view('licence-finale', $data);
        } else {
            return back()->withError('Aucun résultat trouvé pour cette requête, veuillez lancer le processus de calcul des notes de devoirs et de composition');
        }
    }

    //pour l'affiche des notes de devoirs + compo
    public function showNoteFinale2(Request $request)
    {
        $data['menu'] = "niveau";
        $data['sous_menu'] = "devoir";

        $specialite = $request->input('specialite');
        $matiere = $request->input('matiere');

        $matx = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))->where('specialite', $specialite)->where('code_matiere', $matiere)->groupBy('code_matiere')->first();

        $matiere = $matx->libelle;

        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        $data['etudiant'] = DB::table('delib_note')
            ->join('delib_etudiant', 'delib_note.id_auto', '=', 'delib_etudiant.id_auto')
            ->where('delib_note.code_rentree', session('rentree'))
            ->where('delib_note.niveau', session('niveau'))->where('delib_note.semestre', session('semestre'))
            ->where('delib_note.specialite', $specialite)->where('delib_note.nom_matiere', $matiere)
            ->get();
        // var_dump($data['etudiant']);
        // exit;

        //statistiques sur les devoirs
        $data['nbre_etudiant'] = DB::table('delib_note')
            ->where('niveau', session('niveau'))->where('semestre', session('semestre'))
            ->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['option'] = $this->nano->getAll('delib_option');
        $data['visible'] = DB::table('delib_note')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('nom_matiere', $matiere)->where('specialite', $specialite)->groupBy('code_matiere')->groupBy('specialite')->first();

        // on verifie si le jury validé le pv
        $data['pv'] = DB::table('delib_note_finale')->where('niveau', session('niveau'))->where('etat', true)
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['matiere'] = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('specialite', $specialite)->where('libelle', $matiere)->groupBy('code_matiere')->first();
        $data['spec'] = $specialite;

        if (!$data["etudiant"]->isEmpty()) {
            return view('devoir-examen', $data);
        } else {
            return back()->withError('Aucun résultat trouvé pour cette requête, veuillez lancer le processus de calcul des notes de devoirs et de composition');
        }
    }

    //Check liste de presence jury
    public function check_presence()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $id = $_POST['id'];

        //on check si le jury existe dans liste de presence
        $check = DB::table("delib_presence")->where('id_jury', $id)->where('niveau', session('niveau'))->where('semestre', session('semestre'))
            ->where('code_rentree', session('rentree'));

        if ($check->count() == 1) {
            $liste = $check->first();

            // on va mettre l'etat de presence a 1
            $etat = ($liste->presence == true) ? false : true;

            $data = array("presence" => $etat, "date_enreg" => date('Y-m-d H:i:s', time()), "user_validated" => session('user_email'));
            //var_dump($data); exit;
            DB::table("delib_presence")->where('id_jury', $id)->where('niveau', session('niveau'))
                ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))
                ->update($data);
        } else {
            //pour obtenir le dernier enregistrement
            $liste = DB::table('delib_presence')->orderBy('id_presence', 'DESC')->skip(0)->take(1)->first();
            $last = (!empty($liste)) ? $liste->id_presence : 0;

            $jury = $this->nano->getId("delib_jury", "id_jury", $id);
            $id_presence = intval($last);
            $id_presence = str_pad($id_presence + 1, 6, 0, STR_PAD_LEFT);

            $data = array(
                "id_presence" => $id_presence, "id_jury" => $id, "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "niveau" => session('niveau'), "semestre" => session('semestre'),
                "email" => $jury->email, "presence" => true, "date_enreg" => date('Y-m-d H:i:s', time()), "user_validated" => session('user_email')
            );
            //var_dump($data); exit;=>$
            $this->nano->saveData('delib_presence', $data);
        }

        $liste = $check->first();
        echo json_encode($liste);
    }

    //validation d ela de la liste de presence
    public function validate_presence(Request $request)
    {
        $specialite = $request->input('specialites');
        $matiere = $request->input('matieres');
        $date = $request->input('date_delib');

        //on check si le jury existe dans liste de presence
        $check = DB::table("delib_jour")->where('niveau', session('niveau'))->where('semestre', session('semestre'))
            ->where('code_rentree', session('rentree'));

        if ($check->count() == 1) {
            $pres = $check->first();

            $data = array("date" => $date, "user_validated" => session('user_email'));

            DB::table("delib_jour")->where('id_jour', $pres->id_jour)->update($data);
        } else {
            $data = array(
                "date" => $date, "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "niveau" => session('niveau'), "semestre" => session('semestre'),
                "user_validated" => session('user_email')
            );

            $this->nano->saveData('delib_jour', $data);
        }

        return back()->withSuccess('Liste de présence, validée avec succès !');
    }

    public function showetudiant(Request $request)
    {
        $data['menu'] = "niveau";
        $data['sous_menu'] = "etudiant";

        $specialite = $request->input('specialite');

        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        $data["etudiant"] = DB::table('delib_etudiant')
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)
            ->whereRaw('id_auto <> "" ')->get();

        $data['spec'] = $specialite;

        if (!$data["etudiant"]->isEmpty()) {
            return view('licence-etudiant', $data);
        } else {
            return back()->withError('Aucun étudiant trouvée pour cette requête');
        }
    }

    public function show_pv_final(Request $request)
    {
        $data['menu'] = "niveau";
        $data['sous_menu'] = "pv_final";

        $specialite = $request->input('specialite');
        $matiere = $request->input('matiere');

        $matx = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('specialite', $specialite)->where('code_matiere', $matiere)->groupBy('code_matiere')->first();
        $matiere = $matx->libelle;

        $data["jury"] = DB::table("delib_jury")->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))->get();

        $data["jour"] = DB::table("delib_jour")->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))->first();

        $data["specialite"] = DB::table('specialite')
            ->join('parcours', 'specialite.code_specialite', '=', 'parcours.code_specialite')
            ->where('parcours.code_niveau', session('niveau'))
            ->get();

        $data['etudiant'] = DB::table('delib_note_finale')->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->get();

        $data['nbre_etudiant'] = DB::table('delib_note_finale')->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['nb_etudiant'] = DB::table('delib_etudiant')->where('niveau', session('niveau'))->where('specialite', $specialite)
            ->where('code_rentree', session('rentree'))
            ->count();

        $data['compo'] = DB::table('v_note_examen')->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['admis'] = DB::table('delib_note_finale')->where('niveau', session('niveau'))->where('decision', "ADMIS")
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['echec'] = DB::table('delib_note_finale')->where('niveau', session('niveau'))->where('decision', "REFUSE")
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['total'] = DB::table('delib_note_finale')->where('semestre', session('semestre'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        // on verifie si le jury a validé le pv
        $data['pv'] = DB::table('delib_note_finale')->where('niveau', session('niveau'))->where('etat', true)
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('nom_matiere', $matiere)
            ->count();

        $data['matiere'] = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('specialite', $specialite)->where('libelle', $matiere)->groupBy('code_matiere')->first();
        $data['spec'] = $specialite;

        if (!$data["etudiant"]->isEmpty()) {
            if ($data['pv'] > 0) {
                return view('pv-final', $data);
            } else {
                return back()->withError('Veuillez valider le PV, avant de lancer cette requête.');
            }
        } else {
            return back()->withError('Aucun PV trouvé pour cette requête.');
        }
    }

    //application des bonus sur les notes de devoirs
    public function bonus_devoir()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $devoir = $_POST['devoir'];
        $id = $_POST['id'];
        $bonus = $_POST['bonus'];

        //boucle sur la liste
        $etudiant = DB::table("delib_note")->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('code_matiere', $matiere)->where('specialite', $specialite)
            ->get();

        foreach ($etudiant as $etu) {
            if ($etu->$devoir != "") {
                switch ($id) {
                    case '1': //ajouter un plus sur la note
                        $note = $etu->$devoir + $bonus;
                        $note = ($note > 20) ? 20 : $note;
                        $data = array($devoir => $note);
                        break;

                    case '2':  //enlever un plus sur la note
                        $note = $etu->$devoir - $bonus;
                        $note = ($note < 0) ? 0 : $note;
                        $data = array($devoir => $note);
                        break;
                }

                //var_dump($data); exit;
                DB::table("delib_note")->where('niveau', session('niveau'))
                    ->where('code_matiere', $matiere)->where('specialite', $specialite)
                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                    ->where('id_auto', $etu->id_auto)
                    ->update($data);
            }
        }

        echo json_encode(array("status" => "1"));
    }

    //application des bonus sur les notes d'examen
    public function bonus_examen()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $id = $_POST['id'];
        $bonus = $_POST['bonus'];

        //boucle sur la liste
        $etudiant = DB::table("delib_note")->where('niveau', session('niveau'))
            ->where('code_matiere', $matiere)->where('specialite', $specialite)
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->get();

        foreach ($etudiant as $etu) {
            if ($etu->note_examen != "") {
                switch ($id) {
                    case '1': //ajouter un plus sur la note
                        $note = $etu->note_examen + $bonus;
                        $note = ($note > 20) ? 20 : $note;
                        $data = array("note_examen" => $note);
                        break;

                    case '2':  //enlever un plus sur la note
                        $note = $etu->note_examen - $bonus;
                        $note = ($note < 0) ? 0 : $note;
                        $data = array("note_examen" => $note);
                        break;
                }

                //var_dump($etu->id_auto); exit;
                DB::table("delib_note")->where('niveau', session('niveau'))
                    ->where('code_matiere', $matiere)->where('specialite', $specialite)
                    ->where('semestre', session('semestre'))
                    ->where('id_auto', $etu->id_auto)->where('code_rentree', session('rentree'))
                    ->update($data);
            }
        }

        echo json_encode(array("status" => "1"));
    }

    //application des options sur les notes de devoirs
    public function set_option_note()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $etat = $_POST['etat'];
        $coef = $_POST['coef'];

        // on va mettre l'etat du devoir a 1, pour ne pas qu'il soit pas pris en compte
        $coef = ($coef > 0) ? ($coef - 1) : 0;
        $data = array($etat => true, "coef" => $coef);

        DB::table("delib_note")
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('code_matiere', $matiere)
            ->where($etat, false)
            ->where('specialite', $specialite)
            ->update($data);

        echo json_encode(array("status" => "1"));
    }

    //application des options de reinitialisation sur les notes de devoirs
    public function reset_option_note()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $etat = $_POST['etat'];
        $coef = $_POST['coef'];

        // on va mettre l'etat du devoir a 1, pour ne pas qu'il soit pris en compte
        $coef = ($coef > 0) ? ($coef + 1) : 0;
        $data = array($etat => false, "coef" => $coef);

        DB::table("delib_note")
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('code_matiere', $matiere)
            ->where($etat, true)
            ->where('specialite', $specialite)
            ->update($data);

        echo json_encode(array("status" => "1"));
    }

    //supprimer temporairement les notes d'examen
    public function delete_examen()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $id = $_POST['id'];

        // on va mettre l'etat de l'examen a 1, pour ne pas qu'il soit pas pris en compte
        switch ($id) {
            case '1': //supprimer la note d'examen
                $data = array("etat_exam" => true);
                break;

            case '2':  //reinitialiser la note d'examen
                $data = array("etat_exam" => false);
                break;
        }

        //var_dump($data);
        DB::table("delib_note")
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('code_matiere', $matiere)
            ->where('specialite', $specialite)
            ->update($data);

        echo json_encode(array("status" => "1"));
    }

    //mise à jour des notes devoirs d'un etudiant
    public function update_note(Request $request)
    {
        $id = $request->input('id1');
        $coef = $request->input('cof');

        for ($i = 1; $i <= $coef; $i++) {
            $devoir  = $request->input('devoir' . $i);
            $data = array("devoir" . $i => $devoir);
            //var_dump($data);

            $this->nano->updateData('delib_note', 'id_note', $id, $data);
        }

        return back()->withSuccess('Notes modifiées avec succès !');
    }

    //mise à jour de la note d'examen d'un etudiant
    public function update_note_examen(Request $request)
    {
        $note = $request->input('notes');
        $id = $request->input('id3');

        $data = array("note_examen" => $note);

        $this->nano->updateData('delib_note', 'id_note', $id, $data);

        return back()->withSuccess('Note modifiée avec succès !');
    }

    //mise à jour du coefficient (total des notes dans la matiere) d'un etudiant
    public function update_user_coef(Request $request)
    {
        $coef = $request->input('coef');
        $id = $request->input('id2');

        $data = array("coef" => $coef);

        DB::table("delib_note")
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('id_note', $id)
            ->update($data);

        return back()->withSuccess('Coefficient modifié avec succès !');
    }

    //recuperation des info d'un étudiant, relativement à ses notes
    public function getIdNote($id)
    {
        $data = DB::table('delib_note')
            ->join('delib_etudiant', 'delib_note.id_auto', '=', 'delib_etudiant.id_auto')
            ->where('delib_note.niveau', session('niveau'))
            ->where('delib_note.code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('delib_note.id_note', $id)->first();

        echo json_encode($data);
    }

    public function getIdNoteFinale($id)
    {
        $data = DB::table('delib_note_finale')
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('id_note', $id)->first();

        echo json_encode($data);
    }

    public function processus_note_devoir_old()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $taux = $_POST['taux'];
        $taux2 = $_POST['taux2'];

        $etudiant = DB::table('v_note_devoir')
            ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
            ->where('code_matiere', $matiere)->where('specialite', $specialite)
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('etat', false)->groupBy('code_user')->get();
        $moy = 0;
        foreach ($etudiant as $val) {
            //somme des notes de devoir pour la matiere
            $note = DB::table('v_note_devoir')
                ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                ->where('code_user', $val->code_user)->where('code_matiere', $matiere)
                ->where('specialite', $specialite)->where('etat', false)
                ->sum('note');

            $moy1 = ($note / $val->coef);
            $moy2 = $moy1 * ($taux / 100);

            $data = array("moy1" => $moy1, "taux" => $taux, "moy2" => $moy2);

            DB::table("v_note_devoir")
                ->where('niveau', session('niveau'))->where('semestre', session('semestre'))
                ->where('annee_univ', session('annee_univ'))->where('code_rentree', session('rentree'))
                ->where('code_matiere', $matiere)->where('specialite', $specialite)
                ->where('etat', false)->where('code_user', $val->code_user)
                ->update($data);

            //var_dump($note." | ".$moy2);
        }

        //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
        $matieres = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('annee_univ', session('annee_univ'))->where('specialite', $specialite)->where('code_matiere', $matiere)->where('etat', true)->groupBy('code_matiere')->first();

        $etudiant = DB::table('delib_etudiant')
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('annee_univ', session('annee_univ'))
            ->where('specialite', $specialite)->whereRaw('id_auto <> "" ')->get();

        foreach ($etudiant as $d) {
            //on va compter le nbre de devoir de l'etudiant (devoir >=5) et s'il a composé
            $nb_devoir = DB::table('v_note_devoir')
                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
                ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

            $nb_examen = DB::table('v_note_examen')
                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
                ->where('specialite', $specialite)->where('email', $d->email)->count();

            if ($nb_devoir >= 5 && $nb_examen > 0) {
                $devoir = DB::table('v_note_devoir')
                    ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                    ->where('code_matiere', $matiere)->where('specialite', $specialite)->where('code_user', $d->code_etudiant)
                    ->where('etat', false)->groupBy('code_user')->first();

                //on verifie si l'etudiant a une note de devoir
                $moy_devoir = (!empty($devoir)) ? $devoir->moy2 : 0;

                $datas = array(
                    "code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau" => session('niveau'), "semestre" => session('semestre'), "id_auto" => $d->id_auto, "email" => $d->email, "nom" => $d->nom, "prenoms" => $d->prenoms,
                    "code_matiere" => $matiere, "nom_matiere" => $matieres->libelle, "specialite" => $d->specialite, "devoir" => $moy_devoir,
                );

                //on verifie si la ligne existe dans la table delib_note_finale
                $nf = DB::table('delib_note_finale')
                    ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                    ->where('code_matiere', $matiere)->where('specialite', $specialite)->where('email', $d->email)->first();
                if (empty($nf)) {
                    $this->nano->saveData('delib_note_finale', $datas);
                } else {
                    DB::table("delib_note_finale")
                        ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                        ->where('code_matiere', $matiere)->where('specialite', $specialite)
                        ->where('email', $d->email)
                        ->update($datas);
                }
            }
        }

        echo json_encode(array("status" => "1"));
    }

    public function processus_note_devoir()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $taux = $_POST['taux'];
        $taux2 = $_POST['taux2'];

        $etudiant = DB::table('delib_note')
            ->join('delib_etudiant', 'delib_note.id_auto', '=', 'delib_etudiant.id_auto')
            ->where('delib_note.niveau', session('niveau'))
            ->where('delib_note.semestre', session('semestre'))
            ->where('delib_note.annee_univ', session('annee_univ'))
            ->where('delib_note.code_rentree', session('rentree'))
            ->where('delib_note.code_matiere', $matiere)
            ->where('delib_note.specialite', $specialite)->get();

        $moy = 0;
        foreach ($etudiant as $val) {

            //somme des notes de devoir pour la matiere
            $note1 = ($val->etat1 == 0) ? ($val->devoir1 != "") ? $val->devoir1 : 0  :  0;
            $note2 = ($val->etat2 == 0) ? ($val->devoir2 != "") ? $val->devoir2 : 0  :  0;
            $note3 = ($val->etat3 == 0) ? ($val->devoir3 != "") ? $val->devoir3 : 0  :  0;
            $note4 = ($val->etat4 == 0) ? ($val->devoir4 != "") ? $val->devoir4 : 0  :  0;
            $note5 = ($val->etat5 == 0) ? ($val->devoir5 != "") ? $val->devoir5 : 0  :  0;

            $m_exam = ($val->etat_exam == 0) ? ($val->note_examen != "") ? $val->note_examen : 0  :  0;

            //$m_exam = ($val->note_examen != "") ? $val->note_examen : 0;
            $cf = ($val->coef == 0) ? 1 : $val->coef;

            $moy_devoir = (($note1 + $note2 + $note3 + $note4 + $note5) / $cf) * ($taux / 100);

            $moy_examen = $m_exam * ($taux2 / 100);

            $data = array("mga_devoir" => $moy_devoir, "taux1" => $taux, "mga_examen" => $moy_examen, "taux2" => $taux2);

            DB::table("delib_note")
                ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                ->where('code_matiere', $matiere)->where('specialite', $specialite)
                ->where('id_note', $val->id_note)
                ->update($data);

            //ENREGISTREMENT DANS LA TABLE DELIB_NOTE_FINALE
            $mga = $moy_devoir + $moy_examen;
            $mga = round($mga, 2);
            $decision = ($mga >= 10) ? "ADMIS" : "REFUSE";
            $mention = $this->mention($mga);

            $datas = array(
                "code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau" => session('niveau'), "semestre" => session('semestre'), "id_auto" => $val->id_auto, "email" => $val->email, "nom" => $val->nom, "prenoms" => $val->prenoms,
                "code_matiere" => $matiere, "nom_matiere" => $val->nom_matiere, "specialite" => $specialite, "note_examen" => $m_exam, "devoir" => $moy_devoir, "examen" => $moy_examen, "session" => $val->session,
                "mga" => $mga, "mga2" => $mga, "mention" => $mention, "decision" => $decision
            );

            $this->nano->saveData('delib_note_finale', $datas);
        }

        echo json_encode(array("status" => "1"));
    }

    //calcul global pour les note devoirs + examen
    public function processus_global()
    {
        $taux = $_POST['taux'];
        $taux2 = $_POST['taux2'];

        $etudiant = DB::table('delib_note')
            ->where('delib_note.niveau', session('niveau'))
            ->where('delib_note.semestre', session('semestre'))
            ->where('delib_note.code_rentree', session('rentree'))
            ->orderBy('delib_note.specialite')->orderBy('delib_note.code_matiere')->get();

        $moy = 0;
        foreach ($etudiant as $val) {
            $specialite = $val->specialite;
            $matiere = $val->code_matiere;

            //somme des notes de devoir pour la matiere
            $note1 = ($val->etat1 == 0) ? ($val->devoir1 != "") ? $val->devoir1 : 0  :  0;
            $note2 = ($val->etat2 == 0) ? ($val->devoir2 != "") ? $val->devoir2 : 0  :  0;
            $note3 = ($val->etat3 == 0) ? ($val->devoir3 != "") ? $val->devoir3 : 0  :  0;
            $note4 = ($val->etat4 == 0) ? ($val->devoir4 != "") ? $val->devoir4 : 0  :  0;
            $note5 = ($val->etat5 == 0) ? ($val->devoir5 != "") ? $val->devoir5 : 0  :  0;

            $m_exam = ($val->etat_exam == 0) ? ($val->note_examen != "") ? $val->note_examen : 0  :  0;

            //$m_exam = ($val->note_examen != "") ? $val->note_examen : 0;
            $cf = ($val->coef == 0) ? 1 : $val->coef;
            $moy_devoir = (($note1 + $note2 + $note3 + $note4 + $note5) / $cf) * ($taux / 100);

            $moy_examen = $m_exam * ($taux2 / 100);

            $data = array("mga_devoir" => $moy_devoir, "taux1" => $taux, "mga_examen" => $moy_examen, "taux2" => $taux2);

            DB::table("delib_note")
                ->where('niveau', session('niveau'))
                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                ->where('code_matiere', $matiere)->where('specialite', $specialite)
                ->where('id_note', $val->id_note)
                ->update($data);

            //ENREGISTREMENT DANS LA TABLE DELIB_NOTE_FINALE
            $mga = $moy_devoir + $moy_examen;
            $mga = round($mga, 2);
            $decision = ($mga >= 10) ? "ADMIS" : "REFUSE";
            $mention = $this->mention($mga);

            $info = $this->nano->getId("delib_etudiant", "id_auto", $val->id_auto);

            $datas = array(
                "code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau" => session('niveau'), "semestre" => session('semestre'), "id_auto" => $val->id_auto, "email" => $info->email, "nom" => $info->nom, "prenoms" => $info->prenoms,
                "code_matiere" => $matiere, "nom_matiere" => $val->nom_matiere, "specialite" => $specialite, "note_examen" => $m_exam, "devoir" => $moy_devoir, "examen" => $moy_examen, "session" => $val->session,
                "mga" => $mga, "mga2" => $mga, "mention" => $mention, "decision" => $decision
            );

            $this->nano->saveData('delib_note_finale', $datas);
        }

        echo json_encode(array("status" => "1"));
    }

    public function reset_processus_devoir()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];

        $data = array("mga_devoir" => "", "taux1" => "", "mga_examen" => "", 'taux2' => "");

        DB::table("delib_note")
            ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('code_matiere', $matiere)->where('specialite', $specialite)
            ->update($data);

        //SUPPRESSION DES DONNEES DE LA TABLE
        DB::table("delib_note_finale")
            ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('code_matiere', $matiere)->where('specialite', $specialite)
            ->delete();

        echo json_encode(array("status" => "1"));
    }

    public function reset_global()
    {
        $data = array("mga_devoir" => "", "taux1" => "", "mga_examen" => "", 'taux2' => "");

        DB::table("delib_note")
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->update($data);

        //SUPPRESSION DES DONNEES DE LA TABLE
        DB::table("delib_note_finale")
            ->where('niveau', session('niveau'))
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->delete();

        echo json_encode(array("status" => "1"));
    }

    public function processus_note_examen()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];
        $taux = $_POST['taux'];

        $mat = DB::table('delib_matiere')->where('etat', true)->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('annee_univ', session('annee_univ'))->where('specialite', $specialite)->where('libelle', $matiere)->groupBy('code_matiere')->first();

        $check = DB::table('v_note_devoir')
            ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
            ->where('code_matiere', $mat->code_matiere)
            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('etat', false)
            ->groupBy('code_matiere')->groupBy('specialite')->first();

        if ($check->moy2 != "") {
            $etudiant = DB::table('v_note_examen')
                ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                ->where('nom_matiere', $matiere)
                ->where('specialite', $specialite)->get();
            $moy = 0;
            foreach ($etudiant as $val) {
                $moy = $val->note * ($taux / 100);

                $data = array("taux" => $taux, "moy" => $moy);

                DB::table("v_note_examen")
                    ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                    ->where('nom_matiere', $matiere)->where('specialite', $specialite)
                    ->where('email', $val->email)
                    ->update($data);
            }

            //PROCESS POUR ENREGISTRER LES NOTES FINALES D'EXAMEN POUR CHAQUE ETUDIANT
            $etudiant = DB::table('delib_etudiant')
                ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                ->where('code_rentree', session('rentree'))
                ->where('specialite', $specialite)->whereRaw('id_auto <> "" ')->get();

            foreach ($etudiant as $d) {
                //on va compter le nbre de devoir de l'etudiant (devoir >=5) ou s'il a composé
                $nb_devoir = DB::table('v_note_devoir')
                    ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                    ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

                $nb_examen = DB::table('v_note_examen')
                    ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                    ->where('specialite', $specialite)->where('email', $d->email)->count();

                if ($nb_devoir >= 1 || $nb_examen > 0) {
                    $examen = DB::table('v_note_examen')
                        ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                        ->where('nom_matiere', $mat->libelle)->where('specialite', $specialite)->where('email', $d->email)->first();

                    //on verifie si l'etudiant a une note d'examen
                    $moy_exam = (!empty($examen)) ? $examen->moy : 0;

                    //on verifie si la ligne existe dans la table delib_note_finale
                    $nf = DB::table('delib_note_finale')
                        ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                        ->where('code_matiere', $mat->code_matiere)->where('specialite', $specialite)->where('email', $d->email)->first();

                    $mga = $nf->devoir + $moy_exam;
                    $decision = ($mga >= 10) ? "ADMIS" : "REFUSE";
                    $mention = $this->mention($mga);

                    $datas = array(
                        "code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau" => session('niveau'), "semestre" => session('semestre'), "id_auto" => $d->id_auto, "email" => $d->email, "nom" => $d->nom, "prenoms" => $d->prenoms,
                        "code_matiere" => $mat->code_matiere, "nom_matiere" => $mat->libelle, "specialite" => $d->specialite, "note_examen" => $examen->note,
                        "examen" => $moy_exam, "mga" => $mga, "mga2" => $mga, "mention" => $mention, "decision" => $decision
                    );

                    if (empty($nf)) {
                        $this->nano->saveData('delib_note_finale', $datas);
                    } else {
                        DB::table("delib_note_finale")
                            ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                            ->where('code_matiere', $mat->code_matiere)->where('specialite', $specialite)
                            ->where('email', $d->email)
                            ->update($datas);
                    }
                }
            }

            echo json_encode(array("status" => "1"));
        } else {
            echo json_encode(array("status" => 0));
        }
    }

    //Application du repechage
    public function update_all_mga()
    {
        $note = $_POST['note'];
        $moy1 = $_POST['moy1'];
        $moy2 = $_POST['moy2'];
        $matiere = $_POST['matiere'];
        $specialite = $_POST['specialite'];
        $mga = $_POST['mga'];

        $etudiant = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
            ->where('niveau', session('niveau'))->where('semestre', session('semestre'))
            ->where('code_rentree', session('rentree'))
            ->whereBetween('mga2', array($moy1, $moy2))
            ->where('note_examen', '>=', $note)
            ->where('specialite', $specialite)->where('code_matiere', $matiere)
            ->get();

        foreach ($etudiant as $d) {
            $mention = $this->mention($mga);
            $decision = ($mga >= 10) ? "ADMIS" : "REFUSE";

            $data = array("mga2" => $mga, "mention" => $mention, "decision" => $decision);

            DB::table("delib_note_finale")
                ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                ->where('id_note', $d->id_note)->where('code_matiere', $matiere)
                ->where('specialite', $specialite)
                ->update($data);
        }

        echo json_encode(array("status" => "1"));
    }

    //reinitialser toutes les moyenne finales du pv
    public function reset_all_mga()
    {
        $matiere = $_POST['matiere'];
        $specialite = $_POST['specialite'];

        $etudiant = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('code_matiere', $matiere)
            ->get();

        foreach ($etudiant as $d) {
            $mention = $this->mention($d->mga);
            $decision = ($d->mga >= 10) ? "ADMIS" : "REFUSE";

            $data = array("mga2" => $d->mga, "mention" => $mention, "decision" => $decision);

            DB::table("delib_note_finale")
                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
                ->where('id_note', $d->id_note)
                ->where('code_matiere', $matiere)
                ->where('specialite', $specialite)
                ->update($data);
        }

        echo json_encode(array("status" => "1"));
    }

    //Validation du pv finale
    public function valider_pv()
    {
        $matiere = $_POST['matiere'];
        $specialite = $_POST['specialite'];

        $etudiant = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('specialite', $specialite)->where('code_matiere', $matiere)
            ->where('semestre', session('semestre'))
            ->get();

        foreach ($etudiant as $d) {
            $data = array("etat" => true,);

            DB::table("delib_note_finale")
                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
                ->where('id_note', $d->id_note)->where('code_matiere', $matiere)
                ->where('specialite', $specialite)
                ->update($data);
        }

        echo json_encode(array("status" => "1"));
    }

    //Validation du pv finale
    public function reset_pv()
    {
        $matiere = $_POST['matiere'];
        $specialite = $_POST['specialite'];

        $etudiant = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('semestre', session('semestre'))
            ->where('specialite', $specialite)->where('code_matiere', $matiere)
            ->get();

        foreach ($etudiant as $d) {
            $data = array("etat" => false,);

            DB::table("delib_note_finale")
                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
                ->where('id_note', $d->id_note)
                ->where('code_matiere', $matiere)->where('specialite', $specialite)
                ->update($data);
        }

        echo json_encode(array("status" => "1"));
    }

    public function update_mga(Request $request)
    {
        $mga = $request->input('mga');
        $id = $request->input('id_note');

        $mention = $this->mention($mga);
        $decision = ($mga >= 10) ? "ADMIS" : "REFUSE";

        $data = array("mga2" => $mga, "mention" => $mention, "decision" => $decision);

        DB::table("delib_note_finale")
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
            ->where('id_note', $id)
            ->update($data);

        return back()->withSuccess('Moyenne modifiée avec succès !');
    }

    public function update_coef()
    {
        $matiere = DB::table('delib_matiere')->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))->where('semestre', session('semestre'))->where('annee_univ', session('annee_univ'))->where('etat', true)->groupBy('code_matiere')->get();
        foreach ($matiere as $matiere) {
            $nbr = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE code_rentree="' . session('rentree') . '" AND niveau="' . session('niveau') . '" AND semestre="' . session('semestre') . '" AND code_matiere="' . $matiere->code_matiere . '" AND niveau="' . session('niveau') . '" AND etat=0 GROUP BY code_user) AS nb');
            //$nbr = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM v_note_devoir  WHERE code_matiere="'.$matiere->code_matiere.'" AND etat=0 GROUP BY code_user) AS nb');

            $data = array("coef" => $nbr->nombre);

            //$this->nano->updateData('delib_devoir', 'code_matiere', $matiere->code_matiere, $data);
            DB::table("delib_devoir")
                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
                ->where('code_matiere', $matiere->code_matiere)
                ->update($data);
        }
    }

    public function reset_processus_examen()
    {
        $specialite = $_POST['specialite'];
        $matiere = $_POST['matiere'];

        $data = array("taux" => "", "moy" => "");

        DB::table("v_note_examen")
            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
            ->where('annee_univ', session('annee_univ'))->where('semestre', session('semestre'))
            ->where('nom_matiere', $matiere)
            ->where('specialite', $specialite)
            ->update($data);

        echo json_encode(array("status" => "1"));
    }

    //validation definitive de la deliberation
    public function validation_deliberation()
    {
        //on check si la ligne de la deliberation existe
        $check = DB::table("delib_jour")->where('annee_univ', session('annee_univ'))->where('niveau', session('niveau'))
            ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'));

        if ($check->count() == 1) {
            $delib = $check->first();

            $data = array("etat" => true, "date_validation" => date('Y-m-d H:i:s', time()));

            DB::table("delib_jour")->where('id_jour', $delib->id_jour)->update($data);

            echo json_encode(array("status" => "1"));
        } else {
            echo json_encode(array("status" => "0"));
        }
    }

    //envoi des notes finales après deliberation vers la table note_ecue
    public function send_note_finale()
    {
        $matiere = DB::table('delib_matiere')->where('niveau', session('niveau'))
                        ->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->orderBy('specialite')->get();

        //compteur, seulement pour compter les erreurs
        // $i = 0;
        foreach ($matiere as $d)
        {
            $note_finale = DB::table("delib_note_finale")
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))
                            ->where('code_matiere', $d->code_matiere)->where('specialite', $d->specialite)->get();

            foreach ($note_finale as $note)
            {
                $maquette = DB::table('maquette_ecue')->where('niveau_etude', session('niveau'))
                            ->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('code_specialite', $d->specialite)
                            ->where('reference_cours', $d->reference)->first();

                $parc = DB::table('parcours')->where('code_niveau', session('niveau'))
                            ->where('code_specialite', $note->specialite)->first();

                $data = array("moyenne" => $note->mga2, "mention" => $note->mention, "session" => $note->session);

                if (isset($maquette->code_ecue))
                {
                    // On verifie que la ligne exite dans la table note_ecue
                    $verif_note_ecue = DB::table("note_ecue")
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))
                                    ->where('semestre', session('semestre'))
                                    ->where('id_auto', $note->id_auto)
                                    ->where('code_parcours', $parc->code_parcours)
                                    ->where('code_ecue', $maquette->code_ecue)
                                    ->first();

                    if (isset($verif_note_ecue->code_ecue))
                    {
                        $req = DB::table("note_ecue")
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('id_auto', $note->id_auto)
                                ->where('code_parcours', $parc->code_parcours)->where('code_ecue', $maquette->code_ecue)
                                ->update($data);

                        // echo json_encode(array(
                        //     "MAJ" => "OK",
                        //     "Intitule ECUE" => $verif_note_ecue->intitule_ecue,
                        //     "code ECUE" => $verif_note_ecue->code_ecue,
                        //     "Mention" => $verif_note_ecue->mention,
                        // ));
                    }
                    //else
                    //{
                    //     echo 0;
                    //     echo json_encode(array("MAJ" => "PAS OK", "Compteur" => $i++));
                    // }
                    // } else {
                    //     // dump($maquette);
                    //     echo json_encode(array("MAQUETTE NULL" => "PAS OK", "Compteur" => $i++));
                }
            }
        }

        echo json_encode(array("status" => "1"));
    }

    //Calcule de la moyenne obtenu en UE
    public function calcul_moy_ue()
    {
        $etudiant = DB::table("note_ecue")
                    ->select('code_rentree', 'code_parcours', 'niveau', 'semestre', 'id_auto', 'code_ue')
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))
                    ->distinct()
                    ->get();

        foreach ($etudiant as $n)
        {
            $note = DB::table("note_ecue")
                        ->select(DB::raw('sum(moyenne * coefficient) as moy_coef'), DB::raw('sum(coefficient) as coefficient'), 'id_auto')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                        ->where('code_ue', $n->code_ue)
                        ->where('id_auto', $n->id_auto)->first();

            $moy_coef = $note->moy_coef;
            $coef_ue = $note->coefficient;
            $moy_ue = ($coef_ue > 0) ? $moy_coef / $coef_ue : 0;

            //obtenir le nombre de l'ue
            // $maq = DB::table("maquette_ecue")
            //     ->where('niveau_etude', session('niveau'))->where('code_rentree', session('rentree'))
            //     ->where('semestre', session('semestre'))->where('code_ue', $n->code_ue)
            //     ->get();

            // $nbre_ue = $maq->count();
            // $moy_ue = ($nbre_ue > 0) ? $note->moyenne / $nbre_ue : 0;

            //obtenir la note minimum dans l'ecue
            $min_note = DB::table("note_ecue")
                            ->select(DB::raw('min(moyenne) as moyenne'))
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('code_ue', $n->code_ue)
                            ->where('id_auto', $n->id_auto)->first();
            // ->min('moyenne');

            $note_min = $min_note->moyenne;

            //OBTENTION DE LA MENTION
            if ($moy_ue < 10) {
                $mention = "Refusé";
            } elseif ($moy_ue >= 10 && $moy_ue < 12) {
                $mention = "Passable";
            } elseif ($moy_ue >= 12 && $moy_ue < 14) {
                $mention = "Assez Bien";
            } elseif ($moy_ue >= 14 && $moy_ue < 16) {
                $mention = "Bien";
            } elseif ($moy_ue >= 16 && $moy_ue < 18) {
                $mention = "Très Bien";
            } elseif ($moy_ue >= 18 && $moy_ue <= 20) {
                $mention = "Excellent";
            }

            //OBTENTION DE LA DECISION
            if ($note_min < 5)
            {
                $decision = "REFUSE";
                $credit = 0;
            } else
            {
                if ($moy_ue < 10)
                {
                    $decision = "REFUSE";
                    $credit = 0;
                }
                else
                {
                    $decision = "ADMIS";

                    //Obtenir le total de credit
                    $rek = DB::table("maquette_ecue")
                                ->select(DB::raw('sum(credit_ecue) as credit_ecue'))
                                ->where('niveau_etude', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('code_ue', $n->code_ue)
                                ->first();

                    $credit = $rek->credit_ecue;
                }
            }

            //on verifie si l'etudiant a deja validé l'UE
            $rek = DB::table("note_ue")
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))->where('code_ue', $n->code_ue)
                    ->where('id_auto', $n->id_auto)
                    ->first();

            if ($rek->count() > 0)
            {
                if ($rek->decision != "ADMIS")
                {
                    //mise à jour
                    $data = array(
                        "moyenne" => $moy_ue, "decision" => $decision, "mention" => $mention,
                        "credit_obtenu" => $credit, "session" => session('session_compo'), "annee_compo" => $rek->annee_univ
                    );

                    DB::table("note_ue")->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('code_ue', $n->code_ue)
                        ->where('id_auto', $n->id_auto)
                        ->update($data);

                }
                else
                {

                }
            }
            else
            {

            }
        }

        echo json_encode(array("status" => "1"));
    }

    //Calcule de la moyenne semestrielle
    public function calcul_moy_semestrielle()
    {
        $etudiant = DB::table("note_ecue")
                    ->select('code_rentree', 'code_parcours', 'code_specialite', 'niveau', 'semestre', 'id_auto')
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))
                    ->distinct()
                    ->get();

        foreach ($etudiant as $n)
        {
            //Verifier si l'etudiant a obtenu les 30 credits du semestre
            $sql = DB::table("pv_synthese_parcours_sem")
                        ->where('niveau_etude', session('niveau'))->where('code_specialite', $n->code_specialite)
                        ->where('semestre', session('semestre'))->where('id_auto', $n->id_auto)
                        ->first();

            if ($sql->credit_obtenu >= 0 && $sql->credit_obtenu < 30) {
                $rek = DB::table("note_ue")
                        ->select(DB::raw('sum(moyenne * coefficient) as moy_coef'), DB::raw('sum(coefficient) as coefficient'))
                        ->where('niveau', session('niveau'))
                        ->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('id_auto', $n->id_auto)
                        ->first();


                $moy_coef = $rek->moy_coef;
                $coef_ue = $rek->coefficient;
                $moy_sem = ($coef_ue > 0) ? $moy_coef / $coef_ue : 0;


                //Obtenir le total de credit
                $rek = DB::table("note_ue")
                        ->select(DB::raw('sum(credit_obtenu) as credit_obtenu'))
                        ->where('niveau', session('niveau'))
                        ->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('id_auto', $n->id_auto)
                        ->first();

                $total_credit = $rek->credit_obtenu;

                //ON VERIFIE L'EXISTANCE DE LA LIGNE
                $check = DB::table("pv_semestre")
                            ->where('niveau_etude', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('id_auto', $n->id_auto)->first();

                //ON RECHERHCE LES VALEURS DE PARCOURS DANS LA TABLE inscription_semestrielle
                $i_semestrielle = DB::table("inscription_semestrielle")
                                    ->where('niveau_etude', session('niveau'))->where('code_rentree', session('rentree'))
                                    ->where('semestre', session('semestre'))->where('id_auto', $n->id_auto)
                                    ->first();

                //OBTENTION DE LA MENTION
                if ($moy_sem < 10) {
                    $mention = "Refusé";
                } elseif ($moy_sem >= 10 && $moy_sem < 12) {
                    $mention = "Passable";
                } elseif ($moy_sem >= 12 && $moy_sem < 14) {
                    $mention = "Assez Bien";
                } elseif ($moy_sem >= 14 && $moy_sem < 16) {
                    $mention = "Bien";
                } elseif ($moy_sem >= 16 && $moy_sem < 18) {
                    $mention = "Très Bien";
                } elseif ($moy_sem >= 18 && $moy_sem <= 20) {
                    $mention = "Excellent";
                }

                //OBTENTION DE LA DECISION
                if ($total_credit < 30) {
                    $decision = "REFUSE";
                } else {
                    $decision = "ADMIS";
                }
            }

            if (empty($check))
            {
                $data = array(
                    "annee_univ" => $i_semestrielle->annee_univ, "code_rentree" => session('rentree'), "id_auto" => $n->id_auto, "code_filiere" => $i_semestrielle->code_filiere,
                    "code_specialite" => $i_semestrielle->code_specialite,
                    "code_parcours" =>  $i_semestrielle->code_parcours, "niveau_etude" => session('niveau'), "diplome" => $i_semestrielle->diplome_prepare,
                    "semestre" => session('semestre'), "total_credit" => 30, "moyenne" => $moy_sem, "credit_obtenu" => $total_credit,
                    "decision_obtenu" => $decision, "mention_obtenu" => $mention, "session_compo" => session('session_compo'),
                    "annee_compo" => $i_semestrielle->annee_univ
                );

                $this->nano->saveData('pv_semestre', $data);
            }
            else
            {
                //on fait update
                $data = array(
                    "annee_univ" => $i_semestrielle->annee_univ, "code_rentree" => session('rentree'), "id_auto" => $n->id_auto, "code_filiere" => $i_semestrielle->code_filiere,
                    "code_specialite" => $i_semestrielle->code_specialite,
                    "code_parcours" =>  $i_semestrielle->code_parcours, "niveau_etude" => session('niveau'), "diplome" => $i_semestrielle->diplome_prepare,
                    "semestre" => session('semestre'), "total_credit" => 30, "moyenne" => $moy_sem, "credit_obtenu" => $total_credit,
                    "decision_obtenu" => $decision, "mention_obtenu" => $mention, "session_compo" => session('session_compo'),
                    "annee_compo" => $i_semestrielle->annee_univ
                );

                DB::table("pv_semestre")->where('niveau_etude', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))->where('code_specialite', $i_semestrielle->code_specialite)
                    ->where('id_auto', $n->id_auto)
                    ->update($data);

            }

            //MIS A JOUR DANS LA TABLE pv_synthese_parcours_sem
            //RECHERCHE LA LIGNE
            $check2 = DB::table("pv_synthese_parcours_sem")
                        ->where('niveau_etude', session('niveau'))
                        ->where('semestre', session('semestre'))
                        ->where('code_specialite', $i_semestrielle->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();
            // dd($check2);

            if (empty($check2))
            {
                // On l'enregistre
                $data = array(
                    "id_auto" => $n->id_auto, "code_filiere" => $i_semestrielle->code_filiere,
                    "code_specialite" => $i_semestrielle->code_specialite,
                    "niveau_etude" => session('niveau'), "diplome" => $i_semestrielle->diplome_prepare,
                    "credit_obtenu" => $total_credit,
                    "semestre" => session('semestre'),
                    "moyenne" => $moy_sem,
                    "voie_obtention_credit" => "EVALUATION"
                );

                $this->nano->saveData('pv_synthese_parcours_sem', $data);
            }
            else
            {
                //on fait update
                $data = array(
                    "id_auto" => $n->id_auto, "code_filiere" => $i_semestrielle->code_filiere,
                    "code_specialite" => $i_semestrielle->code_specialite,
                    "niveau_etude" => session('niveau'), "diplome" => $i_semestrielle->diplome_prepare,
                    "credit_obtenu" => $total_credit,
                    "semestre" => session('semestre'), "moyenne" => $moy_sem,
                    "voie_obtention_credit" => "EVALUATION"
                );

                DB::table("pv_synthese_parcours_sem")->where('niveau_etude', session('niveau'))
                    ->where('semestre', session('semestre'))
                    ->where('code_specialite', $i_semestrielle->code_specialite)
                    ->where('id_auto', $n->id_auto)
                    ->update($data);
            }


            //MISE A JOUR DANS LA TABLE SEMESTRE EVALUE
            $rek = DB::table("semestre_evalue")
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('id_auto', $n->id_auto)
                        ->where('code_parcours', $n->code_parcours)
                        ->first();

            if(!empty($rek))
            {
                if(session('session_compo') == "SESSION 1")
                {
                    if($total_credit == 30)
                    {
                        $data = array(
                            "calcul_session1" => true, "calcul_session2" => true,
                            "credit_obtenu" => $total_credit,
                        );
                    }
                    else
                    {
                        $data = array(
                            "calcul_session1" => true, "calcul_session2" => false,
                            "credit_obtenu" => $total_credit,
                        );
                    }

                    DB::table("semestre_evalue")->where('niveau_etude', session('niveau'))
                        ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))
                        ->where('id_auto', $n->id_auto)->where('code_parcours', $n->code_parcours)
                        ->update($data);
                }

                if(session('session_compo') == "SESSION 2")
                {
                    $data = array("calcul_session1" => true, "calcul_session2" => true,
                                 "credit_obtenu" => $total_credit, );

                    DB::table("semestre_evalue")->where('niveau_etude', session('niveau'))
                        ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))
                        ->where('id_auto', $n->id_auto)->where('code_parcours', $n->code_parcours)
                        ->update($data);
                }

            }
        }

        echo json_encode(array("status" => "1"));
    }

    //Calcule de la moyenne annuelle
    public function calcul_moy_annuelle()
    {
        $etudiant = DB::table("note_ecue")
                        ->select('code_rentree', 'code_parcours', 'niveau', 'semestre', 'id_auto')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->distinct()
                        ->get();

        foreach ($etudiant as $n)
        {
            //RECUPERATION DE LA VALEUR REDOUBLANT DANS LA TABLE inscription_semestrielle
            $inscription = DB::table("inscription_semestrielle")
                            ->where('niveau_etude', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('code_parcours', $n->code_parcours)
                            ->where('semestre', $n->semestre)
                            ->where('id_auto', $n->id_auto)
                            ->first();

            //Verifier si l'etudiant a obtenu les 60 credits
            $sql = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', session('niveau'))
                        ->where('id_auto', $n->id_auto)
                        ->first();

            if ($sql->credit_obtenu < 60)
            {
                $rek = DB::table("pv_synthese_parcours_sem")
                        ->select(DB::raw('sum(moyenne) as moyenne'), DB::raw('sum(credit_obtenu) as credit_obtenu'), 'id_auto')
                        ->where('niveau_etude', session('niveau'))
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();
                // ->sum('moyenne')
                // ->sum('credit_obtenu');
                // dd($rek->ToSql());

                $moy_sem = $rek->moyenne;
                $credit_obtenu = $rek->credit_obtenu;

                // Nombre de semestre dans l'année
                $nbre = 2;

                $moy_annuelle = ($nbre > 0) ? $moy_sem / $nbre : 0;


                //OBTENTION DE LA MENTION
                if ($moy_annuelle < 10) {
                    $mention = "Refusé";
                } elseif ($moy_annuelle >= 10 && $moy_annuelle < 12) {
                    $mention = "Passable";
                } elseif ($moy_annuelle >= 12 && $moy_annuelle < 14) {
                    $mention = "Assez Bien";
                } elseif ($moy_annuelle >= 14 && $moy_annuelle < 16) {
                    $mention = "Bien";
                } elseif ($moy_annuelle >= 16 && $moy_annuelle < 18) {
                    $mention = "Très Bien";
                } elseif ($moy_annuelle >= 18 && $moy_annuelle <= 20) {
                    $mention = "Excellent";
                }


                $redoublant = $inscription->redoublant;
                $niveau = session('niveau');

                //condition lancement decision
                $rek = DB::table("semestre_evalue")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                            ->orWhere('calcul_session1', '=', 0)
                            ->orWhere('calcul_session2', '=', 0)
                            ->get();

                if(!empty($rek))
                {
                    $decision_calcul = 0;
                }
                else
                {
                    $decision_calcul = 1;
                }

                //OBTENTION DE LA DECISION
                //LICENCE 1
                if ($niveau == "LICENCE 1")
                {
                    // NON REDOUBLANT
                    if ($redoublant == 0) {
                        if ($credit_obtenu == 60) {
                            $decision = "ADMIS";
                        }
                        if ($credit_obtenu >= 48 && $credit_obtenu < 60) {
                            $decision = "DEROGE";
                        }
                        if ($credit_obtenu < 48) {
                            $decision = "AJOURNE";
                        }
                    }
                    // REDOUBLANT
                    if ($redoublant == 1)
                    {
                        if ($credit_obtenu == 60) {
                            $decision = "ADMIS";
                        } else {
                            $decision = "REFUSE";
                        }
                    }
                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                                ->where('niveau_etude', "LICENCE 1")
                                ->where('code_specialite', $inscription->code_specialite)
                                ->where('code_rentree', $inscription->code_rentree)
                                ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2))
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 1", "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant, "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu, "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention, "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    }
                    else
                    {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 1", "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant, "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu, "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention, "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "LICENCE 1")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }

                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                                ->where('niveau_etude', "LICENCE 1")
                                ->where('code_specialite', $inscription->code_specialite)
                                ->where('id_auto', $n->id_auto)->first();

                    if(is_null($check))
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "niveau_etude" => "LICENCE 1", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);
                    }
                    else
                    {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "niveau_etude" => "LICENCE 1", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "LICENCE 1")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);
                    }
                }

                //LICENCE 2

                if ($niveau == "LICENCE 2")
                {
                    //VERIFICATION DE LA VALIDATION DE LA LICENCE 1
                    $validation_l1 = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "LICENCE 1")
                        ->where('code_specialite', $n->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();

                    $credit_obtenu_l1 = $validation_l1->credit_obtenu;
                    // A VALIDER LA LICENCE 1
                    if ($credit_obtenu_l1 == 60) {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            }
                            if ($credit_obtenu >= 48 && $credit_obtenu < 60) {
                                $decision = "DEROGE";
                            }
                            if ($credit_obtenu < 48) {
                                $decision = "AJOURNE";
                            }
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            } else {
                                $decision = "REFUSE";
                            }
                        }
                    } else {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            $decision = "REFUSE";
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            $decision = "REFUSE";
                        }
                    }

                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                        ->where('niveau_etude', "LICENCE 2")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('code_rentree', $inscription->code_rentree)
                        ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2)) {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 2",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 2",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "LICENCE 2")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }


                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "LICENCE 2")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();

                    if ($check == 0)
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "niveau_etude" => "LICENCE 2", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 2", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "LICENCE 2")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }
                }

                // LICENCE 3
                if ($niveau == "LICENCE 3") {
                    //VERIFICATION DE LA VALIDATION DE LA LICENCE 1
                    $validation_l1 = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "LICENCE 1")
                        ->where('code_specialite', $n->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();

                    //VERIFICATION DE LA VALIDATION DE LA LICENCE 2
                    $validation_l2 = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "LICENCE 2")
                        ->where('code_specialite', $n->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();

                    $credit_obtenu_l1 = $validation_l1->credit_obtenu;
                    $credit_obtenu_l2 = $validation_l2->credit_obtenu;
                    // A VALIDER LA LICENCE 2
                    if ($credit_obtenu_l1 == 60 && $credit_obtenu_l2 == 60) {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            }
                            if ($credit_obtenu < 60) {
                                $decision = "AJOURNE";
                            }
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            } else {
                                $decision = "REFUSE";
                            }
                        }
                    } else {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            $decision = "REFUSE";
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            $decision = "REFUSE";
                        }
                    }

                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                        ->where('niveau_etude', "LICENCE 3")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('code_rentree', $inscription->code_rentree)
                        ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2)) {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 3",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 3",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "LICENCE 3")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }

                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "LICENCE 3")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();

                    if ($check == 0)
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "niveau_etude" => "LICENCE 3", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "LICENCE 3", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "LICENCE 3")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }
                }

                //MASTER 1
                if ($niveau == "MASTER 1") {
                    // NON REDOUBLANT
                    if ($redoublant == 0) {
                        if ($credit_obtenu == 60) {
                            $decision = "ADMIS";
                        }
                        if ($credit_obtenu >= 48 && $credit_obtenu < 60) {
                            $decision = "DEROGE";
                        }
                        if ($credit_obtenu < 48) {
                            $decision = "AJOURNE";
                        }
                    }
                    // REDOUBLANT
                    if ($redoublant == 1) {
                        if ($credit_obtenu == 60) {
                            $decision = "ADMIS";
                        } else {
                            $decision = "REFUSE";
                        }
                    }

                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                        ->where('niveau_etude', "MASTER 1")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('code_rentree', $inscription->code_rentree)
                        ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2)) {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 1",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 1",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "MASTER 1")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }

                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "MASTER 1")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();

                    if ($check == 0)
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 1", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 1", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "MASTER 1")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }
                }

                //MASTER 2
                if ($niveau == "MASTER 2") {
                    //VERIFICATION DE LA VALIDATION DE LA LICENCE 1
                    $validation_m1 = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "MASTER 1")
                        ->where('code_specialite', $n->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();

                    $credit_obtenu_m1 = $validation_m1->credit_obtenu;
                    // A VALIDER LE MASTER 1
                    if ($credit_obtenu_m1 == 60) {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            }
                            if ($credit_obtenu < 60) {
                                $decision = "AJOURNE";
                            }
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            } else {
                                $decision = "REFUSE";
                            }
                        }
                    } else {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            $decision = "REFUSE";
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            $decision = "REFUSE";
                        }
                    }

                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                        ->where('niveau_etude', "MASTER 2")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('code_rentree', $inscription->code_rentree)
                        ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2)) {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 2",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 2",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "MASTER 2")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }

                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "MASTER 2")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();

                    if ($check == 0)
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 2", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "MASTER 2", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "MASTER 2")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }
                }

                //DOCTORAT 1
                if ($niveau == "DOCTORAT 1") {
                    // NON REDOUBLANT
                    if ($redoublant == 0) {
                        if ($credit_obtenu == 60) {
                            $decision = "ADMIS";
                        }
                        if ($credit_obtenu >= 48 && $credit_obtenu < 60) {
                            $decision = "DEROGE";
                        }
                        if ($credit_obtenu < 48) {
                            $decision = "AJOURNE";
                        }
                    }
                    // REDOUBLANT
                    if ($redoublant == 1) {
                        if ($credit_obtenu == 60) {
                            $decision = "ADMIS";
                        } else {
                            $decision = "REFUSE";
                        }
                    }

                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                        ->where('niveau_etude', "DOCTORAT 1")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('code_rentree', $inscription->code_rentree)
                        ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2)) {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 1",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 1",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "DOCTORAT 1")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }

                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "DOCTORAT 1")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();

                    if ($check == 0)
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 1", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 1", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "DOCTORAT 1")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);
                    }
                }

                //DOCTORAT 2
                if ($niveau == "DOCTORAT 2") {
                    //VERIFICATION DE LA VALIDATION DU DOCTORAT 1
                    $validation_d1 = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "DOCTORAT 1")
                        ->where('code_specialite', $n->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();

                    $credit_obtenu_d1 = $validation_d1->credit_obtenu;
                    // A VALIDER LE DOCTORAT 1
                    if ($credit_obtenu_d1 == 60) {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            }
                            if ($credit_obtenu >= 48 && $credit_obtenu < 60) {
                                $decision = "DEROGE";
                            }
                            if ($credit_obtenu < 48) {
                                $decision = "AJOURNE";
                            }
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            } else {
                                $decision = "REFUSE";
                            }
                        }
                    } else {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            $decision = "REFUSE";
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            $decision = "REFUSE";
                        }
                    }

                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                        ->where('niveau_etude', "DOCTORAT 2")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('code_rentree', $inscription->code_rentree)
                        ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2)) {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 2",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 2",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "DOCTORAT 1")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }

                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "DOCTORAT 2")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();

                    if ($check == 0)
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 2", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);
                        echo json_encode(array("status" => "1", "INSCRIPTION" => "PV SYNTHESE PARCOURS - DOCTORAT 2"));
                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 2", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "DOCTORAT 2")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }
                }

                // DOCTORAT 3
                if ($niveau == "DOCTORAT 3") {
                    //VERIFICATION DE LA VALIDATION DU DOCTORAT 1
                    $validation_d1 = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "DOCTORAT 1")
                        ->where('code_specialite', $n->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();

                    //VERIFICATION DE LA VALIDATION DE LA LICENCE 2
                    $validation_d2 = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "DOCTORAT 2")
                        ->where('code_specialite', $n->code_specialite)
                        ->where('id_auto', $n->id_auto)
                        ->first();

                    $credit_obtenu_d1 = $validation_d1->credit_obtenu;
                    $credit_obtenu_d2 = $validation_d2->credit_obtenu;
                    // A VALIDER LE DOCTORAT 2
                    if ($credit_obtenu_d1 == 60 && $credit_obtenu_d2 == 60) {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            }
                            if ($credit_obtenu < 60) {
                                $decision = "AJOURNE";
                            }
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            if ($credit_obtenu == 60) {
                                $decision = "ADMIS";
                            } else {
                                $decision = "REFUSE";
                            }
                        }
                    } else {

                        // NON REDOUBLANT
                        if ($redoublant == 0) {
                            $decision = "REFUSE";
                        }
                        // REDOUBLANT
                        if ($redoublant == 1) {
                            $decision = "REFUSE";
                        }
                    }

                    //MIS A JOUR DANS LA TABLE pv_annuel
                    //RECHERCHE LA LIGNE
                    $check2 = DB::table("pv_annuel")
                        ->where('niveau_etude', "DOCTORAT 3")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('code_rentree', $inscription->code_rentree)
                        ->where('id_auto', $n->id_auto)->first();

                    if (is_null($check2)) {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 3",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        $this->nano->saveData('pv_annuel', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 3",
                            "diplome" => $inscription->diplome_prepare,
                            "code_parcours" => $inscription->code_parcours,
                            "code_parcours" => $inscription->code_rentree,
                            "annee_univ" => $inscription->annee_univ,
                            "redoublant" => $inscription->redoublant,
                            "total_credit" => 60,
                            "credit_obtenu" => $credit_obtenu,
                            "mga" => $moy_annuelle,
                            "mention_obtenu" => $mention,
                            "decision_obtenu" => $decision,
                            "session_compo" => session('session_compo'),
                            "annee_compo" => $inscription->annee_univ
                        );

                        DB::table("pv_annuel")
                            ->where('niveau_etude', "DOCTORAT 3")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('code_rentree', $inscription->code_rentree)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }

                    //MIS A JOUR DANS LA TABLE pv_synthese_parcours
                    //RECHERCHE LA LIGNE
                    $check = DB::table("pv_synthese_parcours")
                        ->where('niveau_etude', "DOCTORAT 3")
                        ->where('code_specialite', $inscription->code_specialite)
                        ->where('id_auto', $n->id_auto)->first();

                    if ($check == 0)
                    {
                        // On l'enregistre
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 3", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        $this->nano->saveData('pv_synthese_parcours', $data);

                    } else {
                        //on fait update
                        $data = array(
                            "id_auto" => $n->id_auto, "code_filiere" => $inscription->code_filiere,
                            "code_specialite" => $inscription->code_specialite,
                            "niveau_etude" => "DOCTORAT 3", "diplome" => $inscription->diplome_prepare,
                            "credit_obtenu" => $credit_obtenu, "decision_obtenu"=> $decision, "calcul_decision"=> $decision_calcul,
                            "moyenne" => $moy_annuelle,
                            "voie_obtention_credit" => "EVALUATION"
                        );

                        DB::table("pv_synthese_parcours")->where('niveau_etude', "DOCTORAT 3")
                            ->where('code_specialite', $inscription->code_specialite)
                            ->where('id_auto', $n->id_auto)
                            ->update($data);

                    }
                }
            }
        }

        echo json_encode(array("status" => "1"));
    }

    public function get_decision()
    {
        $session_autorise = date('m')."-".date('Y');

        $etudiant = DB::table("note_ecue")
                        ->select('code_rentree', 'code_parcours', 'niveau', 'semestre', 'id_auto', 'code_ue')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->distinct()
                        ->get();

        foreach($etudiant as $n)
        {
            //on va chercher la derniere decision de l'etudiant
            $inscription = DB::table("inscription_autorises")
                            ->where('id_auto', $n->id_auto)
                            ->orderBy('id_autorisation', 'DESC')->skip(0)->take(1)->first();

            if(!empty($inscription))
            {
                $cp = DB::table("code_paiement")
                        ->where('id_autorisation', $inscription->id_autorisation)
                        ->where('etat_paiement', false)->get();

                if($cp->count() == 0)
                {
                    //condition lancement decision
                    $rek = DB::table("semestre_evalue")
                                ->where('code_specialite', $inscription->code_specialite)
                                ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                                ->orWhere('calcul_session1', '=', 0)
                                ->orWhere('calcul_session2', '=', 0)
                                ->get();

                    if($rek->count() > 0)
                    {
                        //on ne calcul pas de decision
                    }
                    else
                    {
                        //on calcul sa decision
                        //selon le cas: REFUSE
                        $rek = DB::table("pv_synthese_parcours")
                                ->where('code_specialite', $inscription->code_specialite)
                                ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                                ->where('decision_obtenu', "REFUSE")
                                ->get();

                        //CAS: REFUSE
                        if($rek->count() > 0)
                        {
                            $decision_finale = "REFUSE";

                            //RECHERCHE DU SEMESTRE AUTORISE POUR LA PROCHAINE ANNEE
                            $semestre_autorise = "";
                            $sql = DB::table("pv_synthese_parcours_sem")
                                        ->where('code_specialite', $inscription->code_specialite)
                                        ->where('id_auto', $n->id_auto)
                                        ->where('diplome', $inscription->diplome_prepare)
                                        ->where('credit_obtenu', "<", 30)
                                        ->orderBy('semestre', 'ASC')
                                        ->get();

                            foreach($sql as $s)
                            {
                                //on cacatene les semestres
                                $semestre_autorise .= $s->semestre;
                            }

                            //on recherche la ligne dans la table PV_DECISION
                            $ck = DB::table("pv_decision")
                                        ->where('code_rentree', session('rentree'))
                                        ->where('code_specialite', $inscription->code_specialite)
                                        ->where('id_auto', $n->id_auto)
                                        ->where('diplome', $inscription->diplome_prepare)
                                        ->first();

                            if(empty($ck))
                            {
                                //enregistrement dans la table
                                $data = array(
                                    "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                    "code_specialite" => $inscription->code_specialite,
                                    "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                    "semestre_autorise" => $semestre_autorise,
                                );

                                $this->nano->saveData('pv_decision', $data);
                            }
                            else
                            {
                                //UPDATE
                                $data = array(
                                    "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                    "code_specialite" => $inscription->code_specialite,
                                    "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                    "semestre_autorise" => $semestre_autorise,
                                );

                                DB::table("pv_decision")
                                    ->where('code_rentree', session('rentree'))
                                    ->where('code_specialite', $inscription->code_specialite)
                                    ->where('id_auto', $n->id_auto)
                                    ->where('diplome', $inscription->diplome_prepare)
                                    ->update($data);
                            }

                            //On recherche la ligne dans la table _inscription_autorises
                            $ck = DB::table("inscription_autorises")
                                        ->where('session', $session_autorise)
                                        ->where('code_specialite', $inscription->code_specialite)
                                        ->where('id_auto', $n->id_auto)
                                        ->where('diplome_prepare', $inscription->diplome_prepare)
                                        ->get();

                            if(empty($ck))
                            {
                                //info de l'etudiant
                                $etu = DB::table("etudiant")->where('id_auto', $n->id_auto);
                                $statut = ($etu->statut == 1) ? "ORIENTE":"NON-ORIENTE";

                                //OBTENIR LE NIVEAU_ASSOCIE
                                $niv = DB::table("parametre_tranche_paiement")
                                                ->select('semestre_autorise', 'diplome_prepare')
                                                ->where('semestre_autorise', $semestre_autorise)
                                                ->where('diplome_prepare', $inscription->diplome_prepare)
                                                ->distinct()
                                                ->first();

                                $niveau_associe = $niv->niveau_associe;
                                $niveau_sem_ass = $niv->niveau_semestre_associe;

                                //RECHERCHE DES FORMATION_AUTORISEE
                                $sql = DB::table("inscription_semestrielle")
                                                ->select('formation_autorise', 'type_formation', 'redoublant')
                                                ->where('id_auto', $n->id_auto)
                                                ->where('code_rentree', session('rentree'))->where('niveau_etude', session('niveau'))
                                                ->distinct()
                                                ->first();

                                $type_formation = $sql->type_formation; $formation_autorise = "FORMATION CONTINUE";

                                //enregistrement dans la table
                                $datax = array("annee_univ"=> session('annee_univ'), "matricule_mesrs"=> $etu->matricule_mesrs, "num_bac"=> $etu->num_table_bac,
                                            "session"=> $session_autorise, "annee_bac"=> $etu->annee_bac, "serie_bac"=>$etu->serie_bac,
                                            "code_filiere"=> "ISN", "nature_demande"=> "",
                                            "code_specialite"=> $inscription->code_specialite, "niveau_associe"=> $niveau_associe,
                                            'semestre_autorise'=> $semestre_autorise, 'niveau_semestre_associe' => $niveau_sem_ass,
                                            "diplome_prepare"=> $inscription->diplome_prepare, "formation_autorise"=> $formation_autorise,
                                            "type_formation"=> $type_formation, "modalite_paiement" => true,
                                            "statut_etudiant"=> $statut,  "statut_inscription"=> $etu->statut_inscription,
                                            "nom"=> $etu->nom, "prenoms"=> $etu->prenoms,
                                            "nom_prenoms"=> $etu->nom_prenoms, "datenaiss"=> $etu->datenaiss,
                                            "lieunaiss"=> $etu->lieunaiss, "genre"=> $etu->genre,
                                            "nationalite"=> $etu->nationalite, "email_perso"=> $etu->email_uvci,
                                            "tel_mobile"=> $etu->tel_mobile1,
                                            "created_by"=> "", "date_saved"=> date('Y-m-d H:i:s'),);

                                $this->nano->saveData('inscription_autorises', $datax);
                            }

                        }
                        else
                        {
                            //selon le cas: AJOURNE
                            $rek = DB::table("pv_synthese_parcours")
                                    ->where('code_specialite', $inscription->code_specialite)
                                    ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                                    ->where('decision_obtenu', "AJOURNE")
                                    ->get();

                            //CAS : AJOURNE
                            if(!empty($rek))
                            {
                                $decision_finale = "AJOURNE";

                                //RECHERCHE DU SEMESTRE AUTORISE POUR LA PROCHAINE ANNEE
                                $semestre_autorise = "";
                                $sql = DB::table("pv_synthese_parcours_sem")
                                            ->where('code_specialite', $inscription->code_specialite)
                                            ->where('id_auto', $n->id_auto)
                                            ->where('diplome', $inscription->diplome_prepare)
                                            ->where('credit_obtenu', "<", 30)
                                            ->orderBy('semestre', 'ASC')
                                            ->get();

                                foreach($sql as $s)
                                {
                                    //on cacatene les semestres
                                    $semestre_autorise .= $s->semestre;
                                }

                                //on recherche la ligne dans la table PV_DECISION
                                $ck = DB::table("pv_decision")
                                            ->where('code_rentree', session('rentree'))
                                            ->where('code_specialite', $inscription->code_specialite)
                                            ->where('id_auto', $n->id_auto)
                                            ->where('diplome', $inscription->diplome_prepare)
                                            ->first();

                                if(empty($ck))
                                {
                                    //enregistrement dans la table
                                    $data = array(
                                        "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                        "code_specialite" => $inscription->code_specialite,
                                        "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                        "semestre_autorise" => $semestre_autorise,
                                    );

                                    $this->nano->saveData('pv_decision', $data);
                                }
                                else
                                {
                                    //UPDATE
                                    $data = array(
                                        "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                        "code_specialite" => $inscription->code_specialite,
                                        "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                        "semestre_autorise" => $semestre_autorise,
                                    );

                                    DB::table("pv_decision")
                                        ->where('code_rentree', session('rentree'))
                                        ->where('code_specialite', $inscription->code_specialite)
                                        ->where('id_auto', $n->id_auto)
                                        ->where('diplome', $inscription->diplome_prepare)
                                        ->update($data);
                                }

                                //On recherche la ligne dans la table _inscription_autorises
                                $ck = DB::table("inscription_autorises")
                                            ->where('session', $session_autorise)
                                            ->where('code_specialite', $inscription->code_specialite)
                                            ->where('id_auto', $n->id_auto)
                                            ->where('diplome_prepare', $inscription->diplome_prepare)
                                            ->get();

                                if(empty($ck))
                                {
                                    //info de l'etudiant
                                    $etu = DB::table("etudiant")->where('id_auto', $n->id_auto);
                                    $statut = ($etu->statut == 1) ? "ORIENTE":"NON-ORIENTE";

                                    //OBTENIR LE NIVEAU_ASSOCIE
                                    $niv = DB::table("parametre_tranche_paiement")
                                                    ->select('semestre_autorise', 'diplome_prepare')
                                                    ->where('semestre_autorise', $semestre_autorise)
                                                    ->where('diplome_prepare', $inscription->diplome_prepare)
                                                    ->distinct()
                                                    ->first();

                                    $niveau_associe = $niv->niveau_associe;
                                    $niveau_sem_ass = $niv->niveau_semestre_associe;

                                    //RECHERCHE DES FORMATION_AUTORISEE
                                    $sql = DB::table("inscription_semestrielle")
                                                    ->select('formation_autorise', 'type_formation', 'redoublant')
                                                    ->where('id_auto', $n->id_auto)
                                                    ->where('code_rentree', session('rentree'))->where('niveau_etude', session('niveau'))
                                                    ->distinct()
                                                    ->first();

                                    $type_formation = $sql->type_formation;
                                    $formation_autorise = $sql->formation_autorise;

                                    //enregistrement dans la table
                                    $datax = array("annee_univ"=> session('annee_univ'), "matricule_mesrs"=> $etu->matricule_mesrs, "num_bac"=> $etu->num_table_bac,
                                                "session"=> $session_autorise, "annee_bac"=> $etu->annee_bac, "serie_bac"=>$etu->serie_bac,
                                                "code_filiere"=> "ISN", "nature_demande"=> "",
                                                "code_specialite"=> $inscription->code_specialite, "niveau_associe"=> $niveau_associe,
                                                'semestre_autorise'=> $semestre_autorise, 'niveau_semestre_associe' => $niveau_sem_ass,
                                                "diplome_prepare"=> $inscription->diplome_prepare, "formation_autorise"=> $formation_autorise,
                                                "type_formation"=> $type_formation, "modalite_paiement" => true,
                                                "statut_etudiant"=> $statut,  "statut_inscription"=> $etu->statut_inscription,
                                                "nom"=> $etu->nom, "prenoms"=> $etu->prenoms,
                                                "nom_prenoms"=> $etu->nom_prenoms, "datenaiss"=> $etu->datenaiss,
                                                "lieunaiss"=> $etu->lieunaiss, "genre"=> $etu->genre,
                                                "nationalite"=> $etu->nationalite, "email_perso"=> $etu->email_uvci,
                                                "tel_mobile"=> $etu->tel_mobile1,
                                                "created_by"=> "", "date_saved"=> date('Y-m-d H:i:s'),);

                                    $this->nano->saveData('inscription_autorises', $datax);
                                }
                            }
                            else
                            {
                                //selon le cas: DEROGE
                                $rek = DB::table("pv_synthese_parcours")
                                        ->where('code_specialite', $inscription->code_specialite)
                                        ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                                        ->where('decision_obtenu', "DEROGE")
                                        ->get();

                                //CAS: DEROGE
                                if(!empty($rek))
                                {
                                    $decision_finale = "DEROGE";

                                    //RECHERCHE DU SEMESTRE AUTORISE POUR LA PROCHAINE ANNEE
                                    $semestre_autorise = "";
                                    $sql = DB::table("pv_synthese_parcours_sem")
                                                ->where('code_specialite', $inscription->code_specialite)
                                                ->where('id_auto', $n->id_auto)
                                                ->where('diplome', $inscription->diplome_prepare)
                                                ->where('credit_obtenu', "<", 30)
                                                ->orderBy('semestre', 'ASC')
                                                ->get();

                                    foreach($sql as $s)
                                    {
                                        //on cacatene les semestres
                                        $semestre_autorise .= $s->semestre;
                                    }

                                    $rek = DB::table("pv_synthese_parcours")
                                            ->where('code_specialite', $inscription->code_specialite)
                                            ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                                            ->where('decision_obtenu', "DEROGE")
                                            ->orderBy('niveau_etude', 'DESC')->skip(0)->take(1)->first();

                                    if(!empty($rek))
                                    {
                                        if($rek->niveau_etude == "LICENCE 1")
                                        {
                                            $semestre_autorise .= "S3S4";
                                        }

                                        if($rek->niveau_etude == "LICENCE 2")
                                        {
                                            $semestre_autorise .= "S5S6";
                                        }

                                        if($rek->niveau_etude == "MASTER 1")
                                        {
                                            $semestre_autorise .= "S3S4";
                                        }
                                    }

                                    //on recherche la ligne dans la table PV_DECISION
                                    $ck = DB::table("pv_decision")
                                                ->where('code_rentree', session('rentree'))
                                                ->where('code_specialite', $inscription->code_specialite)
                                                ->where('id_auto', $n->id_auto)
                                                ->where('diplome', $inscription->diplome_prepare)
                                                ->first();

                                    if(empty($ck))
                                    {
                                        //enregistrement dans la table
                                        $data = array(
                                            "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                            "code_specialite" => $inscription->code_specialite,
                                            "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                            "semestre_autorise" => $semestre_autorise,
                                        );

                                        $this->nano->saveData('pv_decision', $data);
                                    }
                                    else
                                    {
                                        //UPDATE
                                        $data = array(
                                            "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                            "code_specialite" => $inscription->code_specialite,
                                            "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                            "semestre_autorise" => $semestre_autorise,
                                        );

                                        DB::table("pv_decision")
                                            ->where('code_rentree', session('rentree'))
                                            ->where('code_specialite', $inscription->code_specialite)
                                            ->where('id_auto', $n->id_auto)
                                            ->where('diplome', $inscription->diplome_prepare)
                                            ->update($data);
                                    }

                                    //On recherche la ligne dans la table _inscription_autorises
                                    $ck = DB::table("inscription_autorises")
                                                ->where('session', $session_autorise)
                                                ->where('code_specialite', $inscription->code_specialite)
                                                ->where('id_auto', $n->id_auto)
                                                ->where('diplome_prepare', $inscription->diplome_prepare)
                                                ->get();

                                    if(empty($ck))
                                    {
                                        //info de l'etudiant
                                        $etu = DB::table("etudiant")->where('id_auto', $n->id_auto);
                                        $statut = ($etu->statut == 1) ? "ORIENTE":"NON-ORIENTE";

                                        //OBTENIR LE NIVEAU_ASSOCIE
                                        $niv = DB::table("parametre_tranche_paiement")
                                                        ->select('semestre_autorise', 'diplome_prepare')
                                                        ->where('semestre_autorise', $semestre_autorise)
                                                        ->where('diplome_prepare', $inscription->diplome_prepare)
                                                        ->distinct()
                                                        ->first();

                                        $niveau_associe = $niv->niveau_associe;
                                        $niveau_sem_ass = $niv->niveau_semestre_associe;

                                        //RECHERCHE DES FORMATION_AUTORISEE
                                        $sql = DB::table("inscription_semestrielle")
                                                        ->select('formation_autorise', 'type_formation', 'redoublant')
                                                        ->where('id_auto', $n->id_auto)
                                                        ->where('code_rentree', session('rentree'))->where('niveau_etude', session('niveau'))
                                                        ->distinct()
                                                        ->first();

                                        $type_formation = $sql->type_formation;
                                        $formation_autorise = $sql->formation_autorise;

                                        //enregistrement dans la table
                                        $datax = array("annee_univ"=> session('annee_univ'), "matricule_mesrs"=> $etu->matricule_mesrs, "num_bac"=> $etu->num_table_bac,
                                                    "session"=> $session_autorise, "annee_bac"=> $etu->annee_bac, "serie_bac"=>$etu->serie_bac,
                                                    "code_filiere"=> "ISN", "nature_demande"=> "",
                                                    "code_specialite"=> $inscription->code_specialite, "niveau_associe"=> $niveau_associe,
                                                    'semestre_autorise'=> $semestre_autorise, 'niveau_semestre_associe' => $niveau_sem_ass,
                                                    "diplome_prepare"=> $inscription->diplome_prepare, "formation_autorise"=> $formation_autorise,
                                                    "type_formation"=> $type_formation, "modalite_paiement" => true,
                                                    "statut_etudiant"=> $statut,  "statut_inscription"=> $etu->statut_inscription,
                                                    "nom"=> $etu->nom, "prenoms"=> $etu->prenoms,
                                                    "nom_prenoms"=> $etu->nom_prenoms, "datenaiss"=> $etu->datenaiss,
                                                    "lieunaiss"=> $etu->lieunaiss, "genre"=> $etu->genre,
                                                    "nationalite"=> $etu->nationalite, "email_perso"=> $etu->email_uvci,
                                                    "tel_mobile"=> $etu->tel_mobile1,
                                                    "created_by"=> "", "date_saved"=> date('Y-m-d H:i:s'),);

                                        $this->nano->saveData('inscription_autorises', $datax);
                                    }

                                }
                                else
                                {
                                    //selon le cas: ADMIS
                                    $rek = DB::table("pv_synthese_parcours")
                                            ->where('code_specialite', $inscription->code_specialite)
                                            ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                                            ->where('decision_obtenu', "ADMIS")
                                            ->get();

                                    if(!empty($rek))
                                    {
                                        $decision_finale = "ADMIS";

                                        $rek = DB::table("pv_synthese_parcours")
                                                ->where('code_specialite', $inscription->code_specialite)
                                                ->where('id_auto', $n->id_auto)->where('diplome', $inscription->diplome_prepare)
                                                ->where('decision_obtenu', "ADMIS")
                                                ->orderBy('niveau_etude', 'DESC')->skip(0)->take(1)->first();

                                        if(!empty($rek))
                                        {
                                            if($rek->niveau_etude == "LICENCE 1")
                                            {
                                                $semestre_autorise = "S3S4";
                                            }

                                            if($rek->niveau_etude == "LICENCE 2")
                                            {
                                                $semestre_autorise = "S5S6";
                                            }

                                            if($rek->niveau_etude == "LICENCE 3")
                                            {
                                                $semestre_autorise = "";
                                            }

                                            if($rek->niveau_etude == "MASTER 1")
                                            {
                                                $semestre_autorise = "S3S4";
                                            }

                                            if($rek->niveau_etude == "MASTER 2")
                                            {
                                                $semestre_autorise = "";
                                            }

                                            if($rek->niveau_etude == "DOCTORAT 1")
                                            {
                                                $semestre_autorise = "S3S4";
                                            }

                                            if($rek->niveau_etude == "DOCTORAT 2")
                                            {
                                                $semestre_autorise = "S5S6";
                                            }

                                            if($rek->niveau_etude == "DOCTORAT 3")
                                            {
                                                $semestre_autorise = "";
                                            }
                                        }

                                        //on recherche la ligne dans la table PV_DECISION
                                        $ck = DB::table("pv_decision")
                                                    ->where('code_rentree', session('rentree'))
                                                    ->where('code_specialite', $inscription->code_specialite)
                                                    ->where('id_auto', $n->id_auto)
                                                    ->where('diplome', $inscription->diplome_prepare)
                                                    ->first();

                                        if(empty($ck))
                                        {
                                            //enregistrement dans la table
                                            $data = array(
                                                "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                                "code_specialite" => $inscription->code_specialite,
                                                "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                                "semestre_autorise" => $semestre_autorise,
                                            );

                                            $this->nano->saveData('pv_decision', $data);
                                        }
                                        else
                                        {
                                            //UPDATE
                                            $data = array(
                                                "annee_univ" => session('annee_univ'), "code_rentree" => session('rentree'), "id_auto" => $n->id_auto,
                                                "code_specialite" => $inscription->code_specialite,
                                                "diplome" =>  $inscription->diplome_prepare, "decision" => $decision_finale, "session" => $session_autorise,
                                                "semestre_autorise" => $semestre_autorise,
                                            );

                                            DB::table("pv_decision")
                                                ->where('code_rentree', session('rentree'))
                                                ->where('code_specialite', $inscription->code_specialite)
                                                ->where('id_auto', $n->id_auto)
                                                ->where('diplome', $inscription->diplome_prepare)
                                                ->update($data);
                                        }

                                        //On recherche la ligne dans la table _inscription_autorises
                                        $ck = DB::table("inscription_autorises")
                                                    ->where('session', $session_autorise)
                                                    ->where('code_specialite', $inscription->code_specialite)
                                                    ->where('id_auto', $n->id_auto)
                                                    ->where('diplome_prepare', $inscription->diplome_prepare)
                                                    ->get();

                                        if(empty($ck))
                                        {
                                            //info de l'etudiant
                                            $etu = DB::table("etudiant")->where('id_auto', $n->id_auto);
                                            $statut = ($etu->statut == 1) ? "ORIENTE":"NON-ORIENTE";

                                            //OBTENIR LE NIVEAU_ASSOCIE
                                            $niv = DB::table("parametre_tranche_paiement")
                                                            ->select('semestre_autorise', 'diplome_prepare')
                                                            ->where('semestre_autorise', $semestre_autorise)
                                                            ->where('diplome_prepare', $inscription->diplome_prepare)
                                                            ->distinct()
                                                            ->first();

                                            $niveau_associe = (!empty($niv)) ? $niv->niveau_associe : "";
                                            $niveau_sem_ass = (!empty($niv)) ? $niv->niveau_semestre_associe : "";

                                            //RECHERCHE DES FORMATION_AUTORISEE
                                            $sql = DB::table("inscription_semestrielle")
                                                            ->select('formation_autorise', 'type_formation', 'redoublant')
                                                            ->where('id_auto', $n->id_auto)
                                                            ->where('code_rentree', session('rentree'))->where('niveau_etude', session('niveau'))
                                                            ->distinct()
                                                            ->first();

                                            if($statut == "ORIENTE")
                                            {
                                                $formation_autorise = "FORMATION INITIALE" ;
                                            }
                                            else
                                            {
                                                $formation_autorise = "FORMATION CONTINUE" ;
                                            }

                                            $type_formation = (!empty($sql)) ? $sql->type_formation : "";

                                            //enregistrement dans la table
                                            $datax = array("annee_univ"=> session('annee_univ'), "matricule_mesrs"=> $etu->matricule_mesrs, "num_bac"=> $etu->num_table_bac,
                                                        "session"=> $session_autorise, "annee_bac"=> $etu->annee_bac, "serie_bac"=>$etu->serie_bac,
                                                        "code_filiere"=> "ISN", "nature_demande"=> "",
                                                        "code_specialite"=> $inscription->code_specialite, "niveau_associe"=> $niveau_associe,
                                                        'semestre_autorise'=> $semestre_autorise, 'niveau_semestre_associe' => $niveau_sem_ass,
                                                        "diplome_prepare"=> $inscription->diplome_prepare, "formation_autorise"=> $formation_autorise,
                                                        "type_formation"=> $type_formation, "modalite_paiement" => true,
                                                        "statut_etudiant"=> $statut,  "statut_inscription"=> $etu->statut_inscription,
                                                        "nom"=> $etu->nom, "prenoms"=> $etu->prenoms,
                                                        "nom_prenoms"=> $etu->nom_prenoms, "datenaiss"=> $etu->datenaiss,
                                                        "lieunaiss"=> $etu->lieunaiss, "genre"=> $etu->genre,
                                                        "nationalite"=> $etu->nationalite, "email_perso"=> $etu->email_uvci,
                                                        "tel_mobile"=> $etu->tel_mobile1,
                                                        "created_by"=> "", "date_saved"=> date('Y-m-d H:i:s'),);

                                            $this->nano->saveData('inscription_autorises', $datax);
                                        }
                                    }
                                }
                            }
                        }

                    }

                }
                else
                {
                    //on calcul pas de decision
                }
            }
        }

        echo json_encode(array("status" => "1"));
    }

    /**** USER */
    public function add_user(Request $request)
    {
        $btn_value = $request->input('Enregistrer');

        if ($btn_value == "Enregistrer") {
            $check = DB::table('delib_user')->where('email', $request->input('email'))->count();
            if ($check == 0) {
                $data = array(
                    "nom" => strtoupper($request->input('nom')), "prenoms" => strtoupper($request->input('prenoms')), "email" => $request->input('email'),
                    "password" => $request->input('password'), "statut" => $request->input('statut'), "admin" => $request->input('admin'),
                    "jury" => $request->input('jury'), "date_created" => date('Y-m-d H:i:s', time())
                );

                $this->nano->saveData('delib_user', $data);

                return back()->withSuccess("Utilisateur ajouté avec succès.");
            } else {
                return back()->withError('Cet utilisateur existe déjà.');
            }
        } else {
            $data = array(
                "nom" => strtoupper($request->input('nom')), "prenoms" => strtoupper($request->input('prenoms')), "email" => $request->input('email'),
                "password" => $request->input('password'), "statut" => $request->input('statut'), "admin" => $request->input('admin'),
                "jury" => $request->input('jury'),
            );

            DB::table("delib_user")->where('id_user', $request->input('id'))->update($data);

            return back()->withSuccess("Utilisateur modifé avec succès.");
        }
    }

    public function get_user($id)
    {
        $data = DB::table('delib_user')->where('id_user', $id)->first();

        echo json_encode($data);
    }

    public function delete_user($id)
    {
        $data = DB::table('delib_user')->where('id_user', $id)->delete();

        echo json_encode(array("status" => true));
    }
    /**** FIN USER */

    public function load_table_note2()
    {
        //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
        $etudiant = DB::table('delib_etudiant')
            ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
            ->where('specialite', "ISN-DAS")->whereRaw('id_auto <> "" ')->get();

        foreach ($etudiant as $i => $d) {
            $specialite = $d->specialite;

            //ON BOUCLE PAR MATIERE
            $matieres = DB::table('delib_matiere')->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))->where('etat', true)->where('specialite', $specialite)->get();
            foreach ($matieres as $matieres) {
                //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
                $nb_devoir = DB::table('v_note_devoir')
                    ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                    ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

                $nb_examen = DB::table('v_note_examen')
                    ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                    ->where('specialite', $specialite)->where('email', $d->email)->count();

                if ($nb_devoir >= 1 || $nb_examen > 0) {
                    //on va enregistrer la ligne de l'étudiant
                    $examen = DB::table('v_note_examen')
                        ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                        ->where('nom_matiere', 'like', "%" . $matieres->libelle . "%")
                        ->where('specialite', $specialite)->where('email', $d->email)->first();

                    //coefficient (nbre de note)
                    $coef = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE  delib_devoir.niveau="' . session('niveau') . '" AND delib_devoir.code_matiere="' . $matieres->code_matiere . '" AND delib_etudiant.specialite="' . $specialite . '" AND delib_etudiant.annee_univ="' . session('annee_univ') . '" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');

                    $note_examen = (!empty($examen)) ? $examen->note : "";
                    $coeff = ($coef->nombre != null) ? $coef->nombre : 0;
                    var_dump($i);

                    $datas = array(
                        "annee_univ" => session('annee_univ'), "niveau" => session('niveau'), "id_auto" => $d->id_auto,
                        "code_matiere" => $matieres->code_matiere, "nom_matiere" => $matieres->libelle, "specialite" => $d->specialite,
                        "note_examen" => $note_examen, "coef" => $coeff
                    );

                    $assoc = DB::table('delib_note')
                        ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                        ->where('code_matiere', $matieres->code_matiere)->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                    if (empty($assoc)) {
                        //on enregistre la ligne
                        $this->nano->saveData('delib_note', $datas);
                    } else {
                        //on fait un update
                        DB::table("delib_note")
                            ->where('niveau', session('niveau'))
                            ->where('annee_univ', session('annee_univ'))
                            ->where('code_matiere', $matieres->code_matiere)
                            ->where('specialite', $specialite)
                            ->where('id_auto', $d->id_auto)
                            ->update($datas);
                    }

                    //on boucle au moins sur 5 devoirs
                    for ($i = 1; $i < 6; $i++) {
                        $evaluation = "Devoir 0" . $i;

                        $devoir = DB::table('v_note_devoir')
                            ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                            ->where('code_matiere', $matieres->code_matiere)->where('evaluation', $evaluation)
                            ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->first();

                        $note_devoir = (!empty($devoir)) ? $devoir->note : "";
                        $etat_devoir = (!empty($devoir)) ? $devoir->etat : "";

                        $datax = array("note" . $i => $note_devoir, "etat" . $i => $etat_devoir,);

                        //on fait un update
                        DB::table("delib_note")
                            ->where('niveau', session('niveau'))
                            ->where('annee_univ', session('annee_univ'))
                            ->where('code_matiere', $matieres->code_matiere)
                            ->where('specialite', $specialite)
                            ->where('id_auto', $d->id_auto)
                            ->update($datax);
                    }
                }
            }
        }

        echo json_encode(array("status" => "1"));
    }

    function mention($note)
    {
        $mention = "";
        if ($note < 10) {
            $mention = "INSUFFISANT";
        } else if ($note >= 10 && $note < 12) {
            $mention = "PASSABLE";
        } else if ($note >= 12 && $note < 14) {
            $mention = "ASSEZ BIEN";
        } else if ($note >= 14 && $note < 16) {
            $mention = "BIEN";
        } else if ($note >= 16 && $note < 18) {
            $mention = "TRES BIEN";
        } else if ($note >= 18) {
            $mention = "EXCELLENT";
        }

        return $mention;
    }

    public function valider_resultat()
    {
        $inscription = DB::table('inscription_semestriel')
                        ->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))
                        ->where('specialite', "ISN-DAS")->whereRaw('id_auto <> "" ')->get();
    }
}
