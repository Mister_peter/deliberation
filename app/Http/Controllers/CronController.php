<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nano;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CronController extends Controller
{
    protected $nano;

	public function __construct()
	{
		$this->nano = new Nano;
	}

    /*********************** PLATEFORME SEMESTRIELLE ********************* */

    //recuperation des etudiants et cours
    public function cron_process1()
    {
        set_time_limit(0);

        $params = DB::table('delib_parametre')->where('cron', true)->get();
        foreach($params as $param)
        {
            $session_var = $param->session;
            $niveau_lien = str_replace(' ', '',  strtolower($param->niveau));
            $niveau = $param->niveau;
            $sem_lien = strtolower($param->semestre);
            $semestre = $param->semestre;
            $rentree = $param->code_rentree;
            $annee_univ = $param->annee_univ;


            //LES ETUDIANTS A RECUPERER
            if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
            {
                //on vide la table
                $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", $niveau, $rentree, $semestre);

                //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
                $etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto
                                                FROM note_ecue WHERE niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_rentree="'.$rentree.'" '));

                foreach($etudiant as $e)
                {
                    //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
                    //recuperer tout le contenu du webservice
                    $result = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apicron/get_student/'.$rentree.'/'.$semestre.'/'.$e->id_auto, false);
                    $json   = json_decode($result, true);

                    //boucle des données avec la clé (cours) du resultat_json
                    if(!empty($json['etudiants']))
                    {
                        foreach($json['etudiants'] as $d)
                        {
                            //var_dump($d['id_cours']); exit;
                            $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=> $niveau,
                                            'parcours'=>$d['niveau'], 'semestre'=> $semestre, 'specialite'=> $d['specialite'],
                                            'annee_univ'=> $annee_univ, 'code_rentree'=> $d['code_rentree'],
                                            'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_etudiant', $datas);
                        }
                    }
                }
            }

            //LES MATIERES(ECUE) ACTIVES SUR CAMPUS
            if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
            {
                //on vide la table
                $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", $niveau, $rentree, $semestre);

                //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
                //Recuperer tout le contenu du webservice
                $result = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apicron/get_course/'.$rentree.'/'.$semestre, false);
                $json   = json_decode($result, true);

                if(!empty($json['cours']))
                {
                    foreach($json['cours'] as $d)
                    {
                        $spec = explode("-", $d['specialite']);
                        $nb = count($spec);

                        for($i=0; $i<$nb; $i++)
                        {
                            $semestres = $d['semestre'];
                            if($semestre == $semestres)
                            {
                                $datas = array('code_rentree'=>$rentree, 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=> $niveau,
                                                'annee_univ'=>$annee_univ, 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=>$semestre);

                                //enregistrement des données dans la table matiere
                                $this->nano->saveData('delib_matiere', $datas);
                            }
                        }
                    }
                }

            }
        }

    }

    //recuperation des notes de devoirs et compo
    public function cron_process2()
    {
        set_time_limit(0);

        $params = DB::table('delib_parametre')->where('cron', true)->get();
        foreach($params as $param)
        {
            $session_var = $param->session;
            $niveau_lien = str_replace(' ', '',  strtolower($param->niveau));
            $niveau = $param->niveau;
            $sem_lien = strtolower($param->semestre);
            $semestre = $param->semestre;
            $rentree = $param->code_rentree;
            $annee_univ = $param->annee_univ;

            if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
            {
                //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
                //on vide la table pour la session <> session2
                //$this->nano->deleteDatas4_("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", $niveau, $rentree, $semestre, "SESSION 2");
                //$this->nano->deleteDatas4_("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", $niveau, $rentree, $semestre, "SESSION 2");

                //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
                $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apicron/get_note/'.$rentree.'/'.$semestre.'/'.$session_var, false);
                $json   = json_decode($results, true);

                if(!empty($json['note']))
                {
                    //SI L'ECUE contient des notes
                    if($json['success'] == true)
                    {
                        foreach($json['note'] as $d)
                        {
                            $matiere = DB::table('delib_matiere')
                                        ->where('niveau', $niveau)->where('code_rentree', $rentree)->where('specialite', $d['specialite'])
                                        ->where('semestre', $semestre)->where('code_matiere', $d['id_matiere'])
                                        ->groupBy('code_matiere')->first();

                            if(!empty($matiere))
                            {
                                $pedagogie = DB::selectOne('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue, ue_valide
                                                            FROM note_ecue WHERE id_auto="'.$d['id_auto'].'" AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_rentree="'.$rentree.'"
                                                            AND reference_ecue="'.$matiere->reference.'" ');


                                if(!empty($pedagogie))
                                {
                                    if($pedagogie->ue_valide == 0)
                                    {
                                        // on check pour voir si le libelle commence par D ou S
                                        if(substr(trim($d['num_devoir']),0,1) == "D")
                                        {
                                            $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                            $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                        }
                                        elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                        {
                                            $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                            $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                        }

                                        if($devoir !=  "SESSION 2")
                                        {
                                            $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> $semestre, "reference"=> $matiere->reference,
                                                            'id_auto'=> $d['id_auto'], 'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=> $niveau, 'specialite'=>$d['specialite'], 'annee_univ'=> $annee_univ);

                                            //enregistrement des données dans la table note
                                            $save = $this->nano->saveData('delib_devoir_temp', $datas);
                                            if($save == 1)
                                            {
                                                $etat = 1;
                                                $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apicron/update_etat_note/'.$d['id_note'].'/'.$rentree.'/'.$etat, false);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $etat = 3;
                                        $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apicron/update_etat_note/'.$d['id_note'].'/'.$rentree.'/'.$etat, false);
                                    }
                                }
                                else
                                {
                                    $etat = 2;
                                    $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apicron/update_etat_note/'.$d['id_note'].'/'.$rentree.'/'.$etat, false);
                                }
                            }
                        }
                    }
                }
            }
            else //POUR LA SESSION 2
            {
                //on vide les tables
                // $this->nano->deleteDatas4("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", $niveau, $rentree, $semestre, "SESSION 2");
                // $this->nano->deleteDatas4("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", $niveau, $rentree, $semestre, "SESSION 2");

                //REQUETE PRINCIPALE: RECUPERATION DES ETUDIANTS CONCERNEES PAR LA DELIBERATION
                $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apicron/get_note/'.$rentree.'/'.$semestre.'/'.$session_var, false);
                $json   = json_decode($results, true);

                if(!empty($json['note']))
                {
                    //SI L'ECUE contient des notes
                    if($json['success'] == true)
                    {
                        foreach($json['note'] as $d)
                        {
                            $matiere = DB::table('delib_matiere')
                                            ->where('niveau', $niveau)->where('code_rentree', $rentree)
                                            ->where('etat', true)->where('semestre', $semestre)->where('code_matiere', $d['id_matiere'])
                                            ->groupBy('code_matiere')->first();
                            if(!empty($matiere))
                            {
                                $pedagogie = DB::selectOne('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                            FROM note_ecue WHERE id_auto="'.$d['id_auto'].'" AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_rentree="'.$rentree.'"
                                                            AND reference_ecue="'.$matiere->reference.'" AND moyenne<10 ');

                                if(!empty($pedagogie))
                                {
                                    // on check pour voir si le libelle commence par D ou S
                                    if(substr(trim($d['num_devoir']),0,1) == "D")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                    }
                                    elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                    {
                                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                        $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                    }

                                    if($devoir ==  "SESSION 2")
                                    {
                                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> $semestre, "reference"=> $matiere->reference,
                                                        'id_auto'=> $d['id_auto'], 'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=> $niveau, 'specialite'=>$d['specialite'], 'annee_univ'=> $annee_univ);

                                        //enregistrement des données dans la table matiere
                                        $this->nano->saveData('delib_devoir_temp', $datas);
                                        $save = $this->nano->saveData('delib_devoir', $datas);

                                        if($save == 1)
                                        {
                                            $etat = true;
                                            $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.$rentree.'/'.$etat, false);
                                        }
                                    }
                                }
                                else
                                {
                                    $etat = 2;
                                    $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/apis/update_etat_note/'.$d['id_note'].'/'.$rentree.'/'.$etat, false);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
    //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
    public function cron_process4()
    {
        set_time_limit(0);

        $params = DB::table('delib_parametre')->where('cron', true)->get();
        foreach($params as $param)
        {
            $session_var = $param->session;
            $niveau_lien = str_replace(' ', '',  strtolower($param->niveau));
            $niveau = $param->niveau;
            $sem_lien = strtolower($param->semestre);
            $semestre = $param->semestre;
            $rentree = $param->code_rentree;
            $annee_univ = $param->annee_univ;

            if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
            {
                $this->fill_miror_new($rentree, $niveau, $semestre, $annee_univ);
            }
        }
    }

    //fusion des notes en une seule ligne par matiere |12|5|11.22|
    public function cron_process3()
    {
        set_time_limit(0);

        $params = DB::table('delib_parametre')->where('cron', true)->get();
        foreach($params as $param)
        {
            $session = $param->session;
            $niveau = $param->niveau;
            $semestre = $param->semestre;
            $rentree = $param->code_rentree;
            $annee_univ = $param->annee_univ;

            if($session == "SESSION 1")
            {
                //SUPRESSION DES LIGNES
                $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", $niveau, $rentree, $semestre, $session);
                $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", $niveau, $rentree, $semestre, $session);

                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                        FROM note_ecue WHERE niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND
                                                        code_rentree="'.$rentree.'"  AND recuperation=1 AND reference_ecue<>"" '));

                foreach($liste_etudiant as $etu)
                {
                    $d = DB::table('v_etudiant_matiere')
                                        ->where('niveau', $niveau)
                                        ->where('code_rentree', $rentree)->where('semestre', $semestre)->where('specialite', $etu->code_specialite)
                                        ->where('id_auto', $etu->id_auto)->where('reference', $etu->reference_ecue)
                                        ->first();

                    if(!empty($d))
                    {
                        $specialite = $d->specialite; $matiere = $d->libelle;

                        //on va recuperer la note d'examen de l'etudiant
                        $examen = DB::table('delib_devoir')
                                    ->where('niveau', $niveau)->where('evaluation', $session)
                                    ->where('code_rentree', $rentree)->where('semestre', $semestre)
                                    ->where('code_matiere', $d->code_matiere)
                                    ->where('specialite', $specialite)->where('id_auto', $etu->id_auto)->first();

                        //coefficient (nbre de note de devoir)
                        $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.$rentree.'"
                                                AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'"
                                                AND evaluation LIKE "%Devoir%" AND etat=0 GROUP BY code_user) AS nb');

                        $note_examen = (!empty($examen)) ? $examen->note : 0;
                        $etat_examen = (!empty($examen)) ? false : true;
                        $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                        $semestre_examen = (!empty($examen)) ? $examen->semestre : $semestre;
                        $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                        //NOTE DE DEVOIR
                        $note = DB::selectOne('SELECT SUM(note) AS note, code_matiere FROM delib_devoir WHERE code_rentree="'.$rentree.'"
                                                        AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND id_auto="'.$etu->id_auto.'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'"
                                                        AND evaluation LIKE "%Devoir%" GROUP BY code_matiere, id_auto');

                        $notes = (!empty($note)) ? (floor(($note->note*100))/100)/$coeff : 0;

                        $datas = array("code_rentree" => $rentree, "annee_univ" => $annee_univ, "niveau"=> $niveau, "id_auto"=>$d->id_auto,
                                        "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                        "note_devoir"=>$notes, "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "coef"=>$coeff, "reference"=> $d->reference );

                        $assoc = DB::table('delib_note')
                                    ->where('niveau', $niveau)->where('code_rentree', $rentree)->where('semestre', $semestre)
                                    ->where('session', $session)->where('code_matiere', $d->code_matiere)
                                    ->where('specialite', $specialite)->where('id_auto', $etu->id_auto)->first();

                        if(empty($assoc))
                        {
                            //on enregistre la ligne
                            $this->nano->saveData('delib_note', $datas);
                        }
                    }
                }
            }
            else
            {
                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::select(DB::raw('SELECT DISTINCT code_rentree, niveau, code_specialite, semestre, id_auto, reference_ecue
                                                        FROM note_ecue WHERE niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND
                                                        code_rentree="'.$rentree.'"  AND recuperation=1 AND reference_ecue<>"" AND moyenne<10 '));

                foreach($liste_etudiant as $etu)
                {
                    $d = DB::table('v_etudiant_matiere')
                                        ->where('niveau', $niveau)
                                        ->where('code_rentree', $rentree)->where('semestre', $semestre)->where('specialite', $etu->code_specialite)
                                        ->where('id_auto', $etu->id_auto)->where('reference', $etu->reference_ecue)
                                        ->first();

                    if(!empty($d))
                    {
                        $specialite = $d->specialite; $matiere = $d->libelle;

                        //on va recuperer la note d'examen de l'etudiant
                        $examen = DB::table('delib_devoir')
                                    ->where('niveau', $niveau)->where('evaluation', $session)
                                    ->where('code_rentree', $rentree)->where('semestre', $semestre)
                                    ->where('code_matiere', $d->code_matiere)
                                    ->where('specialite', $specialite)->where('id_auto', $etu->id_auto)->first();

                        $note_examen = (!empty($examen)) ? $examen->note : 0;
                        $etat_examen = (!empty($examen)) ? false : true;

                        $semestre_examen = (!empty($examen)) ? $examen->semestre : $semestre;
                        $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                        $datas = array("session"=> $session_examen, "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, );

                        if($note_examen != "")
                        {
                            $assoc = DB::table('delib_note')
                                        ->where('niveau', $niveau)->where('code_rentree', $rentree)->where('semestre', $semestre)
                                        ->where('code_matiere', $d->code_matiere)->where('specialite', $specialite)->where('id_auto', $etu->id_auto)
                                        ->update($datas);
                        }

                    }
                }
            }
        }
    }

    public function fill_miror_new($rentree, $niveau, $semestre, $annee_univ)
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', $niveau)->where('code_rentree', $rentree)
                        ->where('semestre', $semestre)->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
            $rek = DB::select(DB::raw('SELECT id_note, annee_univ, id_auto, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation, specialite
                                FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_rentree="'.$rentree.'" AND evaluation in (
                                        SELECT evaluation
                                            FROM delib_devoir_temp
                                            WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_rentree="'.$rentree.'"
                                        GROUP BY  evaluation, code_user
                                ) GROUP BY evaluation, code_user'));

            foreach($rek as $i => $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=> $rentree, 'semestre'=> $semestre, "reference"=> $matiere->reference, "id_auto"=> $d->id_auto,
                                'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> $niveau, 'specialite'=> $d->specialite, 'annee_univ'=> $annee_univ);

                if($d->evaluation != "SESSION 2")
                {
                    //enregistrement des données dans la table
                    $this->nano->saveData('delib_devoir', $datas);
                }
            }
        }
    }

    //pour la prise en compte des specialités
    public function fill_miror_new2($rentree, $niveau, $semestre, $annee_univ, $specialite)
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', $niveau)->where('code_rentree', $rentree)
                        ->where('specialite', $specialite)
                        ->where('semestre', $semestre)->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
            $rek = DB::select(DB::raw('SELECT id_note, annee_univ, id_auto, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation, specialite
                                FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$specialite.'" AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_rentree="'.$rentree.'" AND evaluation in (
                                        SELECT evaluation
                                            FROM delib_devoir_temp
                                            WHERE code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$specialite.'" AND niveau="'.$niveau.'" AND semestre="'.$semestre.'" AND code_rentree="'.$rentree.'"
                                        GROUP BY  evaluation, code_user
                                ) GROUP BY evaluation, code_user'));

            foreach($rek as $i => $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=> $rentree, 'semestre'=> $semestre, "reference"=> $matiere->reference, "id_auto"=> $d->id_auto,
                                'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> $niveau, 'specialite'=> $d->specialite, 'annee_univ'=> $annee_univ);

                if($d->evaluation != "SESSION 2")
                {
                    //enregistrement des données dans la table
                    $this->nano->saveData('delib_devoir', $datas);
                }
            }
        }
    }

}
