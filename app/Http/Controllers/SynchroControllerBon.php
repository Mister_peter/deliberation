<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nano;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SynchroController extends Controller
{
    protected $nano;

	public function __construct()
	{
        if(!Session::get('user_id')) return redirect('login')->send();

		$this->nano = new Nano;
	}


	public function index_l1()
    {
        $data["specialite"] = DB::table('parcours')->where('code_niveau', session('niveau'))->get();

        //Affichage la vue
        return view('synchro_l1', $data);
    }

    public function index_licence()
    {
        $data["etudiant"] = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        $data["devoir"] = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["examen"] = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["fusion"] = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        //Affichage la vue
        return view('synchro_l1', $data);
    }

    public function process1_licence()
    {
        $session_var = session('session_compo');
        $niveau_lien = str_replace(' ', '',  strtolower(session('niveau')));

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        $semestre = $d['semestre'];
                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);
                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }
        }
        else //POUR LA SESSION 2
        {
            $nb_etudiant=""; $nb_cours="";

            //ETUDIANT
            //on verifie s'il ya des etudiants de la rentrée, du niveau, du semestre dans la BD
            $check = DB::table('delib_etudiant')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                //recuperer tout le contenu du webservice
                $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($result, true);

                //boucle des données avec la clé (cours) du resultat_json
                if(!empty($json['etudiants']))
                {
                    foreach($json['etudiants'] as $d)
                    {
                        $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                        'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                        'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                        'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                        //enregistrement des données dans la table delib_etudiant
                        $this->nano->saveData('delib_etudiant', $datas);
                    }
                }
            }

            //COURS (ECUE)
            $check = DB::table('delib_matiere')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($result, true);

                //boucle des données avec la clé (cours) du resultat_json
                if(!empty($json['cours']))
                {
                    foreach($json['cours'] as $d)
                    {
                        $spec = explode("-", $d['specialite']);
                        $nb = count($spec);

                        for($i=0; $i<$nb; $i++)
                        {
                            $semestre = $d['semestre'];
                            if($semestre == session('semestre'))
                            {
                                $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                                'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                                //enregistrement des données dans la table matiere
                                $this->nano->saveData('delib_matiere', $datas);

                            }
                        }
                    }
                }
            }
        }

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>$nb_cours, "info"=> "Synchronisé le ".date('d-m-Y', strtotime($info->date_created)) ));
    }

    public function process2_licence()
    {
        $session_var = session('session_compo');
        $niveau_lien = str_replace(' ', '',  strtolower(session('niveau')));

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas2("delib_devoir_temp", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas2("delib_devoir", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);
                //var_dump($results); exit;
                $json   = json_decode($results, true);

                if(!empty($json['note']))
                {
                    //SI L'ECUE contient des notes
                    if($json['success'] == true)
                    {
                        foreach($json['note'] as $d)
                        {
                            $devoir = str_replace(' ', '', trim($d['num_devoir']));
                            $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                            $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                            'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_devoir_temp', $datas);
                        }
                    }
                }
            }
        }
        else //POUR LA SESSION 2
        {
            $nb_etudiant=""; $nb_cours="";

            //NOTES DE DEVOIR
            $check = DB::table('delib_devoir')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach ($sql as $value)
                {
                    $results = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);

                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                                $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                                'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                //enregistrement des données dans la table matiere
                                $this->nano->saveData('delib_devoir_temp', $datas);
                            }
                        }
                    }
                }
            }
        }

        //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
        //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
        $this->fill_miror();

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $note = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->get()->count();

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Synchronisé le ".date('d-m-Y', strtotime($info->date_created)) ));
    }

    //POUR LES PLATEFORMES D'EXAMEN
    public function process3_licence()
    {
        $session_var = session('session_compo');

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            // //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == $this->get_niveau(session('niveau')) && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == $this->get_niveau(session('niveau')) && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre')  && $value->session == $session_var)
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                             $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

        }
        else //POUR LA SESSION 2
        {
            //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }
        }

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $note = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->get()->count();

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Synchronisé le ".date('d-m-Y', strtotime($info->date_created)) ));
    }

    public function index_l2()
    {
        $data["specialite"] = DB::table('parcours')->where('code_niveau', session('niveau'))->get();

        //Affichage la vue
        return view('synchro_l2', $data);
    }

    public function index_l3()
    {
        $data["specialite"] = DB::table('parcours')->where('code_niveau', session('niveau'))->get();

        //Affichage la vue
        return view('synchro_l3', $data);
    }

    public function licence1_ok()
    {
        $session_var = session('session_compo');

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        $semestre = $d['semestre'];
                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);
                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }


            //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas2("delib_devoir_temp", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas2("delib_devoir", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);
                //var_dump($results); exit;
                $json   = json_decode($results, true);

                if(!empty($json['note']))
                {
                    //SI L'ECUE contient des notes
                    if($json['success'] == true)
                    {
                        foreach($json['note'] as $d)
                        {
                            $devoir = str_replace(' ', '', trim($d['num_devoir']));
                            $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                            $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                            'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_devoir_temp', $datas);
                        }
                    }
                }
            }

            //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
            //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
            $this->fill_miror();


            // //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre')  && $value->session == $session_var)
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                             $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //$this->write_ecue();

            //remplissage de la table delib_note
            $this->load_table_note();

        }
        else //POUR LA SESSION 2
        {
            $nb_etudiant=""; $nb_cours="";

            //ETUDIANT
            //on verifie s'il ya des etudiants de la rentrée, du niveau, du semestre dans la BD
            $check = DB::table('delib_etudiant')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                //recuperer tout le contenu du webservice
                $result = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($result, true);

                //boucle des données avec la clé (cours) du resultat_json
                if(!empty($json['etudiants']))
                {
                    foreach($json['etudiants'] as $d)
                    {
                        $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                        'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                        'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                        'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                        //enregistrement des données dans la table delib_etudiant
                        $this->nano->saveData('delib_etudiant', $datas);
                    }
                }
            }

            //COURS (ECUE)
            $check = DB::table('delib_matiere')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                $result = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($result, true);

                //boucle des données avec la clé (cours) du resultat_json
                if(!empty($json['cours']))
                {
                    foreach($json['cours'] as $d)
                    {
                        $spec = explode("-", $d['specialite']);
                        $nb = count($spec);

                        for($i=0; $i<$nb; $i++)
                        {
                            $semestre = $d['semestre'];
                            if($semestre == session('semestre'))
                            {
                                $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                                'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                                //enregistrement des données dans la table matiere
                                $this->nano->saveData('delib_matiere', $datas);

                            }
                        }
                    }
                }
            }

            //NOTES DE DEVOIR
            $check = DB::table('delib_devoir')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach ($sql as $value)
                {
                    $results = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);

                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                                $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                                'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                //enregistrement des données dans la table matiere
                                $this->nano->saveData('delib_devoir_temp', $datas);
                            }
                        }
                    }
                }

                //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
                //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
                $this->fill_miror();
            }


            //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //remplissage de la table delib_note
            $this->load_table_note2();
        }

        echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>$nb_cours, "nb_note"=>'', ));
    }

    //septembre 2020-2021
    public function licence1()
    {
        $session_var = session('session_compo');

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //on vide la table
            $this->nano->deleteDatas2("delib_devoir_temp", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas2("delib_devoir", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            /**************       ***************/

            // //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre')  && $value->session == $session_var)
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                             $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            $this->write_ecue();

            $this->fill_devoir();

            //remplissage de la table delib_note
            $this->load_table_note();

        }

        echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>'', "nb_note"=>'', ));
    }

    public function write_ecue()
    {
        $sql = DB::table('delib_compo')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))
                            ->groupBy('nom_matiere')->groupBy('specialite')->get();

        foreach($sql as $d)
        {
            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d->code_devoir, 'libelle'=>trim($d->nom_matiere), 'niveau'=> session('niveau'),
            'annee_univ'=>session('annee_univ'), 'specialite'=> $d->specialite, 'etat'=>true, 'semestre'=> $d->semestre);

            $this->nano->saveData('delib_matiere', $datas);
        }
    }

    public function licence11()
    {
        $session_var = $_POST['session']; $spec = $_POST['specialite'];

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre').'/'.$spec, false);
            $json   = json_decode($result, true);

            //on vide la table
            if(!empty($spec))
            {
                $this->nano->deleteDatas4("delib_etudiant", "niveau", "annee_univ", "code_rentree", "semestre", "specialite", "LICENCE 1", session('annee_univ'), session('rentree'), session('semestre'), $spec);
            }
            else
            {
                $this->nano->deleteDatas4("delib_etudiant", "niveau", "annee_univ", "code_rentree", "semestre", "LICENCE 1", session('annee_univ'), session('rentree'), session('semestre'));
            }

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>'LICENCE 1',
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=>session('rentree'),
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'nom'=>$d['nom'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }

            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas4("delib_matiere", "niveau", "annee_univ", "code_rentree", "semestre", "LICENCE 1", session('annee_univ'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        //$code_maquette = ($d['nom_cours'] != "") ? substr($d['niveau'], 0,-3) : "";
                        $matiere = explode(" - ", $d['nom_cours']);
                        $matiere = (count($matiere) ==1) ? $matiere[0] : $matiere[1];

                        // $matiere = explode(':', $matiere);
                        // $matiere = $matiere[0];

                        $semestre = explode(':', $matiere[2]);
                        $semestre = $semestre[0];

                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'code_maquette'=>'', 'categorie'=>$d['category'], 'libelle'=>trim($matiere), 'niveau'=>'LICENCE 1',
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=>$semestre);

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);
                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }


            //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas4("delib_devoir_temp", "niveau", "annee_univ", "code_rentree", "semestre", "LICENCE 1", session('annee_univ'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas4("delib_devoir", "niveau", "annee_univ", "code_rentree", "semestre", "LICENCE 1", session('annee_univ'), session('rentree'), session('semestre'));

            $sql = DB::table('delib_matiere')
                            ->where('niveau', "LICENCE 1")->where('code_rentree', session('rentree'))
                            ->where('etat', true)->where('semestre', session('semestre'))
                            ->where('annee_univ', session('annee_univ'))->groupBy('code_matiere')->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://licence1.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $devoir = str_replace(' ', '', trim($d['num_devoir']));
                        $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'), 'semestre'=>$value->semestre,
                                        'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>'LICENCE 1', 'annee_univ'=>session('annee_univ'));

                        //enregistrement des données dans la table matiere
                        $this->nano->saveData('delib_devoir_temp', $datas);

                    }
                }
            }

            //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
            //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
            $this->fill_miror();

            /******************************** PLATEFORME DE COMPO - PARTIEL *************************************/

            //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas6("delib_matiere_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $matiere = explode(" - ", $d['nom_cours']);
                $sem = explode("EMESTRE ", $matiere[1]);
                $semestre = $sem[0].$sem[1];

                //obtention de la session d'examen
                $session = (strpos($matiere[2] , "NORMALE") == true) ? "SESSION 1":"SESSION 2";
                $niveau = explode(":", $d['niveau']);

                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=>trim($semestre), 'code_rentree'=>session('rentree'),
                                'niveau'=>$d['niveau'], 'annee_univ'=>session('annee_univ'), 'session'=>trim($session), 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($niveau[0] == "LICENCE 1" && $semestre == session('semestre') && $session == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas6("delib_matiere_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $matiere = explode(" - ", $d['nom_cours']);
                $sem = explode("EMESTRE ", $matiere[1]);
                $semestre = $sem[0].$sem[1];

                //obtention de la session d'examen
                $session = (strpos($matiere[2] , "NORMALE") == true) ? "SESSION 1":"SESSION 2";
                $niveau = explode(":", $d['niveau']);

                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=>trim($semestre), 'code_rentree'=>session('rentree'),
                                'niveau'=>$d['niveau'], 'annee_univ'=>session('annee_univ'), 'session'=>trim($session), 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($niveau[0] == "LICENCE 1" && $semestre == session('semestre') && $session == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas6("delib_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas5("delib_note", "niveau", "annee_univ", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas5("delib_note_finale", "niveau", "annee_univ", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', "LICENCE 1")->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)
                            ->where('annee_univ', session('annee_univ'))->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/L1', false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['num_devoir'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>'LICENCE 1', 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=>$value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        $this->nano->saveData('delib_compo', $datas);

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas6("delib_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', "LICENCE 1")->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)
                            ->where('annee_univ', session('annee_univ'))->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/L1', false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['num_devoir'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>'LICENCE 1', 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=>$value->session, 'semestre'=>$value->semestre,);

                        //enregistrement des données dans la table matiere
                        $this->nano->saveData('delib_compo', $datas);

                    }
                }
            }

            //remplissage de la table delib_note
            $this->load_table_note();

        }
        else //si la session est: rattrapage, on recupere les notes d'examen seulement. Puis on recupere la meilleure note d'examen entre les deux sessions
        {
            /******************************** PLATEFORME DE COMPO - PARTIEL *************************************/

            //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas6("delib_matiere_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $matiere = explode(" - ", $d['nom_cours']);
                $sem = explode("EMESTRE ", $matiere[1]);
                $semestre = $sem[0].$sem[1];

                //obtention de la session d'examen
                $session = (strpos($matiere[2] , "NORMALE") == true) ? "SESSION 1":"SESSION 2";
                $niveau = explode(":", $d['niveau']);

                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=>trim($semestre), 'code_rentree'=>session('rentree'),
                                'niveau'=>$d['niveau'], 'annee_univ'=>session('annee_univ'), 'session'=>trim($session), 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($niveau[0] == "LICENCE 1" && $semestre == session('semestre') && $session == $session_var)
                {
                    //on veifie si la matiere existe deja
                    // $check = DB::table('delib_matiere_compo')
                    //             ->where('niveau', "LICENCE 1")->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                    //             ->where('semestre', session('semestre'))->where('session', $session_var)->where('annee_univ', session('annee_univ'))->first();

                    // if($check->isEmpty())
                    $this->nano->saveData('delib_matiere_compo', $datas);

                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas6("delib_matiere_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $matiere = explode(" - ", $d['nom_cours']);
                $sem = explode("EMESTRE ", $matiere[1]);
                $semestre = $sem[0].$sem[1];

                //obtention de la session d'examen
                $session = (strpos($matiere[2] , "NORMALE") == true) ? "SESSION 1":"SESSION 2";
                $niveau = explode(":", $d['niveau']);

                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=>trim($semestre), 'code_rentree'=>session('rentree'),
                                'niveau'=>$d['niveau'], 'annee_univ'=>session('annee_univ'), 'session'=>trim($session), 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($niveau[0] == "LICENCE 1" && $semestre == session('semestre') && $session == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas6("delib_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', "LICENCE 1")->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)
                            ->where('annee_univ', session('annee_univ'))->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/L1', false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['num_devoir'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>'LICENCE 1', 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=>$value->session, 'semestre'=>$value->semestre, );

                        //on verifie si la note de la session 1, existe;
                        $check = DB::table('delib_compo')->where('niveau', "LICENCE 1")->where('plateforme', "PARTIEL1")->where('code_rentree', "SESSION 1")
                                ->where('nom_matiere', $d['num_devoir'])->where('semestre', session('semestre'))->where('email_user', $d['user_email'])
                                ->where('session', $session_var)->where('annee_univ', session('annee_univ'))->first();

                        if($check->isEmpty())
                        {
                            //enregistrement des données dans la table note examen
                            $this->nano->saveData('delib_compo', $datas);
                        }
                        else
                        {
                            //si le user a composé en session 1; et qu'il a une note <10, on fait le update
                            if($check->note < 10)
                            {
                                DB::table("delib_compo")->where('id_note', $check->id_note)->update($datas);
                            }
                        }
                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas6("delib_compo", "niveau", "annee_univ", "plateforme", "code_rentree", "semestre", "session", "LICENCE 1", session('annee_univ'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', "LICENCE 1")->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)
                            ->where('annee_univ', session('annee_univ'))->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/L1', false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['num_devoir'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>'LICENCE 1', 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=>$value->session, 'semestre'=>$value->semestre,);

                        //on verifie si la note de la session 1, existe;
                        $check = DB::table('delib_compo')->where('niveau', "LICENCE 1")->where('plateforme', "PARTIEL2")->where('code_rentree', "SESSION 1")
                                    ->where('nom_matiere', $d['num_devoir'])->where('semestre', session('semestre'))->where('email_user', $d['user_email'])
                                    ->where('session', $session_var)->where('annee_univ', session('annee_univ'))->first();

                        if($check->isEmpty())
                        {
                            //enregistrement des données dans la table des examens
                            $this->nano->saveData('delib_compo', $datas);
                        }
                        else
                        {
                            //si le user a composé en session 1; et qu'il a une note <10, on fait le update
                            if($check->note < 10)
                            {
                                DB::table("delib_compo")->where('id_note', $check->id_note)->update($datas);
                            }
                        }

                    }
                }
            }
        }

        echo json_encode(array("status" => TRUE, "nb_etudiant"=>$nb_etudiant));
    }

    public function licence2()
    {
        $session_var = session('session_compo');

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence2.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence2.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        $semestre = $d['semestre'];
                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);

                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }


            //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas2("delib_devoir_temp", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas2("delib_devoir", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://licence2.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);
                //var_dump($results); exit;
                $json   = json_decode($results, true);

                if(!empty($json['note']))
                {
                    //SI L'ECUE contient des notes
                    if($json['success'] == true)
                    {
                        foreach($json['note'] as $d)
                        {
                            $devoir = str_replace(' ', '', trim($d['num_devoir']));
                            $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                            $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                            'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_devoir_temp', $datas);

                        }
                    }
                }
            }

            //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
            //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
            $this->fill_miror();


            // //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L2" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L2" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                    ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre')  && $value->session == $session_var)
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                    ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                             $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //$this->write_ecue();

            //remplissage de la table delib_note
            $this->load_table_note();

        }
        else //POUR LA SESSION 2
        {
            $nb_etudiant=""; $nb_cours="";

            //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L2" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L2" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //remplissage de la table delib_note
            $this->load_table_note2();
        }

        echo json_encode(array("status" => TRUE, "nb_etudiant"=>$nb_etudiant, "nb_cours"=>$nb_cours, "nb_note"=>'', ));
    }

    public function licence3()
    {
        $session_var = session('session_compo');

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence3.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence3.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        $semestre = $d['semestre'];
                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);

                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }


            //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas2("delib_devoir_temp", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas2("delib_devoir", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://licence3.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);
                //var_dump($results); exit;
                $json   = json_decode($results, true);

                if(!empty($json['note']))
                {
                    //SI L'ECUE contient des notes
                    if($json['success'] == true)
                    {
                        foreach($json['note'] as $d)
                        {
                            $devoir = str_replace(' ', '', trim($d['num_devoir']));
                            $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                            $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                            'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_devoir_temp', $datas);

                        }
                    }
                }
            }

            //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
            //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
            $this->fill_miror();


            // //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L3" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L3" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre')  && $value->session == $session_var)
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                             $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //$this->write_ecue();

            //remplissage de la table delib_note
            $this->load_table_note();

        }
        else //POUR LA SESSION 2
        {
            $nb_etudiant=""; $nb_cours="";

            //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L3" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L3" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //remplissage de la table delib_note
            $this->load_table_note2();
        }

        echo json_encode(array("status" => TRUE, "nb_etudiant"=>$nb_etudiant, "nb_cours"=>$nb_cours, "nb_note"=>'', ));
    }

    public function update_coef()
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))->get();
        foreach($sql as $matiere)
        {
            $nbr = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM v_note_devoir WHERE code_rentree="'.session('rentree').'" AND niveau="'.session('niveau').'" AND annee_univ="'.session('annee_univ').'" AND code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$matiere->specialite.'" AND etat=0 GROUP BY code_user) AS nb');

            $data = array("coef" => $nbr->nombre);
            //var_dump($data); exit;

            DB::table("delib_devoir")
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))
                        ->where('code_matiere', $matiere->code_matiere)
                        ->update($data);
        }
    }

    public function update_coefs($matiere)
    {
        $nbr = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE code_rentree="'.session('rentree').'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND annee_univ="'.session('annee_univ').'" AND code_matiere="'.$matiere.'" AND etat=0 GROUP BY code_user) AS nb');

        $data = array("coef" => $nbr->nombre);

        DB::table("delib_devoir")
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('annee_univ', session('annee_univ'))
                    ->where('semestre', session('semestre'))
                    ->where('code_matiere', $matiere)
                    ->update($data);
    }

    //supprimer les notes de devoirs les plus faibles par matieres et par devoir
    public function delete_min_note()
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))->groupBy('code_matiere')->get();
        foreach($sql as $matiere)
        {
            $rek = DB::table('delib_devoir_temp')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))
                        ->where('code_matiere', $matiere->code_matiere)->groupBy('evaluation')->get();

            foreach($rek as $d)
            {
                DB::delete("DELETE t1 FROM delib_devoir_temp t1 INNER JOIN delib_devoir_temp t2
                                WHERE t1.note < t2.note AND
                                t1.code_user = t2.code_user AND t1.code_matiere='".$matiere->code_matiere."' AND t1.evaluation='".$d->evaluation."'
                                AND t1.niveau='".session('niveau')."' AND t1.annee_univ='".session('annee_univ')."' AND t1.code_rentree='".session('rentree')."' ");
            }

            /*DB::delete("DELETE FROM delib_devoir_temp WHERE code_matiere='".$matiere->code_matiere."'
                                AND note NOT IN(SELECT note from (SELECT MIN(note) as note from delib_devoir_temp GROUP BY evaluation, code_user) AS t) ");*/

        }
    }

    public function fill_miror($specialite = null)
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

            foreach($sql as $matiere)
            {
                //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
                $rek = DB::select(DB::raw('SELECT id_note, annee_univ, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation
                                    FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" AND evaluation in (
                                            SELECT evaluation
                                                FROM delib_devoir_temp
                                                WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                            GROUP BY  evaluation, code_user
                                    ) GROUP BY evaluation, code_user'));

                foreach($rek as $i => $d)
                {
                    $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=>session('semestre'),
                                    'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> session('niveau'), 'specialite'=> $matiere->specialite, 'annee_univ'=>session('annee_univ'));

                    //enregistrement des données dans la table
                    $this->nano->saveData('delib_devoir', $datas);
                }
            }
    }

    //pour rentree speciale de septembre 2020-2021
    public function fill_devoir()
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            $rek = DB::select('SELECT * FROM delib_compo WHERE code_devoir="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" ');

            foreach($rek as $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=> $d->semestre,
                                'code_user'=>$d->code_user, 'evaluation'=>"Devoir 01", 'note'=> 0, 'niveau'=> session('niveau'), 'specialite'=>$d->specialite, 'annee_univ'=>session('annee_univ'));

                //enregistrement des données dans la table
                $this->nano->saveData('delib_devoir_temp', $datas);
                $this->nano->saveData('delib_devoir', $datas);
            }
        }
    }

    //our la session 1
    public function load_table_note()
    {
        $session = session('session_compo');

        //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
        $etudiant = DB::table('v_etudiant_matiere')
                            ->where('niveau', session('niveau'))
                            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                            ->where('etat', true)->get();

        foreach($etudiant as $d)
        {
            $specialite = $d->specialite; $matiere = $d->libelle;

            //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
            $nb_devoir = DB::table('v_note_devoir')
                                ->where('niveau', session('niveau'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

            $nb_examen = DB::table('v_note_examen')
                                ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('specialite', $specialite)->where('email', $d->email )->count();

            if($nb_devoir >= 1 || $nb_examen>0)
            {
                //on va enregistrer la ligne de l'étudiant
                $examen = DB::table('v_note_examen')
                                ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('nom_matiere', 'LIKE', "%".$d->libelle."%")
                                ->where('specialite', $specialite)->where('email', $d->email)->first();

                //coefficient (nbre de note)
                $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE  delib_etudiant.code_rentree="'.session('rentree').'" AND delib_etudiant.niveau="'.session('niveau').'" AND delib_devoir.semestre="'.session('semestre').'" AND delib_devoir.code_matiere="'.$d->code_matiere.'" AND delib_etudiant.specialite="'.$specialite.'" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');

                //var_dump($coef); exit;

                $note_examen = (!empty($examen)) ? $examen->note : "";
                $etat_examen = (!empty($examen)) ? false : true;
                $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                $session_examen = (!empty($examen)) ? $examen->session : $session;


                $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff);
                //var_dump($datas);

                $assoc = DB::table('delib_note')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                            ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                            ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                if(empty($assoc))
                {
                    //on enregistre la ligne
                    $this->nano->saveData('delib_note', $datas);
                }
                else
                {
                    //on fait un update
                    DB::table("delib_note")
                        ->where('niveau', session('niveau'))
                        ->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('session', session('session_compo'))
                        ->where('code_matiere', $d->code_matiere)
                        ->where('specialite', $specialite)
                        ->where('id_auto', $d->id_auto)
                        ->update($datas);
                }

                $note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note) AS note, GROUP_CONCAT(t1.evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t2.annee_univ="'.session('annee_univ').'" GROUP BY t1.code_matiere, t1.code_user');
                $liste_devoir = DB::selectOne('SELECT GROUP_CONCAT(t1.evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t1.code_user="'.$d->code_etudiant.'" AND t2.annee_univ="'.session('annee_univ').'" GROUP BY t1.code_matiere, t1.code_user');
                //var_dump($note);

                if(!empty($note))
                {
                    $notes = explode(",", $note->note);
                    $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                    $merge= array(array_combine($dev, $notes));
                    ksort($merge);

                    //var_dump($liste_devoir->evaluation);

                    $devoirs = explode(",",  str_replace(' ', '',  $liste_devoir->evaluation));
                    ksort($devoirs);

                    foreach($devoirs as $vaa)
                    {
                        $note_devoir = (array_key_exists($vaa, $merge[0])) ? (floor(($merge[0][$vaa]*100))/100) : "";
                        //var_dump($note_devoir);

                        $titre = explode('0', $vaa);
                        $titre = (count($titre) ==1) ? $titre[0] : $titre[0].$titre[1];
                        //var_dump($titre);

                        $taille = strlen($titre);
                        if($taille >7)
                        {
                            $titre = substr($titre, 0, 7);
                        }

                        $datax = array(strtolower($titre) =>$note_devoir);
                        //var_dump($datax);

                        //on fait un update
                        DB::table("delib_note")
                                ->where('niveau', session('niveau'))
                                ->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->where('session', session('session_compo'))
                                ->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)
                                ->where('id_auto', $d->id_auto)
                                ->update($datax);
                    }
                }
            }

        }
    }

    //our la session 2
    public function load_table_note2()
    {
        $session = session('session_compo');

        //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
        $etudiant = DB::table('v_etudiant_matiere')
                            ->where('niveau', session('niveau'))
                            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                            ->where('etat', true)->get();

        foreach($etudiant as $d)
        {
            $specialite = $d->specialite; $matiere = $d->libelle;

            //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
            $nb_devoir = DB::table('v_note_devoir')
                                ->where('niveau', session('niveau'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

            $nb_examen = DB::table('v_note_examen')
                                ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('specialite', $specialite)->where('email', $d->email )->count();

            if($nb_devoir >= 1 || $nb_examen>0)
            {
                //on verifie si l'etudiant a composé en session 2
                $examen = DB::table('v_note_examen')
                                ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('nom_matiere', 'LIKE', "%".$d->libelle."%")
                                ->where('specialite', $specialite)->where('email', $d->email)->first();

                if(!empty($examen))
                {
                    //coefficient (nbre de note)
                    $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE  delib_etudiant.code_rentree="'.session('rentree').'" AND delib_etudiant.niveau="'.session('niveau').'" AND delib_devoir.semestre="'.session('semestre').'" AND delib_devoir.code_matiere="'.$d->code_matiere.'" AND delib_etudiant.specialite="'.$specialite.'" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');

                    //var_dump($coef); exit;

                    $note_examen = (!empty($examen)) ? $examen->note : "";
                    $etat_examen = (!empty($examen)) ? false : true;
                    $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                    $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                    $session_examen = (!empty($examen)) ? $examen->session : $session;


                    $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                    "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                    "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff);
                    //var_dump($datas);

                    $assoc = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                    if(empty($assoc))
                    {
                        //on enregistre la ligne
                        $this->nano->saveData('delib_note', $datas);
                    }
                    else
                    {
                        //on fait un update
                        DB::table("delib_note")
                            ->where('niveau', session('niveau'))
                            ->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))
                            ->where('session', session('session_compo'))
                            ->where('code_matiere', $d->code_matiere)
                            ->where('specialite', $specialite)
                            ->where('id_auto', $d->id_auto)
                            ->update($datas);
                    }

                    $note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note) AS note, GROUP_CONCAT(t1.evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t2.annee_univ="'.session('annee_univ').'" GROUP BY t1.code_matiere, t1.code_user');
                    $liste_devoir = DB::selectOne('SELECT GROUP_CONCAT(t1.evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t1.code_user="'.$d->code_etudiant.'" AND t2.annee_univ="'.session('annee_univ').'" GROUP BY t1.code_matiere, t1.code_user');
                    //var_dump($note);

                    if(!empty($note))
                    {
                        $notes = explode(",", $note->note);
                        $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                        $merge= array(array_combine($dev, $notes));
                        ksort($merge);

                        //var_dump($liste_devoir->evaluation);

                        $devoirs = explode(",",  str_replace(' ', '',  $liste_devoir->evaluation));
                        ksort($devoirs);

                        foreach($devoirs as $vaa)
                        {
                            $note_devoir = (array_key_exists($vaa, $merge[0])) ? (floor(($merge[0][$vaa]*100))/100) : "";
                            //var_dump($note_devoir);

                            $titre = explode('0', $vaa);
                            $titre = (count($titre) ==1) ? $titre[0] : $titre[0].$titre[1];
                            //var_dump($titre);

                            $taille = strlen($titre);
                            if($taille >7)
                            {
                                $titre = substr($titre, 0, 7);
                            }

                            $datax = array(strtolower($titre) =>$note_devoir);
                            //var_dump($datax);

                            //on fait un update
                            DB::table("delib_note")
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))
                                    ->where('semestre', session('semestre'))
                                    ->where('session', session('session_compo'))
                                    ->where('code_matiere', $d->code_matiere)
                                    ->where('specialite', $specialite)
                                    ->where('id_auto', $d->id_auto)
                                    ->update($datax);
                        }
                    }

                }

            }

        }
    }

    public function move_note()
    {
        $matiere = DB::table('delib_matiere')->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))->groupBy('code_matiere')->get();

        foreach($matiere as $mat)
        {
            $notes = DB::table('delib_devoir_temp')
                                ->where('niveau', session('niveau'))
                                ->where('annee_univ', session('annee_univ'))
                                ->where('code_matiere', $mat->code_matiere)
                                ->where('code_user', $d->code_etudiant)->get();
        }

        $notes = DB::table('delib_devoir_temp')
                                ->where('niveau', session('niveau'))
                                ->where('annee_univ', session('annee_univ'))
                                ->where('code_matiere', $matiere)
                                ->where('code_user', $d->code_etudiant)->get();

        $not = array();
        foreach($data["note"] as $d)
        {


            $nb = count($notes);
            $row = $data['nbr']->nombre - $nb;

            foreach($notes as $d)
            {
                $not[] = number_format((float) round($d->note, 2), 2, '.', '');
            }
        }
    }

}
