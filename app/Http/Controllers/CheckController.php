<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nano;
use Illuminate\Support\Facades\DB;

class CheckController extends Controller
{
    protected $nano;

	public function __construct()
	{
		$this->nano = new Nano;
	}

	public function getNiveau()
    {
        $specialite = $_POST['loadId'];

		$result = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))
                            ->where('etat', true)
                            ->where('specialite', $specialite)
                            ->get();

		$HTML="";

		if(!empty($result)){
			foreach($result as $list){
				$HTML.="<option value='".$list->code_matiere."'>".$list->libelle."</option>";
			}
		}
		echo $HTML;
    }

    public function getMatiere()
    {
        $specialite = $_POST['loadId'];

		$result = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))
                            ->where('etat', true)
                            ->where('specialite', $specialite)
                            ->get();

		$HTML="";

		if(!empty($result)){
			foreach($result as $list){
				$HTML.="<option value='".$list->libelle."'>".$list->libelle."</option>";
			}
		}
		echo $HTML;
    }

    public function getMatieres()
    {
        $specialite = $_POST['loadId'];

		$result = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('etat', true)
                            ->where('specialite', $specialite)
                            ->get();

		$HTML="";

		if(!empty($result)){
			foreach($result as $list){
				$HTML.="<option value='".$list->code_matiere."'>".$list->libelle."</option>";
			}
		}
		echo $HTML;
    }

    public function getSemestre()
    {
        $niveau = $_POST['loadId'];

		$result = DB::table('semestre_niveau')
                            ->where('niveau', $niveau)
                            ->get();

		$HTML="";

		if(!empty($result)){
			foreach($result as $list){
				$HTML.="<option value='".$list->semestre."'>".$list->semestre."</option>";
			}
		}
		echo $HTML;
    }

    // Affichage des infos du taux d'admis lors du repechage
    public function getTableau()
    {
        $note = $_POST['note']; $moy1 = $_POST['moy1']; $moy2 = $_POST['moy2']; $matiere = $_POST['matiere']; $specialite = $_POST['specialite'];

        $admis = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
                                    ->where('code_rentree', session('rentree'))
                                    ->where('semestre', session('semestre'))
                                    ->where('niveau', session('niveau'))->where('decision', "ADMIS")
                                    ->where('specialite', $specialite)->where('code_matiere', $matiere)
                                    ->count();

        $echec = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
                                    ->where('code_rentree', session('rentree'))
                                    ->where('semestre', session('semestre'))
                                    ->where('niveau', session('niveau'))->where('decision', "REFUSE")
                                    ->where('specialite', $specialite)->where('code_matiere', $matiere)
                                    ->count();

        $total = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
                                    ->where('code_rentree', session('rentree'))->where('niveau', session('niveau'))
                                    ->where('specialite', $specialite)->where('code_matiere', $matiere)
                                    ->where('semestre', session('semestre'))
                                    ->count();

        $repechage = DB::table('delib_note_finale')->where('annee_univ', session('annee_univ'))
                                    ->where('semestre', session('semestre'))
                                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                    ->whereBetween('mga2', array($moy1, $moy2))
                                    ->where('note_examen', '>=', $note)
                                    ->where('specialite', $specialite)->where('code_matiere', $matiere)
                                    ->count();

        $t_admis = ($admis*100)/$total; $t_admis = round($t_admis, 2);
        $t_rep = ($repechage*100)/$total; $t_rep = round($t_rep, 2);
        $total_rep = ($admis+$repechage);
        $t_total = ($total_rep*100)/$total; $t_total = round($t_total, 2);
        $t_echec = (($total-$total_rep)*100)/$total; $t_echec = round($t_echec, 2);

        $HTML="";

        if($moy1>=0 && $moy2>=0)
        {
            $HTML.='<p style="color: rgb(2, 105, 66)">ADMIS<br>'.$admis.' | '.$t_admis.'%</p>';
            $HTML.='<p style="color: red">REFUSE<br>'.($echec - $repechage).' | '.$t_echec.'%</p>';
            $HTML.='<p style="color: green">REP&Ecirc;CHAGE <br>'.$repechage.' | '.$t_rep.'%</p>';
            $HTML.='<p>TOTAL ADMIS<br>'.$total_rep.' | '.$t_total.'% </p>';
        }
        else
        {
            $HTML="";
        }

		echo $HTML;
    }

    public function getNote()
    {
        $matiere= $_POST['matiere']; $user= $_POST['user'];

        $notes = DB::table('delib_note_tempon')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))
                            ->where('code_matiere', $matiere)
                            ->where('code_user', $user)->get();
        $note = array();
        foreach($notes as $d)
        {
            $note[] = $d->note;
        }
        echo json_encode($note);
    }
}
