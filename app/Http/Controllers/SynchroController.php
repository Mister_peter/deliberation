<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nano;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SynchroController extends Controller
{
    protected $nano;

	public function __construct()
	{
        if(!Session::get('user_id')) return redirect('login')->send();

		$this->nano = new Nano;
	}

	public function index_licence()
    {
        $data["etudiant"] = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        $data["devoir"] = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["examen"] = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["fusion"] = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        //Affichage la vue
        return view('synchro_l1', $data);
    }

    public function septembre()
    {
        $data["etudiant"] = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        //Affichage la vue
        return view('synchro_septembre');
    }

    public function index_licences()
    {
        $data["specialite"] = DB::table('parcours')->where('code_niveau', session('niveau'))->get();

        $data["etudiant"] = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        $data["devoir"] = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["examen"] = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $data["fusion"] = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        //Affichage la vue
        return view('synchro_semestre', $data);
    }

    public function process1_licence()
    {
        $session_var = session('session_compo');
        $niveau_lien = str_replace(' ', '',  strtolower(session('niveau')));
        $sem_lien = strtolower(session('semestre'));

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            //$result = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        $semestre = $d['semestre'];
                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);
                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }
        }
        else //POUR LA SESSION 2
        {
            $nb_etudiant=""; $nb_cours="";

            //ETUDIANT
            //on verifie s'il ya des etudiants de la rentrée, du niveau, du semestre dans la BD
            $check = DB::table('delib_etudiant')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                //recuperer tout le contenu du webservice
                $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($result, true);

                //boucle des données avec la clé (cours) du resultat_json
                if(!empty($json['etudiants']))
                {
                    foreach($json['etudiants'] as $d)
                    {
                        $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                        'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                        'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                        'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                        //enregistrement des données dans la table delib_etudiant
                        $this->nano->saveData('delib_etudiant', $datas);
                    }
                }
            }

            //COURS (ECUE)
            $check = DB::table('delib_matiere')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                $result = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($result, true);

                //boucle des données avec la clé (cours) du resultat_json
                if(!empty($json['cours']))
                {
                    foreach($json['cours'] as $d)
                    {
                        $spec = explode("-", $d['specialite']);
                        $nb = count($spec);

                        for($i=0; $i<$nb; $i++)
                        {
                            $semestre = $d['semestre'];
                            if($semestre == session('semestre'))
                            {
                                $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                                'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                                //enregistrement des données dans la table matiere
                                $this->nano->saveData('delib_matiere', $datas);

                            }
                        }
                    }
                }
            }
        }

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>$nb_cours, "info"=> "Synchronisé le ".date('d-m-Y', strtotime($info->date_created)) ));
    }

    public function process2_licence()
    {
        $session_var = session('session_compo');
        $niveau_lien = str_replace(' ', '',  strtolower(session('niveau')));

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas2("delib_devoir_temp", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas2("delib_devoir", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);
                //var_dump($results); exit;
                $json   = json_decode($results, true);

                if(!empty($json['note']))
                {
                    //SI L'ECUE contient des notes
                    if($json['success'] == true)
                    {
                        foreach($json['note'] as $d)
                        {
                            $devoir = str_replace(' ', '', trim($d['num_devoir']));
                            $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                            $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                            'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_devoir_temp', $datas);
                        }
                    }
                }
            }
        }
        else //POUR LA SESSION 2
        {
            $nb_etudiant=""; $nb_cours="";

            //NOTES DE DEVOIR
            $check = DB::table('delib_devoir')->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))
                        ->where('niveau', session('niveau'))->get();

            if($check->count() == 0)
            {
                $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach ($sql as $value)
                {
                    $results = file_get_contents('http://'.$niveau_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre'), false);

                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère

                                $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                                'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                //enregistrement des données dans la table matiere
                                $this->nano->saveData('delib_devoir_temp', $datas);
                            }
                        }
                    }
                }
            }
        }

        //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
        //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
        $this->fill_miror();

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $note = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->get()->count();

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Synchronisé le ".date('d-m-Y', strtotime($info->date_created)) ));
    }

    //POUR LES PLATEFORMES D'EXAMEN PARTIEL
    public function process3_licence()
    {
        $session_var = session('session_compo');

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            // //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == $this->get_niveau(session('niveau')) && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == $this->get_niveau(session('niveau')) && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre')  && $value->session == $session_var)
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                             $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

        }
        else //POUR LA SESSION 2
        {
            //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }

            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == "L1" && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }
        }

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $note = DB::table('delib_compo')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->get()->count();

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Synchronisé le ".date('d-m-Y', strtotime($info->date_created)) ));
    }


    /*********************** PLATEFORME SEMESTRIELLE ********************* */
    //recuperation des etudiants et cours
    public function process1_licences()
    {
        set_time_limit(0);

        $session_var = session('session_compo');
        $niveau_lien = str_replace(' ', '',  strtolower(session('niveau')));
        $sem_lien = strtolower(session('semestre'));

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //Recuperer tout le contenu du webservice
            $result = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/get_course/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            $nb_cours = 0;
            if(!empty($json['cours']))
            {
                foreach($json['cours'] as $d)
                {
                    $spec = explode("-", $d['specialite']);
                    $nb = count($spec);

                    for($i=0; $i<$nb; $i++)
                    {
                        $semestre = $d['semestre'];
                        if($semestre == session('semestre'))
                        {
                            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d['id_cours'], 'reference'=> $d['reference'], 'categorie'=>$d['category'], 'libelle'=>trim($d['nom_cours']), 'niveau'=>session('niveau'),
                                            'annee_univ'=>session('annee_univ'), 'specialite_groupe'=> $d['specialite'], 'specialite'=> "ISN-".$spec[$i], 'etat'=>true, 'semestre'=> session('semestre'));

                            //enregistrement des données dans la table matiere
                            $this->nano->saveData('delib_matiere', $datas);
                        }
                    }

                    $nb_cours++;
                }
            }
            else
            {
                $nb_cours = 0;
            }
        }
        else
        {
            $nb_etudiant=0; $nb_cours=0;
        }

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_etudiant')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_etudiant', 'DESC')->skip(0)->take(1)->first();

        echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>$nb_cours, "info"=> "Synchronisé le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
    }

    //recuperation des notes de devoirs et compo
    public function process2_licences()
    {
        set_time_limit(0);

        $session_var = session('session_compo');
        $sem_lien    = strtolower(session('semestre'));
        $specialite = $_POST['specialite'];

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            if($specialite == "TOUS")
            {
                //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE
                //on vide la table
                $this->nano->deleteDatas4_("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");
                $this->nano->deleteDatas4_("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");

                $sql = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach ($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var, false);
                    //var_dump($results); exit;
                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                // on check pour voir si le libelle commence par D ou S
                                if(substr(trim($d['num_devoir']),0,1) == "D")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                }
                                elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                }

                                if($devoir !=  "SESSION 2")
                                {
                                    $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                                    'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                    //enregistrement des données dans la table note
                                    $save = $this->nano->saveData('delib_devoir_temp', $datas);

                                    if($save == 1)
                                    {
                                        $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/update_etat_note/'.$d['id_note'].'/'.session('rentree'), false);
                                    }
                                }
                            }
                        }
                    }
                }

                //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
                //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
                $this->fill_miror_new();
            }
            else
            {
                //RECUPERATION DES NOTES DE DEVOIR PAR ECUE AVEC LE PARAMETRE RENTREE et PAR SPECIALITE
                //on vide la table selon la specialite
                $this->nano->deleteDatas5_("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);
                $this->nano->deleteDatas5_("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);

                $sql = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('specialite', $specialite)
                            ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach ($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var.'/'.$specialite, false);
                    //var_dump($results); exit;
                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                // on check pour voir si le libelle commence par D ou S
                                if(substr(trim($d['num_devoir']),0,1) == "D")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                }
                                elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                }

                                if($devoir !=  "SESSION 2" && $specialite == $d['specialite'])
                                {
                                    $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                                    'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                    //enregistrement des données dans la table matiere
                                    $save = $this->nano->saveData('delib_devoir_temp', $datas);

                                    if($save == 1)
                                    {
                                        $send = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/update_etat_note/'.$d['id_note'].'/'.session('rentree'), false);
                                    }
                                }
                            }
                        }
                    }
                }

                //REMPLISSAGE DE LA TABLE IMAGE DES NOTES DE DEVOIR EN PRENANT EN COMPTE
                //LES NOTES SUPERIEURES ENTRE LES MEMES DEVOIR POUR UN ETUDIANT
                $this->fill_miror_new2($specialite);
            }
        }
        else //POUR LA SESSION 2
        {
            if($specialite == "TOUS")
            {
                //on vide les tables
                $this->nano->deleteDatas4("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");
                $this->nano->deleteDatas4("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", session('niveau'), session('rentree'), session('semestre'), "SESSION 2");

                //NOTES DE RATTRAPAGE
                $sql = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach ($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var, false);

                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                // on check pour voir si le libelle commence par D ou S
                                if(substr(trim($d['num_devoir']),0,1) == "D")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                }
                                elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                }

                                if($devoir ==  "SESSION 2")
                                {
                                    $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                                'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                    //enregistrement des données dans la table matiere
                                    $this->nano->saveData('delib_devoir_temp', $datas);
                                    $this->nano->saveData('delib_devoir', $datas);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //on vide les tables
                $this->nano->deleteDatas5("delib_devoir_temp", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);
                $this->nano->deleteDatas5("delib_devoir", "niveau", "code_rentree", "semestre", "evaluation", "specialite", session('niveau'), session('rentree'), session('semestre'), "SESSION 2", $specialite);

                //NOTES DE RATTRAPAGE
                $sql = DB::table('delib_matiere')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('specialite', $specialite)
                            ->where('etat', true)->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

                foreach ($sql as $value)
                {
                    $results = file_get_contents('http://licence'.$sem_lien.'.uvci.edu.ci/api_deliberation/api/get_note/'.$value->code_matiere.'/'.session('rentree').'/'.session('semestre').'/'.$session_var.'/'.$specialite, false);

                    $json   = json_decode($results, true);

                    if(!empty($json['note']))
                    {
                        //SI L'ECUE contient des notes
                        if($json['success'] == true)
                        {
                            foreach($json['note'] as $d)
                            {
                                // on check pour voir si le libelle commence par D ou S
                                if(substr(trim($d['num_devoir']),0,1) == "D")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '6' , ' ' , true );  //on ajoute un espace apres le 6e caractère
                                }
                                elseif(substr(trim($d['num_devoir']),0,1) == "S")
                                {
                                    $devoir = str_replace(' ', '', trim($d['num_devoir']));
                                    $devoir = wordwrap($devoir , '7' , ' ' , true );  //on ajoute un espace apres le 7e caractère
                                }

                                if($devoir ==  "SESSION 2" && $specialite == $d['specialite'] )
                                {
                                    $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=> $d['code_rentree'], 'semestre'=> session('semestre'),
                                                'code_user'=>$d['id_user'], 'evaluation'=>trim($devoir), 'note'=>floor(($d['note']*100))/100, 'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'));

                                    //enregistrement des données dans la table matiere
                                    $this->nano->saveData('delib_devoir_temp', $datas);
                                    $this->nano->saveData('delib_devoir', $datas);
                                }
                            }
                        }
                    }
                }
            }
        }

        //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
        $info = DB::table('delib_devoir')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('evaluation', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        if($specialite == "TOUS")
        {
            $note = DB::table('delib_devoir')
                        ->where(function($query) {
                                            $query->where('evaluation', session('session_compo'))
                                                ->orWhere('evaluation', 'LIKE', '%Devoir%');
                                })
                        ->where('code_rentree', session('rentree'))
                        ->where('niveau', session('niveau'))
                        ->where('semestre', session('semestre'))
                        ->get()->count();
        }
        else
        {
            $note = DB::table('delib_devoir')
                        ->where(function($query) {
                                            $query->where('evaluation', session('session_compo'))
                                                ->orWhere('evaluation', 'LIKE', '%Devoir%');
                                })
                        ->where('code_rentree', session('rentree'))
                        ->where('specialite', $specialite)
                        ->where('niveau', session('niveau'))
                        ->where('semestre', session('semestre'))
                        ->get()->count();
        }

        if(!empty($info))
        {
            echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Synchronisé le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
        }
        else
        {
            echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Aucune ligne synchronisée. ") );
        }

    }
    /******************************************** */

    //septembre 2020-2021
    public function licence_septembre()
    {
        $session_var = session('session_compo');

        $code_niv = explode(" ", session('niveau'));
        $n1 = $code_niv[0]; $n2= $code_niv[1];
        $niveau_code = substr($n1, 0, 1).$n2;

        if($session_var == "SESSION 1") //si la session est normale, on recupere les notes devoir & examen
        {
            //RECUPERATION DES ETUDIANTS AVEC LE PARAMETRE RENTREE
            //recuperer tout le contenu du webservice
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_student/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas2("delib_etudiant", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //boucle des données avec la clé (cours) du resultat_json
            if(!empty($json['etudiants']))
            {
                $nb_etudiant = 0;
                foreach($json['etudiants'] as $d)
                {
                    //var_dump($d['id_cours']);
                    $datas = array('code_etudiant'=>$d['id_user'], 'num_bac'=>$d['num_bac'], 'id_auto'=>$d['id_auto'], 'niveau'=>session('niveau'),
                                    'parcours'=>$d['niveau'], 'semestre'=> session('semestre'), 'specialite'=> $d['specialite'],
                                    'annee_univ'=>session('annee_univ'), 'code_rentree'=> $d['code_rentree'],
                                    'nom'=>$d['nom'], 'prenoms'=>$d['prenoms'], 'email'=>$d['email'], );

                    //enregistrement des données dans la table matiere
                    $this->nano->saveData('delib_etudiant', $datas);
                    $nb_etudiant++;
                }
            }
            else
            {
                $nb_etudiant = 0;
            }


            //RECUPERATION DES MATIERES DE COURS (ECUE) AVEC LE PARAMETRE RENTREE
            //on vide la table
            $this->nano->deleteDatas2("delib_matiere", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            //on vide la table
            $this->nano->deleteDatas2("delib_devoir_temp", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));
            $this->nano->deleteDatas2("delib_devoir", "niveau", "code_rentree", "semestre", session('niveau'), session('rentree'), session('semestre'));

            /**************       ***************/

            // //RECUPERATION DES MATIERES SUR PARTIEL 1
            $result = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL1",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == $niveau_code && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES MATIERES SUR PARTIEL 2
            $result = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_course_examen/'.session('rentree').'/'.session('semestre'), false);
            $json   = json_decode($result, true);

            //on vide la table
            $this->nano->deleteDatas5("delib_matiere_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            //boucle des données avec la clé (cours) du resultat_json
            foreach($json['cours'] as $d)
            {
                $datas = array('id_cours'=>$d['id_cours'], 'nom_cours'=>$d['nom_cours'], 'semestre'=> $d['semestre'], 'code_rentree'=>session('rentree'),
                                'niveau'=> session('niveau'), 'annee_univ'=>session('annee_univ'), 'specialite'=> $d['specialite'], 'session'=> $d['session'], 'plateforme'=>"PARTIEL2",);

                //enregistrement des données dans la table matiere
                if($d['niveau'] == $niveau_code && $d['semestre'] == session('semestre') && $d['session'] == $session_var)
                {
                    $this->nano->saveData('delib_matiere_compo', $datas);
                }
            }


            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 1
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL1", session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                            ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL1")->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel1.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL1",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre')  && $value->session == $session_var)
                        {
                            $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            //RECUPERATION DES NOTES DE COMPO SUR PARTIEL 2
            $this->nano->deleteDatas5("delib_compo", "niveau", "plateforme", "code_rentree", "semestre", "session", session('niveau'), "PARTIEL2", session('rentree'), session('semestre'), $session_var);

            $sql = DB::table('delib_matiere_compo')
                        ->where('niveau', session('niveau'))->where('plateforme', "PARTIEL2")->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->where('session', $session_var)->get();

            foreach ($sql as $value)
            {
                $results = file_get_contents('http://partiel2.uvci.edu.ci/api_deliberation/api/get_note_examen/'.$value->id_cours.'/'.session('rentree').'/'.session('semestre'), false);
                $json   = json_decode($results, true);

                //SI L'ECUE contient des notes
                if($json['success'] == true)
                {
                    foreach($json['note'] as $d)
                    {
                        $datas = array('code_note'=>$d['id_note'], 'code_matiere'=>$d['id_matiere'], 'code_devoir'=>$d['id_devoir'], 'code_rentree'=>session('rentree'),
                                        'code_user'=>$d['id_user'], 'id_auto'=>$d['id_auto'], 'email_user'=>$d['user_email'], 'nom_matiere'=>$d['nom_cours'], 'reference'=>$d['reference'], 'note'=>floor(($d['note']*100))/100,
                                        'niveau'=>session('niveau'), 'specialite'=>$d['specialite'], 'annee_univ'=>session('annee_univ'), 'plateforme'=> "PARTIEL2",
                                        'session'=> $value->session, 'semestre'=>$value->semestre, );

                        //enregistrement des données dans la table matiere
                        if($d['niveau'] == session('niveau') && $d['semestre'] == session('semestre') && $value->session == $session_var )
                        {
                             $this->nano->saveData('delib_compo', $datas);
                        }

                    }
                }
            }

            $this->write_ecue();

            $this->fill_devoir();

            //remplissage de la table delib_note
            $this->load_table_note_septembre();

        }

        echo json_encode(array("status" => TRUE, "nb_etudiant"=> $nb_etudiant, "nb_cours"=>'', "nb_note"=>'', ));
    }

    public function write_ecue()
    {
        $sql = DB::table('delib_compo')
                            ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                            ->where('semestre', session('semestre'))
                            ->groupBy('nom_matiere')->groupBy('specialite')->get();

        foreach($sql as $d)
        {
            $datas = array('code_rentree'=>session('rentree'), 'code_matiere'=>$d->code_devoir, 'libelle'=>trim($d->nom_matiere), 'niveau'=> session('niveau'),
            'annee_univ'=>session('annee_univ'), 'specialite'=> $d->specialite, 'etat'=>true, 'semestre'=> $d->semestre);

            $this->nano->saveData('delib_matiere', $datas);
        }
    }

    public function update_coef()
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))->get();
        foreach($sql as $matiere)
        {
            $nbr = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM v_note_devoir WHERE code_rentree="'.session('rentree').'" AND niveau="'.session('niveau').'" AND annee_univ="'.session('annee_univ').'" AND code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$matiere->specialite.'" AND etat=0 GROUP BY code_user) AS nb');

            $data = array("coef" => $nbr->nombre);
            //var_dump($data); exit;

            DB::table("delib_devoir")
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))
                        ->where('code_matiere', $matiere->code_matiere)
                        ->update($data);
        }
    }

    public function update_coefs($matiere)
    {
        $nbr = DB::selectOne('SELECT MAX(nbr) AS nombre FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE code_rentree="'.session('rentree').'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND annee_univ="'.session('annee_univ').'" AND code_matiere="'.$matiere.'" AND etat=0 GROUP BY code_user) AS nb');

        $data = array("coef" => $nbr->nombre);

        DB::table("delib_devoir")
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('annee_univ', session('annee_univ'))
                    ->where('semestre', session('semestre'))
                    ->where('code_matiere', $matiere)
                    ->update($data);
    }

    //supprimer les notes de devoirs les plus faibles par matieres et par devoir
    public function delete_min_note()
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))->groupBy('code_matiere')->get();
        foreach($sql as $matiere)
        {
            $rek = DB::table('delib_devoir_temp')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('annee_univ', session('annee_univ'))
                        ->where('code_matiere', $matiere->code_matiere)->groupBy('evaluation')->get();

            foreach($rek as $d)
            {
                DB::delete("DELETE t1 FROM delib_devoir_temp t1 INNER JOIN delib_devoir_temp t2
                                WHERE t1.note < t2.note AND
                                t1.code_user = t2.code_user AND t1.code_matiere='".$matiere->code_matiere."' AND t1.evaluation='".$d->evaluation."'
                                AND t1.niveau='".session('niveau')."' AND t1.annee_univ='".session('annee_univ')."' AND t1.code_rentree='".session('rentree')."' ");
            }

            /*DB::delete("DELETE FROM delib_devoir_temp WHERE code_matiere='".$matiere->code_matiere."'
                                AND note NOT IN(SELECT note from (SELECT MIN(note) as note from delib_devoir_temp GROUP BY evaluation, code_user) AS t) ");*/

        }
    }

    public function fill_miror($specialite = null)
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
            $rek = DB::select(DB::raw('SELECT id_note, annee_univ, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation, specialite
                                FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" AND evaluation in (
                                        SELECT evaluation
                                            FROM delib_devoir_temp
                                            WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                        GROUP BY  evaluation, code_user
                                ) GROUP BY evaluation, code_user'));

            foreach($rek as $i => $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=>session('semestre'),
                                'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> session('niveau'), 'specialite'=> $d->specialite, 'annee_univ'=>session('annee_univ'));

                //enregistrement des données dans la table
                $this->nano->saveData('delib_devoir', $datas);
            }
        }
    }

    public function fill_miror_new($specialite = null)
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
            $rek = DB::select(DB::raw('SELECT id_note, annee_univ, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation, specialite
                                FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" AND evaluation in (
                                        SELECT evaluation
                                            FROM delib_devoir_temp
                                            WHERE code_matiere="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                        GROUP BY  evaluation, code_user
                                ) GROUP BY evaluation, code_user'));

            foreach($rek as $i => $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=>session('semestre'),
                                'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> session('niveau'), 'specialite'=> $d->specialite, 'annee_univ'=>session('annee_univ'));

                if($d->evaluation != "SESSION 2")
                {
                    //enregistrement des données dans la table
                    $this->nano->saveData('delib_devoir', $datas);
                }
            }
        }
    }

    //pour la prise en compte des specialités
    public function fill_miror_new2($specialite)
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('specialite', $specialite)
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            //requete pour obtenir la note maximale pour un etudiant dans un devoir par matiere
            $rek = DB::select(DB::raw('SELECT id_note, annee_univ, niveau, code_note, code_matiere, code_devoir, code_user, max(note) as note, evaluation, specialite
                                FROM delib_devoir_temp WHERE code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$specialite.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" AND evaluation in (
                                        SELECT evaluation
                                            FROM delib_devoir_temp
                                            WHERE code_matiere="'.$matiere->code_matiere.'" AND specialite="'.$specialite.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'"
                                        GROUP BY  evaluation, code_user
                                ) GROUP BY evaluation, code_user'));

            foreach($rek as $i => $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=>session('semestre'),
                                'code_user'=>$d->code_user, 'evaluation'=>$d->evaluation, 'note'=>$d->note, 'niveau'=> session('niveau'), 'specialite'=> $d->specialite, 'annee_univ'=>session('annee_univ'));

                if($d->evaluation != "SESSION 2")
                {
                    //enregistrement des données dans la table
                    $this->nano->saveData('delib_devoir', $datas);
                }
            }
        }
    }

    //pour rentree speciale de septembre 2020-2021
    public function fill_devoir()
    {
        $sql = DB::table('delib_matiere')
                        ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                        ->where('semestre', session('semestre'))->groupBy('code_matiere')->get();

        foreach($sql as $matiere)
        {
            $rek = DB::select('SELECT * FROM delib_compo WHERE code_devoir="'.$matiere->code_matiere.'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_rentree="'.session('rentree').'" ');

            foreach($rek as $d)
            {
                $datas = array('code_note'=>$d->id_note, 'code_matiere'=>$matiere->code_matiere, 'code_devoir'=>$d->code_devoir, 'code_rentree'=>session('rentree'), 'semestre'=> $d->semestre,
                                'code_user'=>$d->code_user, 'evaluation'=>"Devoir 01", 'note'=> 0, 'niveau'=> session('niveau'), 'specialite'=>$d->specialite, 'annee_univ'=>session('annee_univ'));

                //enregistrement des données dans la table
                $this->nano->saveData('delib_devoir_temp', $datas);
                $this->nano->saveData('delib_devoir', $datas);
            }
        }
    }

    //pour la session 1
    public function load_table_note()
    {
        set_time_limit(0);
        $session = session('session_compo');

        if($session == "SESSION 1")
        {
            //SUPRESSION DES LIGNES
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);

            //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
            $etudiant = DB::table('v_etudiant_matiere')
                                ->where('niveau', session('niveau'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('etat', true)->get();

            foreach($etudiant as $d)
            {
                $specialite = $d->specialite; $matiere = $d->libelle;

                //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
                $nb_devoir = DB::table('v_note_devoir')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

                $nb_examen = DB::table('v_note_examen')
                                    ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('specialite', $specialite)->where('email', $d->email )->count();

                if($nb_devoir >= 1 || $nb_examen>0)
                {
                    //on va enregistrer la ligne de l'étudiant
                    $examen = DB::table('v_note_examen')
                                    ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('nom_matiere', 'LIKE', "%".$d->libelle."%")
                                    ->where('specialite', $specialite)->where('email', $d->email)->first();

                    //coefficient (nbre de note)
                    //$coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE  delib_etudiant.code_rentree="'.session('rentree').'" AND delib_etudiant.niveau="'.session('niveau').'" AND delib_devoir.semestre="'.session('semestre').'" AND delib_devoir.code_matiere="'.$d->code_matiere.'" AND delib_etudiant.specialite="'.$specialite.'" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');
                    $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.session('rentree').'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'" AND etat=0 GROUP BY code_user) AS nb');

                    //var_dump($coef); exit;

                    $note_examen = (!empty($examen)) ? $examen->note : "";
                    $etat_examen = (!empty($examen)) ? false : true;
                    $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                    $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                    $session_examen = (!empty($examen)) ? $examen->session : $session;

                    //NOTES DE DEVOIR
                    $note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t2.annee_univ="'.session('annee_univ').'" GROUP BY t1.code_matiere, t1.code_user');

                    if(!empty($note))
                    {
                        $notes = explode(",", $note->note);
                        $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                        $merge= array(array_combine($dev, $notes));

                        $note_devoir1 = (array_key_exists("Devoir01", $merge[0])) ? (floor(($merge[0]["Devoir01"]*100))/100) : "";
                        $note_devoir2 = (array_key_exists("Devoir02", $merge[0])) ? (floor(($merge[0]["Devoir02"]*100))/100) : "";
                        $note_devoir3 = (array_key_exists("Devoir03", $merge[0])) ? (floor(($merge[0]["Devoir03"]*100))/100) : "";
                        $note_devoir4 = (array_key_exists("Devoir04", $merge[0])) ? (floor(($merge[0]["Devoir04"]*100))/100) : "";
                        $note_devoir5 = (array_key_exists("Devoir05", $merge[0])) ? (floor(($merge[0]["Devoir05"]*100))/100) : "";
                        $note_devoir6 = (array_key_exists("Devoir06", $merge[0])) ? (floor(($merge[0]["Devoir06"]*100))/100) : "";
                    }
                    else
                    {
                        $note_devoir1=""; $note_devoir2=""; $note_devoir3=""; $note_devoir4=""; $note_devoir5=""; $note_devoir6="";
                    }

                    //var_dump($d->code_etudiant." | ".$d->code_matiere." | ".$note_devoir1." | ".$note_devoir2." | ".$note_devoir3." | ".$note_devoir4." | ".$note_devoir5. " | ".$note_devoir6); exit;

                    $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                    "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                    "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff,
                                    "devoir1"=> $note_devoir1, "devoir2"=> $note_devoir2, "devoir3"=> $note_devoir3,
                                    "devoir4"=> $note_devoir4, "devoir5"=> $note_devoir5, "devoir6"=> $note_devoir6, );
                    //var_dump($datas);

                    $assoc = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                    if(empty($assoc))
                    {
                        //on enregistre la ligne
                        $this->nano->saveData('delib_note', $datas);
                    }

                }

            }
        }
        else
        {

        }



        //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
        $info = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->get()->count();

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Fusionné le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
    }

    //pour la session 1
    public function load_table_note_septembre()
    {
        $session = session('session_compo');

        if($session == "SESSION 1")
        {
            //SUPRESSION DES LIGNES
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);

            //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
            $etudiant = DB::table('v_etudiant_matiere')
                                ->where('niveau', session('niveau'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('etat', true)->get();

            foreach($etudiant as $d)
            {
                $specialite = $d->specialite; $matiere = $d->libelle;

                //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
                $nb_devoir = DB::table('v_note_devoir')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

                $nb_examen = DB::table('v_note_examen')
                                    ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('specialite', $specialite)->where('email', $d->email )->count();

                if($nb_devoir >= 1 || $nb_examen>0)
                {
                    //on va enregistrer la ligne de l'étudiant
                    $examen = DB::table('v_note_examen')
                                    ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('nom_matiere', 'LIKE', "%".$d->libelle."%")
                                    ->where('specialite', $specialite)->where('email', $d->email)->first();

                    //coefficient (nbre de note)
                    //$coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE  delib_etudiant.code_rentree="'.session('rentree').'" AND delib_etudiant.niveau="'.session('niveau').'" AND delib_devoir.semestre="'.session('semestre').'" AND delib_devoir.code_matiere="'.$d->code_matiere.'" AND delib_etudiant.specialite="'.$specialite.'" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');
                    $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.session('rentree').'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'" AND etat=0 GROUP BY code_user) AS nb');

                    //var_dump($coef); exit;

                    $note_examen = (!empty($examen)) ? $examen->note : "";
                    $etat_examen = (!empty($examen)) ? false : true;
                    $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                    $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                    $session_examen = (!empty($examen)) ? $examen->session : $session;

                    //NOTES DE DEVOIR
                    $note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t2.annee_univ="'.session('annee_univ').'" GROUP BY t1.code_matiere, t1.code_user');

                    if(!empty($note))
                    {
                        $notes = explode(",", $note->note);
                        $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                        $merge= array(array_combine($dev, $notes));

                        $note_devoir1 = (array_key_exists("Devoir01", $merge[0])) ? (floor(($merge[0]["Devoir01"]*100))/100) : "";
                        $note_devoir2 = (array_key_exists("Devoir02", $merge[0])) ? (floor(($merge[0]["Devoir02"]*100))/100) : "";
                        $note_devoir3 = (array_key_exists("Devoir03", $merge[0])) ? (floor(($merge[0]["Devoir03"]*100))/100) : "";
                        $note_devoir4 = (array_key_exists("Devoir04", $merge[0])) ? (floor(($merge[0]["Devoir04"]*100))/100) : "";
                        $note_devoir5 = (array_key_exists("Devoir05", $merge[0])) ? (floor(($merge[0]["Devoir05"]*100))/100) : "";
                        $note_devoir6 = (array_key_exists("Devoir06", $merge[0])) ? (floor(($merge[0]["Devoir06"]*100))/100) : "";
                    }
                    else
                    {
                        $note_devoir1=""; $note_devoir2=""; $note_devoir3=""; $note_devoir4=""; $note_devoir5=""; $note_devoir6="";
                    }

                    //var_dump($d->code_etudiant." | ".$d->code_matiere." | ".$note_devoir1." | ".$note_devoir2." | ".$note_devoir3." | ".$note_devoir4." | ".$note_devoir5. " | ".$note_devoir6); exit;

                    $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                    "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                    "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff,
                                    "devoir1"=> $note_devoir1, "devoir2"=> $note_devoir2, "devoir3"=> $note_devoir3,
                                    "devoir4"=> $note_devoir4, "devoir5"=> $note_devoir5, "devoir6"=> $note_devoir6, );
                    //var_dump($datas);

                    $assoc = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                    if(empty($assoc))
                    {
                        //on enregistre la ligne
                        $this->nano->saveData('delib_note', $datas);
                    }

                }

            }
        }
        else
        {

        }
    }

    //pour la session 1
    public function load_table_note_new()
    {
        set_time_limit(0);

        $session = session('session_compo');
        $specialite = $_POST['specialite'];

        if($session == "SESSION 1")
        {
            if($specialite == "TOUS")
            {
                //SUPRESSION DES LIGNES
                $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);
                $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);

                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion1', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();
            }
            else
            {
                //SUPRESSION DES LIGNES
                $this->nano->deleteDatas5("delib_note", "niveau", "code_rentree", "semestre", "session", "specialite", session('niveau'), session('rentree'), session('semestre'), $session, $specialite);
                $this->nano->deleteDatas5("delib_note_finale", "niveau", "code_rentree", "semestre", "session", "specialite", session('niveau'), session('rentree'), session('semestre'), $session, $specialite);

                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))->where('specialite', $specialite)
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion1', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))->where('specialite', $specialite)
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();

            }

            foreach($liste_etudiant as $etu)
            {
                // if(time() - $dbtimestamp > 4*60)
                // {
                //     // 4 mins has passed
                //     sleep(5);
                // }
                // else
                // {
                //     echo date('H:i:s', time())."<br>";
                // }

                $etudiant = DB::table('v_etudiant_matiere')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_etudiant', $etu->code_etudiant)
                                    ->get();

                foreach($etudiant as $d)
                {
                    $specialite = $d->specialite; $matiere = $d->libelle;

                    //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
                    $nb_devoir = DB::table('v_note_devoir')
                                        ->where('niveau', session('niveau'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

                    if($nb_devoir >= 1 )
                    {
                        //on va enregistrer la ligne de l'étudiant
                        $examen = DB::table('v_note_devoir')
                                        ->where('niveau', session('niveau'))->where('evaluation', session('session_compo'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('code_matiere', $d->code_matiere)
                                        ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->first();

                        //coefficient (nbre de note de devoir)
                        $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.session('rentree').'"
                                                AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'"
                                                AND evaluation LIKE "%Devoir%" AND etat=0 GROUP BY code_user) AS nb');

                        //var_dump($coef); exit;

                        $note_examen = (!empty($examen)) ? $examen->note : "";
                        $etat_examen = (!empty($examen)) ? false : true;
                        $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                        $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                        $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                        //NOTES DE DEVOIR
                        //$note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t1.evaluation LIKE "%Devoir%" GROUP BY t1.code_matiere, t1.code_user');
                        $note = DB::selectOne('SELECT GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 WHERE t1.code_rentree="'.session('rentree').'"
                                                AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t1.specialite="'.$specialite.'"
                                                AND t1.evaluation LIKE "%Devoir%" GROUP BY t1.code_matiere, t1.code_user');

                        if(!empty($note))
                        {
                            $notes = explode(",", $note->note);
                            $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                            $merge= array(array_combine($dev, $notes));

                            $note_devoir1 = (array_key_exists("Devoir01", $merge[0])) ? (floor(($merge[0]["Devoir01"]*100))/100) : "";
                            $note_devoir2 = (array_key_exists("Devoir02", $merge[0])) ? (floor(($merge[0]["Devoir02"]*100))/100) : "";
                            $note_devoir3 = (array_key_exists("Devoir03", $merge[0])) ? (floor(($merge[0]["Devoir03"]*100))/100) : "";
                            $note_devoir4 = (array_key_exists("Devoir04", $merge[0])) ? (floor(($merge[0]["Devoir04"]*100))/100) : "";
                            $note_devoir5 = (array_key_exists("Devoir05", $merge[0])) ? (floor(($merge[0]["Devoir05"]*100))/100) : "";
                            $note_devoir6 = (array_key_exists("Devoir06", $merge[0])) ? (floor(($merge[0]["Devoir06"]*100))/100) : "";
                        }
                        else
                        {
                            $note_devoir1=""; $note_devoir2=""; $note_devoir3=""; $note_devoir4=""; $note_devoir5=""; $note_devoir6="";
                        }

                        //var_dump($d->code_etudiant." | ".$d->code_matiere." | ".$note_devoir1." | ".$note_devoir2." | ".$note_devoir3." | ".$note_devoir4." | ".$note_devoir5. " | ".$note_devoir6); exit;

                        $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                        "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                        "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff,
                                        "devoir1"=> $note_devoir1, "devoir2"=> $note_devoir2, "devoir3"=> $note_devoir3,
                                        "devoir4"=> $note_devoir4, "devoir5"=> $note_devoir5, "devoir6"=> $note_devoir6, );
                        //var_dump($datas);

                        $assoc = DB::table('delib_note')
                                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                    ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                        if(empty($assoc))
                        {
                            //on enregistre la ligne
                            $this->nano->saveData('delib_note', $datas);
                        }
                    }
                }

                $datas = array("etat_fusion1" => true);

                DB::table('delib_etudiant')
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))
                    ->where('code_etudiant', $etu->code_etudiant)->update($datas);
            }
        }
        else
        {
            if($specialite == "TOUS")
            {
                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion2', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();
            }
            else
            {
                //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
                $liste_etudiant = DB::table('delib_etudiant')
                                    ->where('niveau', session('niveau'))->where('specialite', $specialite)
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('etat_fusion2', false)
                                    ->get();

                // $etudiant = DB::table('v_etudiant_matiere')
                //                     ->where('niveau', session('niveau'))->where('specialite', $specialite)
                //                     ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                //                     ->get();
            }

            foreach($liste_etudiant as $etu)
            {
                $etudiant = DB::table('v_etudiant_matiere')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_etudiant', $etu->code_etudiant)
                                    ->get();

                foreach($etudiant as $d)
                {
                    $specialite = $d->specialite; $matiere = $d->libelle;

                    //on va verifier si l'etudiant a une ligne
                    $nb_devoir = DB::table('delib_note')
                                        ->where('niveau', session('niveau'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('id_auto', $d->id_auto)->where('code_matiere', $d->code_matiere)->count();

                    if($nb_devoir >= 1 )
                    {
                        //on va enregistrer la ligne de l'étudiant
                        $examen = DB::table('v_note_devoir')
                                        ->where('niveau', session('niveau'))->where('evaluation', session('session_compo'))
                                        ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                        ->where('code_matiere', $d->code_matiere)
                                        ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->first();

                        $note_examen = (!empty($examen)) ? $examen->note : "";
                        $etat_examen = (!empty($examen)) ? false : true;

                        $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                        $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                        //var_dump($d->code_etudiant." | ".$d->code_matiere." | ".$note_devoir1." | ".$note_devoir2." | ".$note_devoir3." | ".$note_devoir4." | ".$note_devoir5. " | ".$note_devoir6); exit;

                        $datas = array("session"=>$session, "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, );
                        //var_dump($datas);

                        if($note_examen != "")
                        {
                            $assoc = DB::table('delib_note')
                                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_matiere', $d->code_matiere)->where('specialite', $specialite)->where('id_auto', $d->id_auto)
                                    ->update($datas);
                        }

                    }
                }

                $datas = array("etat_fusion2" => true);

                DB::table('delib_etudiant')
                    ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                    ->where('semestre', session('semestre'))
                    ->where('code_etudiant', $etu->code_etudiant)->update($datas);

            }
        }


        //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
        $info = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->where('session', session('session_compo'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        if($specialite == "TOUS")
        {
            $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('session', session('session_compo'))
                                ->where('semestre', session('semestre'))->get()->count();
        }
        else
        {
            $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('session', session('session_compo'))->where('specialite', $specialite)
                                ->where('semestre', session('semestre'))->get()->count();
        }

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Fusionné le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
    }

    public function load_table_note_new__()
    {
        $session = session('session_compo');

        if($session == "SESSION 1")
        {
            //SUPRESSION DES LIGNES
            $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);
            $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);

            //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
            $etudiant = DB::table('v_etudiant_matiere')
                                ->where('niveau', session('niveau'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->skip(0)->take(100)->get();

            foreach($etudiant as $d)
            {
                $specialite = $d->specialite; $matiere = $d->libelle;

                //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
                $nb_devoir = DB::table('delib_devoir')
                                    ->where('niveau', session('niveau'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->get()->count();

                if($nb_devoir >= 1 )
                {
                    // //on va enregistrer la ligne de l'étudiant
                    $examen = DB::table('v_note_devoir')
                                    ->where('niveau', session('niveau'))->where('evaluation', session('session_compo'))
                                    ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                    ->where('code_matiere', $d->code_matiere)
                                    ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->first();

                    //coefficient (nbre de note de devoir)
                    $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.session('rentree').'"
                                            AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'"
                                            AND evaluation LIKE "%Devoir%" AND etat=0 GROUP BY code_user) AS nb');

                    //var_dump($coef); exit;

                    $note_examen = (!empty($examen)) ? $examen->note : "";
                    $etat_examen = (!empty($examen)) ? false : true;
                    $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                    $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                    $session_examen = (!empty($examen)) ? $examen->evaluation : $session;

                    //NOTES DE DEVOIR
                    //$note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t1.evaluation LIKE "%Devoir%" GROUP BY t1.code_matiere, t1.code_user');
                    $note = DB::selectOne('SELECT GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 WHERE t1.code_rentree="'.session('rentree').'"
                                            AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t1.specialite="'.$specialite.'"
                                            AND t1.evaluation LIKE "%Devoir%" GROUP BY t1.code_matiere, t1.code_user');

                    if(!empty($note))
                    {
                        $notes = explode(",", $note->note);
                        $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                        $merge= array(array_combine($dev, $notes));

                        $note_devoir1 = (array_key_exists("Devoir01", $merge[0])) ? (floor(($merge[0]["Devoir01"]*100))/100) : "";
                        $note_devoir2 = (array_key_exists("Devoir02", $merge[0])) ? (floor(($merge[0]["Devoir02"]*100))/100) : "";
                        $note_devoir3 = (array_key_exists("Devoir03", $merge[0])) ? (floor(($merge[0]["Devoir03"]*100))/100) : "";
                        $note_devoir4 = (array_key_exists("Devoir04", $merge[0])) ? (floor(($merge[0]["Devoir04"]*100))/100) : "";
                        $note_devoir5 = (array_key_exists("Devoir05", $merge[0])) ? (floor(($merge[0]["Devoir05"]*100))/100) : "";
                        $note_devoir6 = (array_key_exists("Devoir06", $merge[0])) ? (floor(($merge[0]["Devoir06"]*100))/100) : "";
                    }
                    else
                    {
                        $note_devoir1=""; $note_devoir2=""; $note_devoir3=""; $note_devoir4=""; $note_devoir5=""; $note_devoir6="";
                    }

                    //var_dump($d->code_etudiant." | ".$d->code_matiere." | ".$note_devoir1." | ".$note_devoir2." | ".$note_devoir3." | ".$note_devoir4." | ".$note_devoir5. " | ".$note_devoir6); exit;

                    $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                    "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                    "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff,
                                    "devoir1"=> $note_devoir1, "devoir2"=> $note_devoir2, "devoir3"=> $note_devoir3,
                                    "devoir4"=> $note_devoir4, "devoir5"=> $note_devoir5, "devoir6"=> $note_devoir6, );
                    //var_dump($datas);

                    $assoc = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                    if(empty($assoc))
                    {
                        //on enregistre la ligne
                        $this->nano->saveData('delib_note', $datas);
                    }

                }

            }
        }
        else
        {

        }



        //recuperation des infos de la derniere ligne synchronisé pour afficher la date de la synchro
        $info = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->get()->count();

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Fusionné le "));
    }

    //pour la session 2
    public function load_table_note2()
    {
        $session = session('session_compo');

        //supression des lignes
        $this->nano->deleteDatas4("delib_note", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);
        $this->nano->deleteDatas4("delib_note_finale", "niveau", "code_rentree", "semestre", "session", session('niveau'), session('rentree'), session('semestre'), $session);

        //PROCESS POUR ENREGISTRER LES NOTES FINALES DE DEVOIR POUR CHAQUE ETUDIANT
        $etudiant = DB::table('v_etudiant_matiere')
                            ->where('niveau', session('niveau'))
                            ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                            ->where('etat', true)->get();

        foreach($etudiant as $d)
        {
            $specialite = $d->specialite; $matiere = $d->libelle;

            //on va compter le nbre de devoir de l'etudiant (devoir >=1) ou ayant fait au moins une compo
            $nb_devoir = DB::table('v_note_devoir')
                                ->where('niveau', session('niveau'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('specialite', $specialite)->where('code_user', $d->code_etudiant)->count();

            $nb_examen = DB::table('v_note_examen')
                                ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('specialite', $specialite)->where('email', $d->email )->count();

            if($nb_devoir >= 1 || $nb_examen>0)
            {
                //on verifie si l'etudiant a composé en session 2
                $examen = DB::table('v_note_examen')
                                ->where('niveau', session('niveau'))->where('session', session('session_compo'))
                                ->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('nom_matiere', 'LIKE', "%".$d->libelle."%")
                                ->where('specialite', $specialite)->where('email', $d->email)->first();

                if(!empty($examen))
                {
                    //coefficient (nbre de note)
                    //$coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir INNER JOIN delib_etudiant ON(delib_devoir.code_user=delib_etudiant.code_etudiant) WHERE  delib_etudiant.code_rentree="'.session('rentree').'" AND delib_etudiant.niveau="'.session('niveau').'" AND delib_devoir.semestre="'.session('semestre').'" AND delib_devoir.code_matiere="'.$d->code_matiere.'" AND delib_etudiant.specialite="'.$specialite.'" AND delib_devoir.etat=0 GROUP BY delib_devoir.code_user) AS nb');
                    $coef = DB::selectOne('SELECT MAX(nbr) AS nombre, code_user FROM (SELECT code_user, COUNT(code_user) AS nbr FROM delib_devoir WHERE  code_rentree="'.session('rentree').'" AND niveau="'.session('niveau').'" AND semestre="'.session('semestre').'" AND code_matiere="'.$d->code_matiere.'" AND specialite="'.$specialite.'" AND etat=0 GROUP BY code_user) AS nb');

                    //var_dump($coef); exit;

                    $note_examen = (!empty($examen)) ? $examen->note : "";
                    $etat_examen = (!empty($examen)) ? false : true;
                    $coeff = ($coef->nombre != null) ? $coef->nombre : 0;

                    $semestre_examen = (!empty($examen)) ? $examen->semestre : session('semestre');
                    $session_examen = (!empty($examen)) ? $examen->session : $session;

                    //NOTES DE DEVOIR
                    $note = DB::selectOne('SELECT t2.*, GROUP_CONCAT(t1.note ORDER BY evaluation) AS note, GROUP_CONCAT(t1.evaluation ORDER BY evaluation) AS evaluation FROM delib_devoir t1 LEFT JOIN delib_etudiant t2 ON t2.code_etudiant = t1.code_user WHERE t1.code_rentree="'.session('rentree').'" AND t1.niveau="'.session('niveau').'" AND t1.semestre="'.session('semestre').'" AND t1.code_user="'.$d->code_etudiant.'" AND t1.code_matiere="'.$d->code_matiere.'" AND t2.specialite="'.$specialite.'" AND t2.annee_univ="'.session('annee_univ').'" GROUP BY t1.code_matiere, t1.code_user');

                    if(!empty($note))
                    {
                        $notes = explode(",", $note->note);
                        $dev = explode(",", str_replace(' ', '',  $note->evaluation));

                        $merge= array(array_combine($dev, $notes));

                        $note_devoir1 = (array_key_exists("Devoir01", $merge[0])) ? (floor(($merge[0]["Devoir01"]*100))/100) : "";
                        $note_devoir2 = (array_key_exists("Devoir02", $merge[0])) ? (floor(($merge[0]["Devoir02"]*100))/100) : "";
                        $note_devoir3 = (array_key_exists("Devoir03", $merge[0])) ? (floor(($merge[0]["Devoir03"]*100))/100) : "";
                        $note_devoir4 = (array_key_exists("Devoir04", $merge[0])) ? (floor(($merge[0]["Devoir04"]*100))/100) : "";
                        $note_devoir5 = (array_key_exists("Devoir05", $merge[0])) ? (floor(($merge[0]["Devoir05"]*100))/100) : "";
                        $note_devoir6 = (array_key_exists("Devoir06", $merge[0])) ? (floor(($merge[0]["Devoir06"]*100))/100) : "";
                    }
                    else
                    {
                        $note_devoir1=""; $note_devoir2=""; $note_devoir3=""; $note_devoir4=""; $note_devoir5=""; $note_devoir6="";
                    }


                    $datas = array("code_rentree" => session('rentree'), "annee_univ" => session('annee_univ'), "niveau"=>session('niveau'), "id_auto"=>$d->id_auto,
                                    "code_matiere"=>$d->code_matiere, "nom_matiere"=>$matiere, "specialite"=>$d->specialite, "semestre"=> $semestre_examen, "session"=>$session,
                                    "note_examen"=>$note_examen, "etat_exam"=> $etat_examen, "cf"=>$coeff, "coef"=>$coeff,
                                    "devoir1"=> $note_devoir1, "devoir2"=> $note_devoir2, "devoir3"=> $note_devoir3,
                                    "devoir4"=> $note_devoir4, "devoir5"=> $note_devoir5, "devoir6"=> $note_devoir6,);
                    //var_dump($datas);

                    $assoc = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))->where('semestre', session('semestre'))
                                ->where('session', session('session_compo'))->where('code_matiere', $d->code_matiere)
                                ->where('specialite', $specialite)->where('id_auto', $d->id_auto)->first();

                    if(empty($assoc))
                    {
                        //on enregistre la ligne
                        $this->nano->saveData('delib_note', $datas);
                    }

                }

            }

        }

        //recuperation des infos de la derniere ligne ynchronisé pour afficher la date de la synchro
        $info = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))
                                ->orderBy('id_note', 'DESC')->skip(0)->take(1)->first();

        $note = DB::table('delib_note')
                                ->where('niveau', session('niveau'))->where('code_rentree', session('rentree'))
                                ->where('semestre', session('semestre'))->get()->count();

        echo json_encode(array("status" => TRUE, "note"=> $note, "info"=> "Fusionné le ".date('d-m-Y à H:i:s', strtotime($info->date_created)) ));
    }

    public function move_note()
    {
        $matiere = DB::table('delib_matiere')->where('niveau', session('niveau'))->where('annee_univ', session('annee_univ'))->groupBy('code_matiere')->get();

        foreach($matiere as $mat)
        {
            $notes = DB::table('delib_devoir_temp')
                                ->where('niveau', session('niveau'))
                                ->where('annee_univ', session('annee_univ'))
                                ->where('code_matiere', $mat->code_matiere)
                                ->where('code_user', $d->code_etudiant)->get();
        }

        $notes = DB::table('delib_devoir_temp')
                                ->where('niveau', session('niveau'))
                                ->where('annee_univ', session('annee_univ'))
                                ->where('code_matiere', $matiere)
                                ->where('code_user', $d->code_etudiant)->get();

        $not = array();
        foreach($data["note"] as $d)
        {


            $nb = count($notes);
            $row = $data['nbr']->nombre - $nb;

            foreach($notes as $d)
            {
                $not[] = number_format((float) round($d->note, 2), 2, '.', '');
            }
        }
    }

    public function get_niveau($niveau)
    {
        $value = "";
        switch ($niveau) {
            case 'LICENCE 1':
                $value = "L1";
                break;
            case 'LICENCE 2':
                $value = "L2";
                break;
            case 'LICENCE 3':
                $value = "L3";
                break;
        }

        return $value;
    }

}
