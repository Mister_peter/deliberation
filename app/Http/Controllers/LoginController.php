<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Connexion;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Hash;

class LoginController extends Controller
{
    protected $connexion;

	public function __construct()
	{
        if(!Session::has('user_id')) return redirect('login');
	}

	public function index()
    {
        $data['niveau'] = DB::table("niveau_etude")->get();
        $data['rentree'] = DB::table("rentree")->where('id_rentree','>', 3)->get();

        return view('login', $data);
    }

    public function connexion(LoginRequest $request)
    {
        $this->connexion = new connexion;

        //remove all sessions
        $request->session()->flush();

        $email = $request->input('email'); $password = $request->input('password'); $niveau = $request->input('niveau');
        $rentree = $request->input('rentree'); $semestre = $request->input('semestre'); $session = $request->input('session_compo');

        //on verifie si le mail est correct
        if($this->connexion->check_email($email) == 1)
        {
            if($this->connexion->check_status($email) == true)
            {
                $data = $this->connexion->get_user($email);

                if($password == $data->password) //on verifie si le mot de passe saisi est conforme a celui de la BD
                {
                    //$annee = DB::table("annee_universitaire")->where("etat_annee", true)->first();
                    $rentrees = DB::table("rentree")->where("code_rentree", $rentree)->first();

                    session(['user_id'=>$data->id_user, 'user_email'=>$data->email, 'user_nom'=>$data->nom, 'user_prenom'=>$data->prenoms,
                            'user_admin'=>$data->admin, 'user_teacher'=>$data->teacher, 'user_jury'=>$data->jury, 'session_compo'=>$session,
                            'annee_univ'=>$rentrees->annee_univ, 'niveau'=>$niveau, 'rentree'=>$rentrees->code_rentree, 'semestre'=>$semestre,] );
                    //var_dump(session('annee_univ')); exit;

                    return redirect('dashboard');
                }
                else
                {
                    return redirect('login')->withError("Désolé, votre mot de passe est incorrect.");
                }
            }
            else
            {
                return redirect('login')->withError("Désolé, votre compte est desactivé.");
            }
        }
        else
        {
            return redirect()->back()->withError("Email non reconnu par le système.");
        }

    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('login');
    }
}
