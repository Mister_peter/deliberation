<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Nano extends Model
{
    protected $table = "";

    //Inserting Records Into A Table
    public function saveData($table, $data)
    {
        DB::table($table)->insert($data);
        return true;
    }

    //Inserting Records Into A Table With An Auto-Incrementing ID
    public function saveId($table, $data)
    {
        DB::table($table)->insertGetId($data);
        return true;
    }

    //Updating Records In A Table
    public function updateData($table, $key, $value, $data)
    {
        DB::table($table)
                ->where($key, $value)
                ->update($data);
        return true;
    }

    //Updating Records In A Table
    public function updates($table, $key1, $key2, $value1, $value2, $data)
    {
        DB::table($table)
                ->where($key1, $value1)
                ->where($key2, $value2)
                ->update($data);
        return true;
    }

    public function getAll($table)
    {
        return DB::table($table)->get();
    }

    public function getQuery($query)
    {
        return DB::select($query);
    }

    public function getId($table, $key, $value)
    {
        return DB::table($table)
                        ->where($key, $value)
                        ->first();
    }

    public function getAllId($table, $key, $value)
    {
        return DB::table($table)
                        ->where($key, $value)
                        ->get();
    }

    public function getIds($table, $key1, $key2, $value1, $value2)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->first();
    }

    public function getId3($table, $key1, $key2, $key3, $value1, $value2, $value3)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->first();
    }

    public function getId4($table, $key1, $key2, $key3, $key4, $value1, $value2, $value3, $value4)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->where($key4, $value4)
                        ->first();
    }

    public function getAllIds($table, $key1, $key2, $value1, $value2)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->get();
    }

    public function getAllIdsGroup($table, $key1, $key2, $value1, $value2, $group)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->groupBy($group)
                        ->get();
    }

    public function count($table)
    {
        return DB::table($table)
                        ->count();
    }

    public function counts($table, $key, $value)
    {
        return DB::table($table)
                        ->where($key, $value)
                        ->count();
    }

    public function deleteData($table, $key, $value)
    {
        return DB::table($table)
                        ->where($key, $value)
                        ->delete();
    }

    public function deleteDatas($table, $key1, $key2, $value1, $value2)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->delete();
    }

    public function deleteDatas2($table, $key1, $key2, $key3, $value1, $value2, $value3)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->delete();
    }

    public function deleteDatas4($table, $key1, $key2, $key3, $key4, $value1, $value2, $value3, $value4)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->where($key4, $value4)
                        ->delete();
    }

    public function deleteDatas4_($table, $key1, $key2, $key3, $key4, $value1, $value2, $value3, $value4)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->where($key4, "<>", $value4)
                        ->delete();
    }

    public function deleteDatas5_($table, $key1, $key2, $key3, $key4, $key5, $value1, $value2, $value3, $value4, $value5)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->where($key4, "<>", $value4)
                        ->where($key5, $value5)
                        ->delete();
    }

    public function deleteDatas5($table, $key1, $key2, $key3, $key4, $key5, $value1, $value2, $value3, $value4, $value5)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->where($key4, $value4)
                        ->where($key5, $value5)
                        ->delete();
    }

    public function deleteDatas6($table, $key1, $key2, $key3, $key4, $key5, $key6, $value1, $value2, $value3, $value4, $value5, $value6)
    {
        return DB::table($table)
                        ->where($key1, $value1)
                        ->where($key2, $value2)
                        ->where($key3, $value3)
                        ->where($key4, $value4)
                        ->where($key5, $value5)
                        ->where($key6, $value6)
                        ->delete();
    }

    public function truncate($table)
    {
        return DB::table($table)
                        ->truncate();
    }
}
