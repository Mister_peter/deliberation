<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Connexion extends Model
{
	//check mail of user 
    public function check_email($email)
    {
    	return DB::table("delib_user")
                        ->where("email", $email)
                        ->count();
    }

    //check le statut du user
    public function check_status($email)
    {
    	return DB::table("delib_user")
                        ->where("email", $email)
                        ->pluck('statut');
    }

    //check le mail & le password du user
    public function get_user($email)
    {
    	return DB::table("delib_user")
                        ->where("email", $email)
                        ->first();
    }
}
