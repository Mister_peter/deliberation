@extends('layouts.master')

@section('css')
<!--Chartist Chart CSS -->
<link rel="stylesheet" href="{{ asset('plugins/chartist/css/chartist.min.css') }}">
@endsection

@section('breadcrumb')
<div class="col-sm-6">
     <h4 class="page-title">TABLEAU DE BORD</h4>
     <ol class="breadcrumb">
         <li class="breadcrumb-item active">Plateforme de délibération</li>
     </ol>
</div>
@endsection

@section('content')
   <div class="row">
        <div class="col-xl-4 col-md-6">
            <div class="card mini-stat bg-success text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4">
                            <img src="{{ asset('assets/images/services-icon/01.png') }}" alt="" >
                        </div>
                    <h5 class="font-16 text-uppercase mt-0 text-white">etudiants - {{ session('niveau') }} </h5>
                        <h4 class="font-500">{{ $niveau }} <i class="mdi mdi-arrow-up text-danger ml-2"></i></h4>
                        <div class="mini-stat-label bg-danger">
                            <p class="mb-0"> <br></p>
                        </div>
                    </div>
                    <!-- <div class="pt-2">
                        <div class="float-right">
                            <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                        </div>
                        <p class="text-white-50 mb-0">Since last month</p>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4">
                            <img src="{{ asset('assets/images/services-icon/02.png') }}" alt="" >
                        </div>
                        <h5 class="font-16 text-uppercase mt-0 text-white">participation - Devoir</h5>
                        <h4 class="font-500"> <i class="mdi mdi-arrow-up text-warning ml-2"></i></h4>
                        <div class="mini-stat-label bg-warning">
                            <p class="mb-0"><br></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6">
            <div class="card mini-stat bg-warning text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4">
                            <img src="{{ asset('assets/images/services-icon/03.png') }}" alt="" >
                        </div>
                        <h5 class="font-16 text-uppercase mt-0 text-white">participation - examen</h5>
                        <h4 class="font-500"> {{ $nb_exam->count().' soit ' }} {{ ($nb_exam->count() != 0) ? round( ($nb_exam->count() * 100)/$niveau, 2) : 0 }}%<i class="mdi mdi-arrow-up text-info ml-2"></i></h4>
                        <div class="mini-stat-label bg-info">
                            <p class="mb-0"><br></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <!--<div class="col-xl-9">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title mb-5">Monthly Earning</h4>
                    <div class="row">
                        <div class="col-lg-7">
                            <div>
                                <div id="chart-with-area" class="ct-chart earning ct-golden-section"></div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text-center">
                                        <p class="text-muted mb-4">This month</p>
                                        <h4>$34,252</h4>
                                        <p class="text-muted mb-5">It will be as simple as in fact it will be occidental.</p>
                                        <span class="peity-donut" data-peity='{ "fill": ["#02a499", "#f2f2f2"], "innerRadius": 28, "radius": 32 }' data-width="72" data-height="72">4/5</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-center">
                                        <p class="text-muted mb-4">Last month</p>
                                        <h4>$36,253</h4>
                                        <p class="text-muted mb-5">It will be as simple as in fact it will be occidental.</p>
                                        <span class="peity-donut" data-peity='{ "fill": ["#02a499", "#f2f2f2"], "innerRadius": 28, "radius": 32 }' data-width="72" data-height="72">3/5</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title mb-4">Taux de participation des examens par spécialité</h4>

                    {{-- <div class="cleafix">
                        <p class="float-left"><i class="mdi mdi-calendar mr-1 text-primary"></i> Jan 01 - Jan 31</p>
                         <h5 class="font-18 text-right">$4230</h5>
                    </div> --}}

                    {{-- <div id="ct-donut" class="ct-chart wid"></div> --}}

                    <div class="mt-4">
                        <table class="table mb-0">
                            <tbody>

                                <tr>
                                    <td>Spécialité</td>
                                    <td>Nbre étudiant</td>
                                    <td>Examen</td>
                                    <td>Pourcentage</td>
                                </tr>

                                @foreach($spec as $i => $d)
                                <tr>
                                    <?php
                                    $nbr = DB::table('v_note_examen')
                                                        ->where('niveau', session('niveau'))
                                                        ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))
                                                        ->where('session', session('session_compo'))
                                                        ->where('specialite', $d->code_specialite)
                                                        ->groupBy('email')->get();

                                    $nbr_spec = DB::table('delib_etudiant')
                                                        ->where('niveau', session('niveau'))
                                                        ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))
                                                        ->where('specialite', $d->code_specialite)->count();
                                    ?>
                                    <td><span class="badge badge-{{ ($i%2==0) ? 'success':'warning'}}">{{ $d->libelle_specialite }}</span></td>
                                    <td><strong>{{ $nbr_spec }} </strong></td>
                                    <td><strong>{{ $nbr->count() }} </strong></td>
                                    <td><strong>{{ ($nbr->count() != 0) ? round((($nbr->count() * 100)/$nbr_spec), 2) : 0}}%</strong></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->
@endsection

@section('script')
<!--Chartist Chart-->
<script src="{{ asset('plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ asset('plugins/chartist/js/chartist-plugin-tooltip.min.js') }}"></script>
<!-- peity JS -->
<script src="{{ asset('plugins/peity-chart/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/pages/dashboard.js') }}"></script>
@endsection
