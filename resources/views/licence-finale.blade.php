@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Liste des notes finales obtenues (Devoir + Examen)</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Délibération</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">{{session('niveau')}}</a></li>
        <li class="breadcrumb-item active">Note</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5>Recherche</h5>

                    <form action="{{ url('show_note_finale') }}" method="get" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="modal-body">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-11">
                                            <select name="specialite" id="specialite" class="form-control" required onchange="selectMatiereExamens(this.options[this.selectedIndex].value)" >
                                                <option value="">Spécialité</option>
                                                <option value="">---------------</option>

                                                @if(!empty($specialite)) @foreach($specialite as $d)
                                                <option value="{{ $d->code_specialite }}"> {{ $d->libelle_specialite }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <img id="loader" src="{{ asset('assets/images/loading.gif') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <select name="matiere" id="matiere" class="form-control" required >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <button type="submit" class="btn btn-success btn-icon" title="Rechercher"><i class="fas fa-search"></i></button>
                                </div>
                            </div>

                        </div>
                      </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @if(!empty($etudiant))
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4><font color="green">{{ $spec }} </font> | ECUE: {{ $matiere->libelle }}</h4><hr>
                    <div class="load"></div>

                    <div class="row">
                        <div class="col-sm-6">
                        <?php
                            $taux_compo = ($compo*100)/$nb_etudiant; $taux_compo = round($taux_compo, 2);
                            $taux_admis = ($admis*100)/$total; $taux_admis = round($taux_admis, 2);
                            $taux_echec = (($echec)*100)/$total; $taux_echec = round($taux_echec, 2);
                        ?>
                        <p style="font-weight: bold">Etudiant: <font color="blue">{{$nbre_etudiant}} </font> | Admis: <font color="green"> {{$admis." (".$taux_admis."%)"}}</font> | Refusé: <font color="red"> {{ ($echec)." (".$taux_echec."%)" }}</font></p>
                        </div>

                        @if($pv == 0)
                        <div class="col-sm-3">
                            <div class="float-left d-none d-md-block">
                                <div class="dropdown">
                                    <button class="btn btn-success dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-settings mr-2"></i> Options de repêchage
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <button type="button" onclick="modal_arrond()" class="dropdown-item" title="Appliquer le traitement">Appliquer un critère</button>
                                        <button type="button" onclick="reset_mga()" class="dropdown-item" title="Annuler le traitement">Reinitialiser</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-sm-3">
                            <div class="float-left d-none d-md-block">
                                <div class="dropdown">
                                    <button class="btn btn-success dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-user mr-2"></i> Liste de présence
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <button type="button" onclick="liste_presence()" class="dropdown-item" title="Appliquer le traitement">Membres du jury</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <br><br>

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Nom & prénoms</th>
                            <th>Devoir</th>
                            <th>M1 (%)</th>
                            <th>Examen</th>
                            <th>M2 (%)</th>
                            <th>MGA (M1+M2)</th>
                            <th>Mention</th>
                            <th>Décision</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody class="">

                        @foreach($etudiant as $d)
                        <tr>
                            <td> {{ $d->nom." ".$d->prenoms }}</td>
                            <td> {{ ($taux->taux1 > 0) ? (($d->devoir*100)/ $taux->taux1) : 0 }} </td>
                            <td> {{ $d->devoir}} </td>
                            <td> {{ ($taux->taux2 > 0) ? (($d->examen*100)/ $taux->taux2) : 0 }} </td>
                            <td> {{ $d->examen }} </td>
                            <td> {{ $d->mga2 }} </td>
                            <td> {{ $d->mention }}</td>
                            <td> {{ $d->decision }} </td>
                            <td>
                                @if($d->mga >0)
                                <button onclick="edit_notes('{{ $d->code_matiere }}', '{{ $d->id_auto }}')" class="btn btn-warning btn-icon" title="Notes de devoirs"><i class="mdi mdi-eye"></i></button>
                                @endif
                                @if($d->etat ==0)
                                <button onclick="edit_note_finale('{{ $d->id_note }}')" class="btn btn-primary btn-icon" title="Modifier la note"><i class="mdi mdi-square-edit-outline"></i></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <br><br>

                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-3">
                            @if($pv == 0)
                            <button type="button" onclick="valider_pv()" class="btn btn-success btn-icon" title="Valider le PV"> VALIDER LE PV </button>
                            @endif
                        </div>
                        <div class="col-sm-3">
                            @if($pv > 0)
                            <button type="button" onclick="reset_pv()" class="btn btn-success btn-icon" title="Annuler le PV"> REINITIALISER LE PV</button>
                            @endif
                        </div>
                    </div>

                    <!-- <button class="float" onclick="" title="Ajouter une matiere"> <i class="ti-plus"></i></button> -->

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @endif
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.cascade')

@if(!empty($matiere))
@include('form_note_devoir')
@include('form_arrondissement')
@include('form_note_finale')
@include('liste_presence')
@endif

@include('script.js')

@endsection

