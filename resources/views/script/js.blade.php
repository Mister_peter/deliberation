<script type="text/javascript">

	//add_form() used to show the modal form
	function add_form()
	{
		$('.submit').removeClass('disabled').attr('disabled', false)
		$('.modal_form')[0].reset(); // reset form on modals
		$('#BtnEnregistrer').html(' Enregistrer');
	  	$('#BtnEnregistrer').attr('value', "Enregistrer");

        $('.tableau').html(" ");
	  	$('.panel-modal').modal('show');

		$(".submit").attr('disabled', false);

	}

    function modal_calcul_devoir()
    {
		$('.modal_form4')[0].reset(); // reset form on modals
	  	$('.panel-modal4').modal('show');
    }

    function modal_calcul_global()
    {
		$('.modal_form4')[0].reset(); // reset form on modals
	  	$('.panel-modal4').modal('show');
    }

    function modal_arrond()
    {
		$('.modal_form')[0].reset(); // reset form on modals
        $('.tableau').html("");
	  	$('.panel-modal').modal('show');
    }

    function modal_calcul_examen()
    {
		$('.modal_form1')[0].reset(); // reset form on modals
	  	$('.panel-modal1').modal('show');
    }

    function delete_devoir()
	{
		$('.modal_form7')[0].reset();
        $('#title7').html("Supprimer un devoir");
        $('.submit7').attr('onclick', "confirmOption()");
		$('.panel-modal7').modal('show');
	}

    function reset_delete_devoir()
	{
		$('.modal_form7')[0].reset();
        $('#title7').html("Réinitialiser un devoir");
        $('.submit7').attr('onclick', "confirmAnnulation()");
		$('.panel-modal7').modal('show');
	}

    function liste_presence()
	{
		$('.modal_form2')[0].reset();
        $('#title2').html("LISTE DE PRESENCE");
		$('.panel-modal2').modal('show');
	}

	function UrlExists(url)
	{
		var http = new XMLHttpRequest();
		http.open('HEAD', url, false);
		http.send();
		return http.status !=404;
	}

	function formatDate (input) {
		var datePart = input.match(/\d+/g),
		year = datePart[0].substring(2), // get only two digits
		month = datePart[1], day = datePart[2];

		return day+'/'+month+'/'+year;
	}

	function format(inputDate) {
	    var date = new Date(inputDate);
	    if (!isNaN(date.getTime())) {
	        var day = date.getDate().toString();
	        var month = (date.getMonth() + 1).toString();
	        // Months use 0 index.

	        return (day[1] ? day : '0' + day[0]) + '-' +
	           (month[1] ? month : '0' + month[0]) + '-' +
	           date.getFullYear()
	    }
	}

    function reset_calcul_global()
	{
        var url = window.location.href;

        swal({
            title: "Réinitialisation",
            text: "Êtes-vous sûr de reinitialiser ce traitement ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('reset_global') }}",
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },

                success: function(data)
                {
                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Traitement effecuté avec succès !",
                        "success");
                    }

                    $('.load').hide();
                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });
        });
    }

    function deliberation_auto()
	{
        var url = window.location.href;

        swal({
            title: "Delibération automatique",
            text: "Êtes-vous sûr d'éffectuer ce traitement ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('delib_auto') }}",
                type: "GET",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },

                success: function(data)
                {
                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Traitement effecuté avec succès !",
                        "success");
                    }

                    $('.load').hide();
                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });
        });
    }

    function check_presence(id)
    {
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&id='+ id;
		$.ajax({
		url : "{{ url('check_presence') }}",
		type: "POST",
		dataType: "JSON",
        cache: false,
        data: dataString,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            if(data.presence == false)
            {
                $('.'+id).html("");
                $('#'+id).removeClass('badge badge-success');
                $('#'+id).addClass("badge badge-danger");
                $('#'+id).html('Non');
            }
            else if(data.presence == true)
            {
                $('#'+id).removeClass('badge badge-danger');
                $('#'+id).addClass("badge badge-success");
                $('#'+id).html('Oui');
                $('.'+id).html(format(data.date_enreg));
            }
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
    }

    //septembre 2020-2021
    function SynchroSeptembre()
	{
        //var dataString = 'session='+ $('[name="session"]').val()+'&specialite='+ $('[name="specialite"]').val();

        swal({
            title: "Synchronisation ?",
            text: "Synchronisation Complète",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('synchronize') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.load').hide();

                    swal(
                        "Synchronisation !",
                        "Synchronisation termin\351ee avec succ\350s.",
                        "success");

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    //ANCIENNE PLATEFORME (COURS ET PARTIEL)
	function SynchroPrcocess1_Licence()
	{
        //var dataString = 'session='+ $('[name="session"]').val()+'&specialite='+ $('[name="specialite"]').val();

        swal({
            title: "Synchronisation ?",
            text: "Synchronisation de la liste des Etudiants + ECUE",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.checked1').hide();
            $('.load').show();

            $.ajax({
                url : "{{ url('synchro_process1_Licence') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.load').hide();
                    $('.checked1').show();
                    $('.date1').html(data.info);

                    swal(
                        "Synchronisation !",
                        "Synchronisation termin\351ee avec succ\350s.\nEtudiant: "+data.nb_etudiant+"\nECUE: "+data.nb_cours,
                        // "Synchronisation termin\351ee avec succ\350s.",
                        "success");

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    function SynchroPrcocess2_Licence()
	{
        swal({
            title: "Synchronisation ?",
            text: "Synchronisation de la liste des note de devoir",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.checked2').hide();
            $('.load').show();

            $.ajax({
                url : "{{ url('synchro_process2_Licence') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.load').hide();
                    $('.checked2').show();
                    $('.date2').html(data.info);

                    swal(
                        "Synchronisation !",
                        "Synchronisation termin\351ee avec succ\350s.\nDevoir: "+data.note,
                        "success");

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    function SynchroPrcocess3_Licence()
	{
        swal({
            title: "Synchronisation ?",
            text: "Synchronisation de la liste des notes d'examen",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.checked3').hide();
            $('.load').show();

            $.ajax({
                url : "{{ url('synchro_process3_Licence') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.load').hide();
                    $('.checked3').show();
                    $('.date3').html(data.info);

                    swal(
                        "Synchronisation !",
                        "Synchronisation termin\351ee avec succ\350s.\nNote de compo: "+data.note,
                        "success");

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    function fusion_Licence()
	{
        swal({
            title: "Fusion de notes",
            text: "Fusionner les notes (devoir + examen), par étudiant et matière",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.checked4').hide();
            $('.load').show();

            $.ajax({
                url : "{{ (session('session_compo') == 'SESSION 1') ? url('fusion1_Licence'): url('fusion2_Licence') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.load').hide();
                    $('.checked4').show();
                    $('.date4').html(data.info);

                    swal(
                        "Fusion des notes !",
                        "Fusion des notes termin\351ee avec succ\350s.",
                        "success");

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    //FIN

    //NOUVELLE PLATEFORME SEMESTRIELLE (COURS)
	function SynchroPrcocess1_Licences()
	{
        swal({
            title: "Synchronisation ?",
            text: "Synchronisation de la liste des Etudiants + ECUE",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.checked1').hide();
            $('.load').show();

            $.ajax({
                url : "{{ url('synchro_process1_Licences') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.load').hide();
                    $('.checked1').show();
                    $('.date1').html(data.info);

                    swal(
                        "Synchronisation !",
                        "Synchronisation termin\351ee avec succ\350s.\nEtudiant: "+data.nb_etudiant+"\nECUE: "+data.nb_cours,
                        // "Synchronisation termin\351ee avec succ\350s.",
                        "success");

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    function SynchroPrcocess2_Licences()
	{
        var dataString = 'specialite='+ $('[name="specialite"]').val();

        if($('[name="specialite"]').val() != "")
        {
            swal({
                title: "Synchronisation ?",
                text: "Synchronisation de la liste des note de devoir",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.checked2').hide();
                $('.load').show();

                $.ajax({
                    url : "{{ url('synchro_process2_Licences') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data)
                    {
                        $('.load').hide();
                        $('.checked2').show();
                        $('.date2').html(data.info);

                        swal(
                            "Synchronisation !",
                            "Synchronisation termin\351ee avec succ\350s.\nDevoir: "+data.note,
                            "success");

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="specialite"]').val() == "")
        {
            swal(
                "Champs obligatoires",
                "Veuillez selectionner une specialité svp",
                "warning");
        }
    }

    function SynchroPrcocess3_Licences()
	{
        var dataString = 'specialite='+ $('[name="specialite"]').val();

        swal({
            title: "Synchronisation ?",
            text: "Synchronisation de la liste des note de devoir",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.checked3').hide();
            $('.load').show();

            $.ajax({
                url : "{{ url('synchro_process3_Licences') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: dataString,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.load').hide();
                    $('.checked3').show();
                    $('.date3').html(data.info);

                    swal(
                        "Synchronisation !",
                        "Synchronisation termin\351ee avec succ\350s.\nDevoir: "+data.note,
                        "success");

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    function fusion_Licences()
	{
        var dataString = 'specialite='+ $('[name="specialite"]').val();

        if($('[name="specialite"]').val() != "")
        {
            swal({
                title: "Fusion de notes",
                text: "Fusionner les notes (devoir + examen), par étudiant et matière",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.checked4').hide();
                $('.load').show();

                $.ajax({
                    url : "{{ url('fusion_Licences') }}",
                    type: "POST",
                    dataType: "JSON",
                    data: dataString,
                    cache: false,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data)
                    {
                        $('.load').hide();
                        $('.checked4').show();
                        $('.date4').html(data.info);

                        swal(
                            "Fusion des notes",
                            "Fusion des notes termin\351ee avec succ\350s.\nLignes enregistr\351ees: "+data.note,
                            "success");

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="specialite"]').val() == "")
        {
            swal(
                "Champs obligatoires",
                "Veuillez selectionner une specialité svp",
                "warning");
        }
    }
    //FIN

    function confirmSynchroL2()
	{
        var dataString = 'session='+ $('[name="session"]').val()+'&specialite='+ $('[name="specialite"]').val();

        if($('[name="session"]').val() != "")
        {
            swal({
                title: "Synchronisation ?",
                text: "Synchronisation des donn\351ees de campus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                //$('#wrapper').css('pointer-events','none');
                //$('.stopper').show();
                $('.lancer').hide();
                $('.load').show();

                $.ajax({
                    url : "{{ url('synchro_licence2') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                            //$('.stopper').hide();
                            $('.load').hide();
                            $('.lancer').show();
                            //$('#wrapper').css('pointer-events','auto');

                        swal(
                            "Synchronisation !",
                            "Synchronisation termin\351ee avec succ\350s.\nEtudiant: "+data.nb_etudiant+"\nECUE: "+data.nb_cours+"\nNotes de devoir: "+data.nb_note,
                            "success");
                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="session"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez selectionner la session svp",
                "warning");
        }
    }

    function confirmSynchroL3()
	{
        var dataString = 'session='+ $('[name="session"]').val()+'&specialite='+ $('[name="specialite"]').val();

        if($('[name="session"]').val() != "")
        {
            swal({
                title: "Synchronisation ?",
                text: "Synchronisation des donn\351ees de campus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                //$('#wrapper').css('pointer-events','none');
                //$('.stopper').show();
                $('.lancer').hide();
                $('.load').show();

                $.ajax({
                    url : "{{ url('synchro_licence3') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                            //$('.stopper').hide();
                            $('.load').hide();
                            $('.lancer').show();
                            //$('#wrapper').css('pointer-events','auto');

                        swal(
                            "Synchronisation !",
                            "Synchronisation termin\351ee avec succ\350s.\nEtudiant: "+data.nb_etudiant+"\nECUE: "+data.nb_cours+"\nNotes de devoir: "+data.nb_note,
                            "success");
                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="session"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez selectionner la session svp",
                "warning");
        }
    }

    function confirmOption()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&etat='+ $('[name="etat"]').val()+'&coef='+ $('[name="coef"]').val();
        //alert(dataString);
        var url = window.location.href;

        if($('[name="etat"]').val() != "")
        {
            swal({
                title: "Traitement...",
                text: "Êtes-vous sûr d'appliquer cette option ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('option_note') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            $('.panel-modal7').modal('hide');

                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }

                        $('.load').hide();

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="etat"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez remplir les champs svp",
                "warning");
        }

    }

    function confirmAnnulation()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&etat='+ $('[name="etat"]').val()+'&coef='+ $('[name="coef"]').val();
        //alert(dataString);
        var url = window.location.href;

        if($('[name="etat"]').val() != "")
        {
            swal({
                title: "Traitement...",
                text: "Êtes-vous sûr d'annuler ce traitement ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('option_annuler') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            $('.panel-modal7').modal('hide');

                            swal(
                            "",
                            "Traitement reinitialisé avec succès !",
                            "success");

                            document.location.href=""+url;
                        }

                        $('.load').hide();
                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="devoir"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez remplir les champs svp",
                "warning");
        }

    }

    function edit_note_old(matiere, etudiant)
	{
		$('#BtnEnregistrer').html(' Modifier ');
        $('.notes_devoirs').html("");
		$('.modal_form')[0].reset(); // reset form on modals
		$('#BtnEnregistrer').attr('value', "modifier");

		//Ajax Load data from ajax
		$.ajax({
		url : "{{ url('get_note/') }}/" + matiere+"/"+etudiant,
		type: "POST",
		dataType: "JSON",
        cache: false,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            $('#title').html(data[0].nom+" "+data[0].prenoms);

            var numbers = data;
            numbers.forEach(myFunction);

            function myFunction(value, index)
            {
                $('.notes_devoirs').append('<div class="form-group row"><label class="col-sm-3 col-form-label">'+value.evaluation+'</label><div class="col-sm-9">'
                                    +'<input type="number" step="0.01" name="note[]" class="form-control" value="'+Math.floor(value.note * 100) / 100+'" required>'
                                    +'<input type="hidden" name="id[]" value="'+value.id_note+'" required>'
                                    +'</div></div>')
            }

			$('.submit').removeClass('disabled').attr('disabled', false);
			$('.panel-modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
	}

    function edit_note(id)
	{
		$('#BtnEnregistrer').html(' Modifier ');
		$('.modal_form')[0].reset(); // reset form on modals
		$('#BtnEnregistrer').attr('value', "modifier");

		//Ajax Load data from ajax
		$.ajax({
		url : "{{ url('get_note/') }}/" + id,
		type: "POST",
		dataType: "JSON",
        cache: false,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            $('#title').html(data.nom+" "+data.prenoms);

            if(data.devoir1 != ""){ $('.form_note1').show(); $('[name="devoir1"]').val(data.devoir1); }else{ $('.form_note1').hide() };
            if(data.devoir2 != ""){ $('.form_note2').show(); $('[name="devoir2"]').val(data.devoir2); }else{ $('.form_note2').hide() };
            if(data.devoir3 != ""){ $('.form_note3').show(); $('[name="devoir3"]').val(data.devoir3); }else{ $('.form_note3').hide() };
            if(data.devoir4 != ""){ $('.form_note4').show(); $('[name="devoir4"]').val(data.devoir4); }else{ $('.form_note4').hide() };
            if(data.devoir5 != ""){ $('.form_note5').show(); $('[name="devoir5"]').val(data.devoir5); }else{ $('.form_note5').hide() };

            $('[name="id1"]').val(data.id_note);

			$('.submit').removeClass('disabled').attr('disabled', false);
			$('.panel-modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
	}

    //pour la partie deliberation
    function edit_notes(id_matiere, id_auto)
	{
        $('.notes_devoirs').html("");
		$('.modal_form3')[0].reset(); // reset form on modals

		//Ajax Load data from ajax
		$.ajax({
		url : "{{ url('get_note_devoir/') }}/" + id_matiere +"/"+id_auto,
		type: "POST",
		dataType: "JSON",
        cache: false,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            $('#title').html(data[0].nom+" "+data[0].prenoms);

            var numbers = data;
            numbers.forEach(myFunction);

            function myFunction(value, index)
            {
                $('.notes_devoirs').append('<div class="form-group row"><label class="col-sm-3 col-form-label">'+value.evaluation+'</label><div class="col-sm-9">'
                                    +'<input type="number" step="0.01" name="note[]" class="form-control" value="'+Math.floor(value.note * 100) / 100+'" required>'
                                    +'<input type="hidden" name="id[]" value="'+value.id_note+'" required>'
                                    +'</div></div>')
            }

			$('.panel-modal3').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
	}

    function add_bonus_devoir(id)
	{
		$('.modal_form5')[0].reset(); // reset form on modals

        if(id == 1)
        $('#title5').html("Ajouter (+) sur le devoir");
        else
        $('#title5').html("Annuler (-) sur le devoir");

        $('[name="id_option"]').val(id);
		$('.panel-modal5').modal('show');
	}

    function confirmBonusDevoir()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&devoir='+ $('[name="devoir"]').val()+'&id='+ $('[name="id_option"]').val()+'&bonus='+ $('[name="bonus_devoir"]').val();
        var url = window.location.href;

        if($('[name="devoir"]').val() != "" && $('[name="bonus_devoir"]').val() !="")
        {
            swal({
                title: "Traitement...",
                text: "Êtes-vous sûr d'appliquer cette option ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();
                $('.panel-modal5').modal('hide');

                $.ajax({
                    url : "{{ url('bonus_devoir') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }

                        $('.load').hide();

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="devoir"]').val() =="" || $('[name="bonus_devoir"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez remplir les champs svp",
                "warning");
        }

    }

    function add_bonus_exam(id)
	{
		$('.modal_form6')[0].reset(); // reset form on modals

        if(id == 1)
        $('#title6').html("Ajouter (+) sur la note d'examen");
        else
        $('#title6').html("Annuler (-) sur la note d'examen");

        $('[name="id_option2"]').val(id);
		$('.panel-modal6').modal('show');
	}

    function confirmBonusExamen()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&id='+ $('[name="id_option2"]').val()+'&bonus='+ $('[name="bonus_examen"]').val();
        var url = window.location.href;

        if($('[name="bonus_examen"]').val() !="")
        {
            swal({
                title: "Traitement...",
                text: "Êtes-vous sûr d'appliquer cette option ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();
                $('.panel-modal6').modal('hide');

                $.ajax({
                    url : "{{ url('bonus_examen') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }

                        $('.load').hide();

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="bonus_examen"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez remplir les champs svp",
                "warning");
        }

    }

    function removeExamen(id) // supprimer une note d'examen
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&id='+id;
        var url = window.location.href;

        if($('[name="matieres"]').val() !="")
        {
            swal({
                title: "Traitement...",
                text: "Êtes-vous sûr d'appliquer cette option ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('delete_examen') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }

                        $('.load').hide();

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="matieres"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez remplir les champs svp",
                "warning");
        }

    }

    function edit_note_examen(id)
	{
		$('#BtnEnregistrer3').html(' Modifier ');
		$('.modal_form3')[0].reset(); // reset form on modals
		$('#BtnEnregistrer3').attr('value', "modifier");

		//Ajax Load data from ajax
		$.ajax({
		url : "{{ url('get_note') }}/" + id,
		type: "POST",
		dataType: "JSON",
        cache: false,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            $('#title3').html(data.nom+" "+data.prenoms);

            $('[name="notes"]').val(""+Math.floor(data.note_examen * 100) / 100);
            $('[name="id3"]').val(data.id_note);

			$('.submit3').removeClass('disabled').attr('disabled', false);
			$('.panel-modal3').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
	}

    function edit_coef(id)
	{
		$('#BtnEnregistrer2').html(' Modifier ');
		$('.modal_form2')[0].reset(); // reset form on modals
		$('#BtnEnregistrer2').attr('value', "modifier");

		//Ajax Load data from ajax
		$.ajax({
		url : "{{ url('get_note/') }}/" + id,
		type: "POST",
		dataType: "JSON",
        cache: false,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            $('#title2').html(data.nom+" "+data.prenoms);

            $('[name="coef"]').val(data.coef);
            $('[name="id2"]').val(data.id_note);

			$('.submit2').removeClass('disabled').attr('disabled', false);
			$('.panel-modal2').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
	}

    function confirmCalculNote()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&taux='+ $('[name="taux"]').val()+'&taux2='+ $('[name="taux2"]').val();
        //alert(dataString);
        var url = window.location.href;

        if($('[name="taux"]').val() !="" && $('[name="taux2"]').val() !="")
        {
            if((parseInt($('[name="taux"]').val()) + parseInt($('[name="taux2"]').val())) == 100)
            {
                swal({
                    title: "Processus de calcul de moyenne...",
                    text: "Êtes-vous sûr d'effectuer ce traitement ?" ,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui",
                    cancelButtonText: "Non",
                    showLoaderOnConfirm:true
                }, function()
                {
                    $('.panel-modal4').modal('hide');
                    $('.load').show();

                    $.ajax({
                        url : "{{ url('processus_devoir') }}",
                        type: "POST",
                        dataType: "JSON",
                        cache: false,
                        data: dataString,
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                        success: function(data){

                            if(data.status == 1)
                            {
                                swal(
                                "",
                                "Traitement effecuté avec succès !",
                                "success");

                                document.location.href=""+url;
                            }

                            $('.load').hide();

                        },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Erreur survenue');
                    }
                    });

                });
            }
            else
            {
                swal(
                    "Taux de pourcentage",
                    "La somme des taux, doit être égal à 100",
                    "warning");
            }
        }
        else if($('[name="taux"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez saisir le taux svp",
                "warning");
        }

    }

    function confirmCalculGlobal()
	{
        var dataString = 'taux='+ $('[name="taux"]').val()+'&taux2='+ $('[name="taux2"]').val();
        //alert(dataString);
        var url = window.location.href;

        if($('[name="taux"]').val() !="" && $('[name="taux2"]').val() !="")
        {
            if((parseInt($('[name="taux"]').val()) + parseInt($('[name="taux2"]').val())) == 100)
            {
                swal({
                    title: "Processus de calcul de moyenne...",
                    text: "Êtes-vous sûr d'effectuer ce traitement ?" ,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui",
                    cancelButtonText: "Non",
                    showLoaderOnConfirm:true
                }, function()
                {
                    $('.panel-modal4').modal('hide');
                    $('.load').show();

                    $.ajax({
                        url : "{{ url('processus_global') }}",
                        type: "POST",
                        dataType: "JSON",
                        cache: false,
                        data: dataString,
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                        success: function(data){

                            if(data.status == 1)
                            {
                                swal(
                                "",
                                "Traitement effecuté avec succès !",
                                "success");

                                //document.location.href=""+url;
                            }

                            $('.load').hide();

                        },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Erreur survenue');
                    }
                    });

                });
            }
            else
            {
                swal(
                    "Taux de pourcentage",
                    "La somme des taux, doit être égal à 100",
                    "warning");
            }
        }
        else if($('[name="taux"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez saisir le taux svp",
                "warning");
        }

    }

    function confirmCalculNoteExamen()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val() +'&specialite='+ $('[name="specialites"]').val() +'&taux='+ $('[name="taux"]').val();
        //alert(dataString);
        var url = window.location.href;

        if($('[name="taux"]').val() !="")
        {
            swal({
                title: "Processus de calcul de moyenne...",
                text: "Êtes-vous sûr d'effectuer ce traitement ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.panel-modal1').modal('hide');
                $('.load').show();

                $.ajax({
                    url : "{{ url('processus_examen') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }
                        else
                        {
                            swal(
                            "",
                            "Veuillez calculer la moyenne des devoirs de cette matiere, avant de lancer ce processus",
                            "warning");
                        }

                        $('.load').hide();

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
        else if($('[name="taux"]').val() =="")
        {
            swal(
                "Champs requis",
                "Veuillez saisir le taux svp",
                "warning");
        }

    }

    function confirmResetNote(matiere, specialite)
	{
        var dataString = 'matiere='+ matiere +'&specialite='+ specialite;

        var url = window.location.href;

        if($('[name="matieres"]').val() !="")
        {
            swal({
                title: "Reinitialisation du processus",
                text: "Êtes-vous sûr d'effectuer ce traitement ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('reset_processus_devoir') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }

                        $('.load').hide();

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
    }

    function confirmResetExamen(matiere, specialite)
	{
        var dataString = 'matiere='+ matiere +'&specialite='+ specialite;

        var url = window.location.href;

        if($('[name="matieres"]').val() !="")
        {
            swal({
                title: "Reinitialisation du processus",
                text: "Êtes-vous sûr d'effectuer ce traitement ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('reset_processus_examen') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data){

                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }

                        $('.load').hide();

                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });

            });
        }
    }

    function show_note_finale()
	{
        var dataString = 'matiere='+ $('[name="matiere"]').val() +'&specialite='+ $('[name="specialite"]').val() +'&annee='+ $('[name="annee"]').val();

        if($('[name="matiere"]').val() !="" && $('[name="annee"]').val() !="")
        {
            $('.load').show();
            $('#contenus').html("");

            $.ajax({
                url : "{{ url('show_note_finale') }}",
                type: "POST",
                dataType: "HTML",
                cache: false,
                data: dataString,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('#contenus').html(data);

                    $('.load').hide();

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });
        }
    }

    function edit_note_finale(id)
	{
		$('.modal_form1')[0].reset(); // reset form on modals

		//Ajax Load data from ajax
		$.ajax({
		url : "{{ url('get_note_finale/') }}/" + id,
		type: "POST",
		dataType: "JSON",
        cache: false,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            $('#title1').html(data.nom+" "+data.prenoms);

            $('[name="mga"]').val(data.mga2);
            $('[name="id_note"]').val(data.id_note);

			$('.panel-modal1').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
	}

    //Repechage
    function show_tableau()
	{
        var dataString = 'moy1='+ $('[name="moy1"]').val() +'&moy2='+ $('[name="moy2"]').val()+'&matiere='+ $('[name="matieres"]').val()+'&specialite='+ $('[name="specialites"]').val()+'&note='+$('[name="note"]').val();

        $('.tableau').html(" ");

        if($('[name="moy1"]').val() !="" && $('[name="moy2"]').val() !="" && $('[name="note"]').val() !="")
        {
            $.ajax({
                url : "{{ url('getTableau') }}",
                type: "POST",
                dataType: "HTML",
                cache: false,
                data: dataString,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data)
                {
                    $('.tableau').html(data);

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });
        }
    }

    function update_mga()
	{
        var dataString = 'moy1='+ $('[name="moy1"]').val() +'&moy2='+ $('[name="moy2"]').val()+'&matiere='+ $('[name="matieres"]').val()+'&specialite='+ $('[name="specialites"]').val()+'&mga='+ $('[name="mga"]').val()+'&note='+$('[name="note"]').val();

        var url = window.location.href;

        if($('[name="moy1"]').val() !="" && $('[name="moy2"]').val() !="" && $('[name="mga"]').val() !="" && $('[name="note"]').val() !="")
        {
            swal({
                title: "Repêchage",
                text: "Êtes-vous sûr d'effectuer ce traitement ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.panel-modal').modal('hide');
                $('.load').show();

                $.ajax({
                    url : "{{ url('update_all_mga') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data)
                    {
                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }
                        $('.load').hide();
                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });
            });
        }
        else
        {
            swal(
                "",
                "Veuillez remplir les champs svp",
                "warning");
        }
    }

    function reset_mga()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val()+'&specialite='+ $('[name="specialites"]').val();

        var url = window.location.href;

        if($('[name="matieres"]').val() !="")
        {
            swal({
                title: "Repêchage",
                text: "Êtes-vous sûr d'effectuer ce traitement ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('reset_all_mga') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data)
                    {
                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }
                        $('.load').hide();
                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });
            });
        }
    }

    function valider_pv()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val()+'&specialite='+ $('[name="specialites"]').val();

        var url = window.location.href;

        if($('[name="matieres"]').val() !="")
        {
            swal({
                title: "Validation du PV",
                text: "Êtes-vous sûr de valider ce pv ? \nSi oui, verifiez d'abord la liste de presence." ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('valider_pv') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data)
                    {
                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }
                        $('.load').hide();
                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });
            });
        }
    }

    function reset_pv()
	{
        var dataString = 'matiere='+ $('[name="matieres"]').val()+'&specialite='+ $('[name="specialites"]').val();

        var url = window.location.href;

        if($('[name="matieres"]').val() !="")
        {
            swal({
                title: "Validation du PV",
                text: "Êtes-vous sûr de reinitialiser ce pv ?" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui",
                cancelButtonText: "Non",
                showLoaderOnConfirm:true
            }, function()
            {
                $('.load').show();

                $.ajax({
                    url : "{{ url('reset_pv') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: dataString,
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    success: function(data)
                    {
                        if(data.status == 1)
                        {
                            swal(
                            "",
                            "Traitement effecuté avec succès !",
                            "success");

                            document.location.href=""+url;
                        }
                        $('.load').hide();
                    },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Erreur survenue');
                }
                });
            });
        }
    }
    //fin repchage

    function validation()
	{
        swal({
            title: "Validation définitive",
            text: "Êtes-vous sûr de valider definitivement cette déliberation ?\n Aucune action ne sera encore possible après la validation. " ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $.ajax({
                url : "{{ url('valider') }}",
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data){

                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Validation effecutée avec succès !",
                        "success");
                    }
                    else if(data.status == 0)
                    {
                        swal(
                        "",
                        "Veuillez valider la liste de présence, des membres du jury, avant d'éffectuer cette action.",
                        "warning");
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    //importation des notes vers la scolarite
    function importation()
	{
        swal({
            title: "Importation des notes",
            text: "Êtes-vous sûr de vouloir faire ce traitement ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('envoyer') }}",
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data){

                    $('.load').hide();

                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Importation réussie.",
                        "success");
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    //Calcul de la moyenne en UE
    function calcul_ue()
	{
        swal({
            title: "Calcul de la moyenne en UE",
            text: "Êtes-vous sûr de vouloir faire ce traitement ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('calcul_ue') }}",
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data){

                    $('.load').hide();

                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Calcul des moyennes en UE réussi.",
                        "success");
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    //Calcul de la moyenne semestrielle
    function calcul_semestrielle()
	{
        swal({
            title: "Calcul de la moyenne semestrielle",
            text: "Êtes-vous sûr de vouloir faire ce traitement ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('calcul_semestrielle') }}",
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data){

                    $('.load').hide();

                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Calcul des moyennes semestrielles réussi.",
                        "success");
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    //Calcul de la moyenne annuelle
    function calcul_annuel()
	{
        swal({
            title: "Calcul de la moyenne annuelle",
            text: "Êtes-vous sûr de vouloir faire ce traitement ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('calcul_annuel') }}",
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data){

                    $('.load').hide();

                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Calcul des moyennes annuelles réussi.",
                        "success");
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    //Calcul de la decision
    function get_decision()
	{
        swal({
            title: "Etablir les décision définitives",
            text: "Êtes-vous sûr de vouloir faire ce traitement ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $('.load').show();

            $.ajax({
                url : "{{ url('get_decision') }}",
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data){

                    $('.load').hide();

                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Décision appliquée avec succès.",
                        "success");
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }


    function edit_user(id)
	{
		$('#BtnEnregistrer').html(' Modifier ');
		$('.modal_form')[0].reset(); // reset form on modals
		$('#BtnEnregistrer').attr('value', "modifier");

		//Ajax Load data from ajax
		$.ajax({
		url : "{{ url('get_user/') }}/" + id,
		type: "POST",
		dataType: "JSON",
        cache: false,
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(data)
		{
            $('[name="id"]').val(data.id_user);
            $('[name="nom"]').val(data.nom); $('[name="prenoms"]').val(data.prenoms);
            $('[name="password"]').val(data.password);
            $('[name="email"]').val(data.email); $('[name="statut"]').val(data.statut);
            $('[name="admin"]').val(data.admin); $('[name="jury"]').val(data.jury);

			$('.submit').removeClass('disabled').attr('disabled', false);
			$('.panel-modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
		    alert('Error get data from ajax');
		}
		});
	}

    function delete_user(id)
	{
        var url = window.location.href;

        swal({
            title: "Suppresion d'utiliasteur",
            text: "Êtes-vous sûr de supprimer cet utilisateur ?" ,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui",
            cancelButtonText: "Non",
            showLoaderOnConfirm:true
        }, function()
        {
            $.ajax({
                url : "{{ url('delete_user/') }}/" + id,
                type: "POST",
                dataType: "JSON",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                success: function(data){

                    if(data.status == 1)
                    {
                        swal(
                        "",
                        "Sppresion effecuté avec succès !",
                        "success");

                        document.location.href=""+url;
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });

        });
    }

    function load_notes(matiere)
	{
        $('#g_notes').find('tr').each (function() {
            var id = $(this).attr('id');
            var dataString = 'matiere='+ matiere +'&user='+ id;

            $.ajax({
                url : "{{ url('get_note_ajax') }}",
                type: "POST",
                cache: false,
                data: dataString,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                dataType: "JSON",
                success: function(data){

                    var numbers = data;
                    numbers.forEach(myFunction);

                    function myFunction(value, index)
                    {
                        $("."+id+"_"+(index+1)).html(""); //on vide la colonne
                        $("."+id+"_"+(index+1)).html(value);
                    }

                },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Erreur survenue');
            }
            });
        });

	}



</script>



