<script>

function selectMatiere(specialite){
	if(specialite!="-1"){
		loadData(specialite);
	}else{
		$("#matiere").html("<option value='-1'>Matiere (ECUE)</option>");
	}
}

function selectMatiereExamen(specialite){
	if(specialite!="-1"){
		loadData2(specialite);
	}else{
		$("#matiere").html("<option value='-1'>Matiere (ECUE)</option>");
	}
}

function selectMatiereExamens(specialite){
	if(specialite!="-1"){
		loadData22(specialite);
	}else{
		$("#matiere").html("<option value='-1'>Matiere (ECUE)</option>");
	}
}

function selectSemestre(niveau){
	if(niveau!="-1"){
		loadData3(niveau);
	}else{
		$("#semestre").html("<option value='-1'>Semestre</option>");
	}
}

function loadData(loadId)
{
	//var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
	var dataString = "loadId="+ loadId;
	//	alert(dataString);
	$("#loader").show();
	$("#loader").fadeIn(400);
	$.ajax({
		type: "POST",
		url: "{{ url('get_niveau') }}",
		data: dataString,
		cache: false,
		headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(result){
			$("#loader").hide();
			$("#matiere").html("<option value='-1'>Matiere (ECUE)</option>");
			$("#matiere").append("<option value=''>--------------</option>");
			$("#matiere").append(result);
		}
	});
}

function loadData2(loadId)
{
	var dataString = "loadId="+ loadId;
	//alert(dataString);
	$("#loader").show();
	$("#loader").fadeIn(400);
	$.ajax({
		type: "POST",
		url: "{{ url('getMatiere') }}",
		data: dataString,
		cache: false,
		headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(result){
			$("#loader").hide();
			$("#matiere").html("<option value='-1'>Matiere (ECUE)</option>");
			$("#matiere").append("<option value=''>--------------</option>");
			$("#matiere").append(result);
		}
	});
}

function loadData22(loadId)
{
	var dataString = "loadId="+ loadId;
	//alert(dataString);
	$("#loader").show();
	$("#loader").fadeIn(400);
	$.ajax({
		type: "POST",
		url: "{{ url('getMatieres') }}",
		data: dataString,
		cache: false,
		headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(result){
			$("#loader").hide();
			$("#matiere").html("<option value='-1'>Matiere (ECUE)</option>");
			$("#matiere").append("<option value=''>--------------</option>");
			$("#matiere").append(result);
		}
	});
}

function loadData3(loadId)
{
	var dataString = "loadId="+ loadId;

	$.ajax({
		type: "POST",
		url: "{{ url('getSemestre') }}",
		data: dataString,
		cache: false,
		headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		success: function(result){
			$("#semestre").html("<option value='-1'>Semestre</option>");
			$("#semestre").append("<option value=''>--------------</option>");
			$("#semestre").append(result);
		}
	});
}


</script>
