    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form" action="{{ url('update_note') }} " method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input class="form-control" name="id1" id="id1" type="hidden">
                    <input class="form-control" name="cof" value="{{ $visible->coef }}" id="cof" type="hidden">

                    @for($k=1; $k<=$visible->coef; $k++)
                    <div class="row form_note{{ $k }}">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Devoir {{ $k }}</label>
                                <div class="col-sm-9">
                                <input type="number" step="0.01" name="devoir{{ $k }}" min="0" max="20" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary submit" id="BtnEnregistrer" name="Enregistrer">Enregistrer</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
