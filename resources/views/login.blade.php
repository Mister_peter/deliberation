@extends('layouts.master-login')

@section('content')
        <!-- <div class="home-btn d-none d-sm-block">
            <a href="index" class="text-dark"><i class="fas fa-home h2"></i></a>
        </div> -->

        <div class="wrapper-page">
            <div class="card overflow-hidden account-card mx-3">
                <div class="bg-uvci p-4 text-white text-center position-relative">
                    <h4 class="font-20 m-b-5">Espace délibération</h4>
                    <p class="text-white-50 mb-4">Se connecter, pour continuer</p>
                    <a href="index" class="logo logo-admin"><img src="{{ asset('assets/images/logo_uvci.png') }}" height="70" alt="logo"></a>
                </div>
                <div class="account-card-content">

                    <form class="form-horizontal m-t-30" action="{{ url('login') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" placeholder="Votre email" required>
                            <small class="help-block" style="color:red">{{ $errors->first('email') }}</small>
                        </div>

                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Votre mot de passe" required>
                            <small class="help-block" style="color:red">{{ $errors->first('password') }}</small>
                        </div>

                        <div class="form-group {{ $errors->has('rentree') ? 'has-error' : '' }}">
                            <select name="rentree" id="rentree" class="form-control" required >
                                <option value="">Rentrée universitaire</option>
                                <option value="">---------------</option>

                                @if(!empty($rentree)) @foreach($rentree as $d)
                                <option value="{{ $d->code_rentree }}"> {{ $d->rentree }} </option>
                                @endforeach @endif
                            </select>
                            <small class="help-block" style="color:red">{{ $errors->first('rentree') }}</small>
                        </div>

                        <div class="form-group {{ $errors->has('niveau') ? 'has-error' : '' }}">
                            <select name="niveau" id="niveau" class="form-control" required onchange="selectSemestre(this.options[this.selectedIndex].value)" >
                                <option value="">Niveau d'étude</option>
                                <option value="">---------------</option>

                                @if(!empty($niveau)) @foreach($niveau as $d)
                                <option value="{{ $d->libelle_niveau }}"> {{ $d->libelle_niveau }} </option>
                                @endforeach @endif
                            </select>
                            <small class="help-block" style="color:red">{{ $errors->first('niveau') }}</small>
                        </div>

                        <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                            <select name="semestre" id="semestre" class="form-control" required >
                                <option value="">Semestre</option>
                            </select>
                            <small class="help-block" style="color:red">{{ $errors->first('semestre') }}</small>
                        </div>

                        <div class="form-group {{ $errors->has('session') ? 'has-error' : '' }}">
                            <select name="session_compo" id="session_compo" class="form-control" required >
                                <option value="">Session d'examen</option>
                                <option value="">---------------</option>
                                <option value="SESSION 1">SESSION NORMALE -- SESSION 1</option>
                                <option value="SESSION 2"> SESSION DE RATTRAPAGE -- SESSION 2</option>
                            </select>
                            <small class="help-block" style="color:red">{{ $errors->first('session') }}</small>
                        </div>


                        <div class="form-group row m-t-20">
                            <div class="col-sm-6">
                                {{-- <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customControlInline">
                                    <label class="custom-control-label" for="customControlInline">Se souvenir</label>
                                </div> --}}
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-uvci w-md waves-effect waves-light" type="submit">Se connecter</button>
                            </div>
                        </div>

                        {{-- <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20">
                                <a href="pages-recoverpw"><i class="mdi mdi-lock"></i> Mot de passe oublié?</a>
                            </div>
                        </div> --}}
                    </form>
                </div>
            </div>

            <div class="m-t-40 text-center">
                <!-- <p>Don't have an account ? <a href="pages-register" class="font-500 text-primary"> Signup now </a> </p> -->
                <p style="color: #fff">Copyright © 2020 UVCI - Déliberation</p>
            </div>

        </div>
        <!-- end wrapper-page -->
@endsection

@include('script.cascade')
@section('script')
@endsection
