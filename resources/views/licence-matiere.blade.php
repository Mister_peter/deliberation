@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/loading.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Liste des matières</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Délibération</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">{{ session('niveau') }}</a></li>
        <li class="breadcrumb-item active">Matières</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5>Recherche</h5>

                    <form action="{{ url('matiere') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="modal-body">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <select name="specialite" class="form-control" required >
                                                <option value="">Spécialité</option>
                                                <option value="">---------------</option>

                                                @if(!empty($specialite)) @foreach($specialite as $d)
                                                <option value="{{ $d->code_specialite }}"> {{ $d->libelle_specialite }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <button type="submit" onclick="" class="btn btn-success btn-icon" title="Rechercher"><i class="fas fa-search"></i></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @if(!empty($matiere))
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4>Spécialité: <font color="green">{{ $spec }} </font> </h4><br>

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            {{-- <th>Catégorie</th> --}}
                            <th>Libellé</th>
                            <th>Spécialité</th>
                            <th>Semestre</th>
                        </tr>
                        </thead>

                        <tbody>

                        @if(!empty($matiere))
                        @foreach($matiere as $d)
                        <tr>
                            <td>{{ $d->code_matiere }}</td>
                            {{-- <td>{{ $d->categorie }}</td> --}}
                            <td>{{ $d->libelle }}</td>
                            <td>{{ $d->specialite_groupe }}</td>
                            <td>{{ $d->semestre }}</td>
                        </tr>
                        @endforeach
                        @endif

                        </tbody>
                    </table>

                    <!-- <button class="float" onclick="" title="Ajouter une matiere"> <i class="ti-plus"></i></button> -->

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    @endif

    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.js')

@endsection

