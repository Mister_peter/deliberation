@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Liste des notes (Devoir + Examen)</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Délibération</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">{{session('niveau')}}</a></li>
        <li class="breadcrumb-item active">Note</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5>Recherche</h5>

                    <form action="{{ url('show_notes') }}" method="get" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="modal-body">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-11">
                                            <select name="specialite" id="specialite" class="form-control" required onchange="selectMatiereExamens(this.options[this.selectedIndex].value)" >
                                                <option value="">Spécialité</option>
                                                <option value="">---------------</option>

                                                @if(!empty($specialite)) @foreach($specialite as $d)
                                                <option value="{{ $d->code_specialite }}"> {{ $d->libelle_specialite }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <img id="loader" src="{{ asset('assets/images/loading.gif') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <select name="matiere" id="matiere" class="form-control" required >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <button type="submit" class="btn btn-success btn-icon" title="Rechercher"><i class="fas fa-search"></i></button>
                                </div>
                            </div>

                        </div>
                      </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @if(!empty($etudiant))
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4><font color="green">{{ $spec }} </font> | ECUE: {{ $matiere->libelle }}</h4><hr>
                    <div class="load"></div>

                    @if($visible->mga_devoir =="")
                    <h5>Processus de calcul de moyenne: <font class="badge badge-danger"> Non Traité </font></h5><hr>

                    <div class="row">
                        <div class="col-sm-5"><h5>Traitement sur les notes de devoirs & examens</h5></div>
                        <div class="col-sm-6">
                            <div class="load"></div>
                            <div class="row">
                                <input name="matieres" value="{{ $matiere->code_matiere }}" type="hidden">
                                <input name="specialites" value="{{ $spec }}" type="hidden">
                                <input name="coef" value="{{ $visible->coef }}" type="hidden">

                                <div class="col-sm-4">
                                    <div class="float-left d-none d-md-block">
                                        <div class="dropdown">
                                            <button class="btn btn-success dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-settings mr-2"></i> Actions - Devoirs
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <button type="button" onclick="delete_devoir()" class="dropdown-item" title="Appliquer le traitement">Supprimer</button>
                                                <button type="button" onclick="reset_delete_devoir()" class="dropdown-item" title="Annuler le traitement">Reinitialiser</button>
                                                <div class="dropdown-divider"></div>
                                                <button type="button" onclick="add_bonus_devoir('1')" class="dropdown-item" title="Ajouter un bonus sur la note">Ajouter (+)</button>
                                                <button type="button" onclick="add_bonus_devoir('2')" class="dropdown-item" title="Soustraire un bonus sur la note" >Soustraire (-)</button>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <button type="button" onclick="confirmOption()" class="btn btn-success btn-icon" title="Appliquer le traitement"> Appliquer</button>&nbsp;&nbsp;
                                    <button type="button" onclick="confirmAnnulation()" class="btn btn-success btn-icon" title="Annuler le traitement"> Reinitialiser</button> --}}
                                </div>
                                <div class="col-sm-4">
                                    <div class="float-left d-none d-md-block">
                                        <div class="dropdown">
                                            <button class="btn btn-success dropdown-toggle arrow-none waves-effect waves-light" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-settings mr-2"></i> Actions - Examens
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <button type="button" onclick="removeExamen('1')" class="dropdown-item" title="Supprimer la note d'examen">Supprimer</button>
                                                <button type="button" onclick="removeExamen('2')" class="dropdown-item" title="Annuler la suppresion de la note">Reinitialiser</button>
                                                <div class="dropdown-divider"></div>
                                                <button type="button" onclick="add_bonus_exam('1')" class="dropdown-item" title="">Ajouter (+)</button>
                                                <button type="button" onclick="add_bonus_exam('2')" class="dropdown-item" title="">Annuler (-)</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <hr>

                    @else
                    <h5>Processus de calucul de moyenne: <font class="badge badge-success"> Traité </font></h5>
                    @endif

                    <div class="row">
                        <div class="col-sm-12">
                        <?php
                            // $taux_compo = ($compo*100)/$nb_etudiant; $taux_compo = round($taux_compo, 2);
                            // $taux_admis = ($admis*100)/$total; $taux_admis = round($taux_admis, 2);
                            // $taux_echec = (($echec)*100)/$total; $taux_echec = round($taux_echec, 2);
                        ?>
                        <p style="font-weight: bold">Etudiant: <font color="blue">{{$nbre_etudiant}} </font> |

                            @for($i=1; $i<=$visible->cf; $i++)
                            <?php
                            $nb = DB::table('delib_note')
                                    ->where('annee_univ', session('annee_univ'))
                                    ->where('niveau', session('niveau'))
                                    ->where('devoir'.$i, '>=', 10)
                                    ->where('specialite', $spec)->where('nom_matiere', $matiere->libelle)
                                    ->count();
                                echo "Devoir ".$i.": <font color='red'>".$nb."</font> admis | ";
                            ?>

                            @endfor
                        </p>
                        </div>
                    </div>
                    <br>

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Nom & prénoms</th>
                            @for($i=1; $i<6; $i++)
                            <th>N° {{ $i }}</th>
                            @endfor
                            <th>Coef</th>
                            <th>Examen</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody class="">

                        @foreach($etudiant as $d)
                        <tr>
                            <td> {{ $d->nom." ".$d->prenoms }}</td>
                            <td>{{ ($d->etat1==0)? ($d->devoir1 != "") ? (floor(($d->devoir1*100))/100) : ""  :  "X" }}</td>
                            <td>{{ ($d->etat2==0)? ($d->devoir2 != "") ? (floor(($d->devoir2*100))/100) : ""  :  "X" }}</td>
                            <td>{{ ($d->etat3==0)? ($d->devoir3 != "") ? (floor(($d->devoir3*100))/100) : ""  :  "X" }}</td>
                            <td>{{ ($d->etat4==0)? ($d->devoir4 != "") ? (floor(($d->devoir4*100))/100) : ""  :  "X" }}</td>
                            <td>{{ ($d->etat5==0)? ($d->devoir5 != "") ? (floor(($d->devoir5*100))/100) : ""  :  "X" }}</td>

                            <td> {{ $d->coef }} </td>
                            <td> {{ ($d->etat_exam==0)? ($d->note_examen != "") ? (floor(($d->note_examen*100))/100) : ""  :  "X" }} </td>
                            <td>

                                <?php
                            $devoir1 =($d->devoir1 != "") ? floor(($d->devoir1*100))/100 : 0;
                            $devoir2 = ($d->devoir2 != "") ? floor(($d->devoir2*100))/100 : 0;
                            $devoir3 = ($d->devoir3 != "") ? floor(($d->devoir3*100))/100 : 0;
                            $devoir4 = ($d->devoir4 != "") ? floor(($d->devoir4*100))/100 : 0;
                            $devoir5 = ($d->devoir5 != "") ? floor(($d->devoir5*100))/100 : 0;
                                ?>

                                @if($visible->mga_devoir =="")
                                @if(intval($devoir1+$devoir2+$devoir3+$devoir4+ $devoir5) > 0 )
                                <button onclick="edit_note('{{ $d->id_note }}')" class="btn btn-primary btn-icon" title="Modifier les notes de devoirs"><i class="mdi mdi-square-edit-outline"></i></button>
                                <button onclick="edit_coef('{{ $d->id_note }}')" class="btn btn-warning btn-icon" title="Modifier le coef"><i class="mdi mdi-square-edit-outline"></i></button>
                                @endif

                                @if($d->etat_exam ==0)
                                <button onclick="edit_note_examen('{{ $d->id_note }}')" class="btn btn-danger btn-icon" title="Modifier la note d'examen"><i class="mdi mdi-square-edit-outline"></i></button>
                                @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <br><br>

                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            @if($visible->mga_devoir =="")
                            <button type="button" onclick="modal_calcul_devoir()" class="btn btn-success btn-icon" title="lancer le traitement"> CALCULER LA MOYENNE (DEVOIRS + EXAMEN)</button>
                            @endif
                        </div>
                        <div class="col-sm-4">
                            @if($visible->mga_devoir !="")
                            @if($pv == 0)
                            <button type="button" onclick="confirmResetNote('{{ $matiere->code_matiere }}', '{{ $spec }}')" class="btn btn-success btn-icon" title="Annuler le traitement"> REINITIALISER LE CALCUL DES MOYENNES</button>
                            @endif
                            @endif
                        </div>
                    </div>

                    <!-- <button class="float" onclick="" title="Ajouter une matiere"> <i class="ti-plus"></i></button> -->

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @endif
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.cascade')

@if(!empty($matiere))
@include('form_note')
@include('form_coef')
@include('form_note_exam')
@include('form_delete_devoir')
@include('form_bonus_devoir')
@include('form_bonus_exam')
@include('form_calcul_note')
@endif

@include('script.js')

@endsection

