@extends('layouts.master')

@section('css')
<!--Chartist Chart CSS -->
<link rel="stylesheet" href="{{ asset('plugins/chartist/css/chartist.min.css') }}">
@endsection

@section('breadcrumb')
<div class="col-sm-6">
     <h4 class="page-title">STATISTIQUES</h4>
     <ol class="breadcrumb">
         <li class="breadcrumb-item active">Statistiques de délibération</li>
     </ol>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title mb-4">Statistique du taux de réussite pour le niveau: {{ session('niveau') }} </h4>

                    <div class="mt-4">
                        <table class="table mb-0">
                            <tbody>

                                <tr>
                                    <td>Niveau</td>
                                    <td>Nbre étudiant</td>
                                    <td>Admis</td>
                                    <td>Refusé</td>
                                    <td>Taux admis</td>
                                    <td>Taux echec</td>
                                </tr>

                                <tr> <!--  1er semestre -->
                                    <?php
                                    $nbr_admis = DB::table('delib_note_finale')
                                                        ->where('niveau', session('niveau'))
                                                        ->where('annee_univ', session('annee_univ'))
                                                        ->where('decision', "ADMIS")->get();

                                    $nbr_echec = DB::table('delib_note_finale')
                                                        ->where('niveau', session('niveau'))
                                                        ->where('annee_univ', session('annee_univ'))
                                                        ->where('decision', "REFUSE")->get();
                                    ?>
                                    <td><span class="badge badge-success">{{ session('niveau') }}</span></td>
                                    <td><strong>{{ $nb_etudiant->count() }} </strong></td>
                                    <td><strong>{{ $nbr_admis->count() }} </strong></td>
                                    <td><strong>{{ $nbr_echec->count() }} </strong></td>
                                    <td class="bg-success"><strong>{{ ($nb_etudiant->count() != 0) ? round((($nbr_admis->count() * 100)/$nb_etudiant->count()), 2) : 0}}%</strong></td>
                                    <td class="bg-danger"><strong>{{ ($nb_etudiant->count() != 0) ? round((($nbr_echec->count() * 100)/$nb_etudiant->count()), 2) : 0}}%</strong></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title mb-4">Statistiques du taux de réussite par spécialité</h4>

                    <div class="mt-4">
                        <table class="table mb-0">
                            <tbody>

                                <tr>
                                    <td>Spécialité</td>
                                    <td>Nbre étudiant</td>
                                    <td>Admis</td>
                                    <td>Refusé</td>
                                    <td>Taux admis</td>
                                    <td>Taux echec</td>
                                </tr>

                                @foreach($spec as $i => $d)
                                <tr>
                                    <?php
                                    $nbr = DB::table('delib_note')
                                                        ->where('niveau', session('niveau'))
                                                        ->where('annee_univ', session('annee_univ'))
                                                        ->where('specialite', $d->code_specialite)
                                                        ->groupBy('id_auto')->get();

                                    $nbr_spec_admis = DB::table('delib_note_finale')
                                                        ->where('niveau', session('niveau'))
                                                        ->where('annee_univ', session('annee_univ'))
                                                        ->where('decision', "ADMIS")
                                                        ->where('specialite', $d->code_specialite)->get();

                                    $nbr_spec_echec = DB::table('delib_note_finale')
                                                        ->where('niveau', session('niveau'))
                                                        ->where('annee_univ', session('annee_univ'))
                                                        ->where('decision', "REFUSE")
                                                        ->where('specialite', $d->code_specialite)->get();
                                    ?>
                                    <td><span class="badge badge-{{ ($i%2==0) ? 'success':'warning'}}">{{ $d->libelle_specialite }}</span></td>
                                    <td><strong>{{ $nbr->count() }} </strong></td>
                                    <td><strong>{{ $nbr_spec_admis->count() }} </strong></td>
                                    <td><strong>{{ $nbr_spec_echec->count() }} </strong></td>
                                    <td><strong>{{ ($nbr->count() != 0) ? round((($nbr_spec_admis->count() * 100)/$nbr->count()), 2) : 0}}%</strong></td>
                                    <td><strong>{{ ($nbr->count() != 0) ? round((($nbr_spec_echec->count() * 100)/$nbr->count()), 2) : 0}}%</strong></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->
@endsection

@section('script')
<!--Chartist Chart-->
<script src="{{ asset('plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ asset('plugins/chartist/js/chartist-plugin-tooltip.min.js') }}"></script>
<!-- peity JS -->
<script src="{{ asset('plugins/peity-chart/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/pages/dashboard.js') }}"></script>
@endsection
