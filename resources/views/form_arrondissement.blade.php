    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0">Formulaire de repêchage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">
                    <input name="matieres" value="{{ $matiere->code_matiere }}" type="hidden">
                    <input name="specialites" value="{{ $spec }}" type="hidden">

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Moyenne (min)</label>
                                <div class="col-sm-6">
                                    <input type="number" onkeyup="show_tableau()" step="0.01" name="moy1" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Moyenne (max)</label>
                                <div class="col-sm-6">
                                    <input type="number" onkeyup="show_tableau()" step="0.01" name="moy2" class="form-control" required>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-7">
                                    <button type="button" onclick="" class="btn btn-success" name="Enregistrer">Voir l'estimation</button>
                                </div>
                            </div> --}}

                            <hr>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Note éliminatoire</label>
                                <div class="col-sm-6">
                                    <input type="number" onkeyup="show_tableau()" step="0.01" name="note" value="5" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Arrondir à</label>
                                <div class="col-sm-6">
                                    <input type="number" step="0.01" name="mga" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 tableau" style="font-weight: bold">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" onclick="update_mga()" class="btn btn-success" name="Enregistrer">Valider</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
