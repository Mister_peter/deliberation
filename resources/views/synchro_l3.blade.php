@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/loading.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Synchronisation des données - Licence 3</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Déliberation</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Licence 3</a></li>
        <li class="breadcrumb-item active">Synchronisation</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <br>
                    <div align="center">
                        <p><font style="color: red; font-weight: bold">La synchronisation prend en compte: </font>
                            <br>- Les étudiants inscrits sur campus
                            <br>- Les matières (ECUE) actives pour chaque spécialité
                            <br>- Les notes de devoirs et examens
                        </p>
                        <br><br>

                        {{-- <div align="center">
                            <div class="col-6">
                                <form method="post" accept-charset="UTF-8">
                                    <div class="form-group">
                                        <select name="specialite" id="specialite" class="form-control" >
                                            <option value="">Spécialité</option>
                                            <option value="">---------------</option>

                                            @if(!empty($specialite))
                                            @foreach($specialite as $d)
                                            <option value="{{ $d->code_specialite }}">{{ $d->libelle_parcours }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="col-6">
                                <form method="post" accept-charset="UTF-8">
                                    <div class="form-group">
                                        <select name="session" id="session" class="form-control" required >
                                            <option value="">Session</option>
                                            <option value="">---------------</option>
                                            <option value="SESSION 1">SESSION NORMALE -- SESSION 1</option>
                                            <option value="SESSION 2"> SESSION DE RATTRAPAGE -- SESSION 2</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div> --}}

                        <br><br>

                        <div class='load'></div>
                        <button type="button" onclick="confirmSynchroL3()" class="btn btn-success lancer" id="BtnEnregistrer" name="Enregistrer">Lancer la synchronisation</button>

                        <!-- <button type="button" class="btn btn-success stopper" id="BtnEnregistrer" name="Enregistrer">Stopper la synchronisation</button> -->
                    </div>


                    <!-- synchro loading -->
                    {{-- <div class='container loading'>
                      <div class='loader'>
                        <div class='loader--dot'></div>
                        <div class='loader--dot'></div>
                        <div class='loader--dot'></div>
                        <div class='loader--dot'></div>
                        <div class='loader--dot'></div>
                        <div class='loader--dot'></div>
                        <div class='loader--text'></div>
                      </div>
                    </div><br><br><br> --}}

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.js')

@endsection

