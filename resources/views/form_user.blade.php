    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="title">Utilisateur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form" action="{{ url('user') }} " method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input class="form-control" name="id" id="id" type="hidden">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nom</label>
                                <div class="col-sm-10">
                                <input type="text" name="nom" class="form-control" spellcheck="false" autocomplete="off" style="text-transform:uppercase" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Prénoms</label>
                                <div class="col-sm-10">
                                <input type="text" name="prenoms" class="form-control" spellcheck="false" autocomplete="off" style="text-transform:uppercase" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" spellcheck="false" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                <input type="password" name="password" min="6" class="form-control" spellcheck="false" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Statut</label>
                                <div class="col-sm-10">
                                    <select name="statut"  class="form-control" required >
                                        <option value="">Faites un choix</option>
                                        <option value="">---------------</option>
                                        <option value="1">Oui</option>
                                        <option value="0">Non</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <p><strong>Droits d'utilisateur</strong></p><hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Admin</label>
                                <div class="col-sm-10">
                                    <select name="admin"  class="form-control" required >
                                        <option value="">Faites un choix</option>
                                        <option value="">---------------</option>
                                        <option value="1">Oui</option>
                                        <option value="0">Non</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Jury</label>
                                <div class="col-sm-10">
                                    <select name="jury"  class="form-control" required >
                                        <option value="">Faites un choix</option>
                                        <option value="">---------------</option>
                                        <option value="1">Oui</option>
                                        <option value="0">Non</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary submit" id="BtnEnregistrer" name="Enregistrer">Enregistrer</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
