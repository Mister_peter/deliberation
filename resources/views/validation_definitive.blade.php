@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/loading.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Validation définitive</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Déliberation</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">{{ session('niveau') }}</a></li>
        <li class="breadcrumb-item active">Validation</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <br>
                    <div align="">
                        <p><font style="color: red; font-weight: bold">VALIDATION DEFINITIVE DE LA DELIBERATION</font>
                            <br>- Après la validation définitive, aucune action ne sera encore possible sur les notes.
                        </p>

                        <br><br>

                        <div class='load'></div>

                        <div class="row">
                            <div class="col-2"><strong>ETAPE 1 </strong></div>
                            <div class="col-10">
                                <button type="button" onclick="validation()" class="btn btn-success" name="Enregistrer">VALIDER DEFINITIVEMENT LA DELIBERATION</button>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-2"><strong>ETAPE 2 </strong></div>
                            <div class="col-10">
                                <button type="button" onclick="importation()" class="btn btn-success" name="Enregistrer">IMPORTATION DES NOTES DE LA DELIBERATION VERS LA SCOLARITE</button>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-2"><strong>ETAPE 3 </strong></div>
                            <div class="col-10">
                                <button type="button" onclick="calcul_ue()" class="btn btn-success" name="Enregistrer">CALCUL DES MOYENNES EN UE</button>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-2"><strong>ETAPE 4 </strong></div>
                            <div class="col-10">
                                <button type="button" onclick="calcul_semestrielle()" class="btn btn-success" name="Enregistrer">CALCUL DES MOYENNES SEMESTRIELLES</button>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-2"><strong>ETAPE 5 </strong></div>
                            <div class="col-10">
                                <button type="button" onclick="calcul_annuel()" class="btn btn-success" name="Enregistrer">CALCUL DES MOYENNES ANNUELLES</button>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-2"><strong>ETAPE 6 </strong></div>
                            <div class="col-10">
                                <button type="button" onclick="get_decision()" class="btn btn-success" name="Enregistrer">ETABLIR LES DECISIONS DEFINITIVES</button>
                            </div>
                        </div> --}}

                        <!-- <button type="button" class="btn btn-success stopper" id="BtnEnregistrer" name="Enregistrer">Stopper la synchronisation</button> -->
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.js')
{{-- @include('form_calcul_note_global') --}}

@endsection

