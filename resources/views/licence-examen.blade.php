@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Liste des notes d'examen</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Déliberation</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Licence 1</a></li>
        <li class="breadcrumb-item active">Note d'examen</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5>Recherche</h5>

                    <form action="{{ url('show_note_examen') }}" method="get" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="modal-body">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-11">
                                            <select name="specialite" id="specialite" class="form-control" required onchange="selectMatiereExamen(this.options[this.selectedIndex].value)" >
                                                <option value="">Spécialité</option>
                                                <option value="">---------------</option>

                                                @if(!empty($specialite)) @foreach($specialite as $d)
                                                <option value="{{ $d->code_specialite }}"> {{ $d->libelle_specialite }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <img id="loader" src="{{ asset('assets/images/loading.gif') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <select name="matiere" id="matiere" class="form-control" required >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <button type="submit" onclick="" class="btn btn-success btn-icon" title="Rechercher"><i class="fas fa-search"></i></button>
                                </div>
                            </div>

                        </div>
                      </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @if(!empty($note))
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4><font color="green">{{ $spec }} </font> | ECUE: {{ $matiere->libelle }}</h4><hr>

                    @if($visible->moy =="")
                    <h5>Processus de calucul de moyenne: <font class="badge badge-danger"> Non Traité </font></h5><br>
                    @else
                    <h5>Processus de calucul de moyenne: <font class="badge badge-success"> Traité </font></h5><br>
                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Nom & prénoms</th>
                            <th>Note</th>
                            <th>Taux</th>
                            <th>Moy</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($note as $d)
                        <tr>
                            <td>{{ $d->nom." ".$d->prenoms }}</td>
                            <td> {{ floor(($d->note*100))/100 }} </td>
                            <td>{{ ($d->taux !="") ? $d->taux."%" : "" }}</td>
                            <td>{{ ($d->moy !="") ? floor(($d->moy*100))/100 : "" }}</td>

                            <td>
                                @if($d->moy =="")
                                <button onclick="edit_note_examen('{{ $d->id_note }}')" class="btn btn-primary btn-icon" title="Modifier la note"><i class="mdi mdi-square-edit-outline"></i></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <br><br>

                    <div class="row">
                        <div class="load"></div>

                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            @if($visible->moy =="")
                            <button type="button" onclick="modal_calcul_examen()" class="btn btn-success btn-icon" title="lancer le traitement"> CALCULER LA MOYENNE D'EXAMEN</button>
                            @endif
                        </div>
                        <div class="col-sm-4">
                            @if($visible->moy !="")
                            @if($pv == 0)
                            <button type="button" onclick="confirmResetExamen('{{ $matiere->libelle }}', '{{ $spec }}')" class="btn btn-success btn-icon" title="Annuler le traitement"> REINITIALISER LE CALCUL DES MOYENNES</button>
                            @endif
                            @endif
                        </div>
                    </div>

                    <!-- <button class="float" onclick="" title="Ajouter une matiere"> <i class="ti-plus"></i></button> -->

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @endif
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.cascade')

@if(!empty($matiere))
@include('form_note_exam')
@include('form_calcul_note_exam')
@endif

@include('script.js')

@endsection

