@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/loading.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Synchronisation des données - {{ session('niveau') }}</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Déliberation</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">{{ ucfirst(strtolower(session('niveau'))) }}</a></li>
        <li class="breadcrumb-item active">Synchronisation</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <br>
                    <div align="center">
                        <p><font style="color: red; font-weight: bold">Synchronisation des données (Plateforme de cours): </font>
                            <br>- Les étudiants inscrits sur campus
                            <br>- Les matières (ECUE) actives sur campus
                            <br>- Les notes de devoirs et examens sur campus
                        </p>
                        <br><br>

                        <div align="center">
                            <div class="col-6">
                                <form method="post" accept-charset="UTF-8">
                                    <div class="form-group">
                                        <select name="specialite" id="specialite" class="form-control">
                                            <option value="">Spécialité</option>
                                            <option value="">---------------</option>

                                            @if(!empty($specialite))
                                            @foreach($specialite as $d)
                                            <option value="{{ $d->code_specialite }}">{{ $d->libelle_parcours }}</option>
                                            @endforeach
                                            @endif
                                            <option value="">--------------------------------</option>
                                            <option value="TOUS">TOUTES LES SPECIALITES</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <br><br>

                        <div class='load'></div>

                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-6">
                                <button type="button" onclick="SynchroPrcocess1_Licences()" class="btn btn-success lancer col-sm-8" id="BtnEnregistrer" name="Enregistrer">LISTE DES ETUDIANTS + MATIERES (ECUE)</button>
                            </div>
                            @if(!empty($etudiant))
                            <div class="col-sm-4 checked1">
                                <img src="{{ asset('assets/images/checked.png') }}" alt="" height="40">&nbsp;&nbsp; <em class="date1">Synchronisé le <?= (!empty($etudiant)) ? date('d-m-Y à H:i:s', strtotime($etudiant->date_created)) : '' ?></em>
                            </div>
                            @endif
                        </div><br>

                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-6">
                                <button type="button" onclick="SynchroPrcocess2_Licences()" class="btn btn-success lancer2 col-sm-8" id="BtnEnregistrer" name="Enregistrer">LISTE DES NOTES (DEVOIR + EXAMEN)</button>
                            </div>
                            @if(!empty($temp))
                            <div class="col-sm-4 checked2">
                                <img src="{{ asset('assets/images/checked.png') }}" alt="" height="40">&nbsp;&nbsp; <em class="date2">Synchronisé le <?= (!empty($temp)) ? date('d-m-Y à H:i:s', strtotime($temp->date_enreg)) : '' ?></em>
                            </div>
                            @endif
                        </div><br>

                        @if(session('session_compo') == "SESSION 1")
                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-6">
                                <button type="button" onclick="SynchroPrcocess3_Licences()" class="btn btn-success lancer2 col-sm-8" id="BtnEnregistrer" name="Enregistrer">TRAITEMENT DES NOTES MAXIMALES DANS UNE EVALUATION</button>
                            </div>
                            @if(!empty($devoir))
                            <div class="col-sm-4 checked3">
                                <img src="{{ asset('assets/images/checked.png') }}" alt="" height="40">&nbsp;&nbsp; <em class="date3">Synchronisé le <?= (!empty($devoir)) ? date('d-m-Y à H:i:s', strtotime($devoir->date_created)) : '' ?></em>
                            </div>
                            @endif
                        </div><br>
                        @endif

                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-6">
                                <button type="button" onclick="fusion_Licences()" class="btn btn-success lancer4 col-sm-8" id="BtnEnregistrer" name="Enregistrer">CALCUL DES NOTES (DEVOIR + EXAMEN)</button>
                            </div>
                            @if(!empty($fusion))
                            <div class="col-sm-4 checked4">
                                <img src="{{ asset('assets/images/checked.png') }}" alt="" height="40">&nbsp;&nbsp; <em class="date4">Fusionné le <?= (!empty($fusion)) ? date('d-m-Y à H:i:s', strtotime($fusion->date_created)) : '' ?></em>
                            </div>
                            @endif
                        </div>
                        <br><br>

                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.js')

@endsection

