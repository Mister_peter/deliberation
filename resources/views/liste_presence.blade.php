    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal2" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="title2"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form2" action="{{ url('validate_presence') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input name="matieres" value="{{ $matiere->libelle }}" type="hidden">
                    <input name="specialites" value="{{ $spec }}" type="hidden">

                    <div class="row">
                        <div class="col-sm-12">
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Nom & prénoms</th>
                                    <th>Présence</th>
                                    <th>Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(!empty($jury))
                                @foreach($jury as $i => $d)
                                <?php
                                $jp = DB::table("delib_presence")->where('id_jury', $d->id_jury)->where('annee_univ', session('annee_univ'))->where('niveau', session('niveau'))
                                                                ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))->first();
                                ?>
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $d->nom_prenoms }}</td>

                                    @if(!empty($jp))
                                    <td>
                                        <span id="{{$d->id_jury}}" class="badge badge-{{ ($jp->presence == 1) ? 'success':'danger' }}">{{ ($jp->presence == 1) ? 'Oui':'Non' }}</span>
                                    </td>
                                    <td class="{{$d->id_jury}}">{{ ($jp->date_enreg != "0000-00-00 00:00:00" && $jp->presence==true) ? date('Y-m-d', strtotime($jp->date_enreg)) : "" }}</td>
                                    <td>
                                        {{-- @if($pv == 0) --}}
                                        @if($jp->presence == 1)
                                        <input type="checkbox" id="switch{{$i}}" switch="none" checked onclick="check_presence('{{ $d->id_jury }}')">
                                        <label for="switch{{$i}}" data-on-label="Oui" data-off-label="Non"></label>
                                        @else
                                        <input type="checkbox" id="switch{{$i}}" switch="none" onclick="check_presence('{{ $d->id_jury }}')">
                                        <label for="switch{{$i}}" data-on-label="Oui" data-off-label="Non"></label>
                                        @endif
                                        {{-- @endif --}}
                                    </td>
                                    @else
                                    <td>
                                        <span id="{{$d->id_jury}}"> </span>
                                    </td>
                                    <td class="{{$d->id_jury}}"></td>
                                    <td>
                                        {{-- @if($pv == 0) --}}
                                        <input type="checkbox" id="switch{{$i}}" switch="none" onclick="check_presence('{{ $d->id_jury }}')">
                                        <label for="switch{{$i}}" data-on-label="Oui" data-off-label="Non"></label>
                                        {{-- @endif --}}
                                    </td>
                                    @endif

                                </tr>
                                @endforeach
                                @endif

                                </tbody>
                            </table>
                            <br><br>
                            <p>
                                <div class="form-group row">
                                    <label class="col-sm-9 col-form-label">
                                        Je soussigné Prof. KOUAME KOFFI FERNAND, DAAP atteste que toutes les personnes susmentionnées ont
                                        participé effectivement à la délibération de ce jour, {{ (!empty($jour)) ? date('d-m-Y', strtotime($jour->date)) : '.../.../.....' }}
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="date" name="date_delib" class="form-control" required>
                                    </div>
                                </div>



                            </p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    {{-- @if($pv == 0) --}}
                    <button type="submit" class="btn btn-success" name="Enregistrer">Valider</button>
                    {{-- @endif --}}
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
