@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Liste des notes de devoirs</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Déliberation</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Licence 1</a></li>
        <li class="breadcrumb-item active">Note de devoir</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5>Recherche</h5>

                    <form action="{{ url('show_note') }}" method="get" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="modal-body">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-11">
                                            <select name="specialite" id="specialite" class="form-control" required onchange="selectMatiere(this.options[this.selectedIndex].value)" >
                                                <option value="">Spécialité</option>
                                                <option value="">---------------</option>

                                                @if(!empty($specialite)) @foreach($specialite as $d)
                                                <option value="{{ $d->code_specialite }}"> {{ $d->libelle_specialite }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <img id="loader" src="{{ asset('assets/images/loading.gif') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <select name="matiere" id="matiere" class="form-control" required >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <button type="submit" onclick="" class="btn btn-success btn-icon" title="Rechercher"><i class="fas fa-search"></i></button>
                                </div>
                            </div>

                        </div>
                      </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @if(!empty($note))
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4><font color="green">{{ $spec }} </font> | ECUE: {{ $matiere->libelle }}</h4><hr>

                    @if($visible->moy1 =="")
                    <h5>Processus de calucul de moyenne: <font class="badge badge-danger"> Non Traité </font></h5><hr>
                    <h5>Traitement sur les notes de devoirs</h5>

                    <form action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="modal-body">
                            {{-- {{ csrf_field() }} --}}

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <div class="col-sm-11">
                                            <input name="matieres" value="{{ $matiere->code_matiere }}" type="hidden">
                                            <input name="specialites" value="{{ $spec }}" type="hidden">

                                            <select name="option"  class="form-control" required >
                                                <option value="">Options</option>
                                                <option value="">---------------</option>

                                                @if(!empty($option)) @foreach($option as $d)
                                                <option value="{{ $d->id }}"> {{ $d->libelle }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <div class="col-sm-11">
                                            <select name="devoir"  class="form-control" required >
                                                <option value="">Devoirs</option>
                                                <option value="">---------------</option>

                                                @if(!empty($devoir)) @foreach($devoir as $d)
                                                <option value="{{ $d->code_devoir }}"> {{ $d->evaluation }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="load"></div>
                                    <button type="button" onclick="confirmOption()" class="btn btn-success btn-icon" title="Appliquer le traitement"> Appliquer</button>&nbsp;&nbsp;
                                    <button type="button" onclick="confirmAnnulation()" class="btn btn-success btn-icon" title="Annuler le traitement"> Reinitialiser</button>
                                </div>
                            </div>

                        </div>
                    </form>
                    @else
                    <h5>Processus de calucul de moyenne: <font class="badge badge-success"> Traité </font></h5>
                    @endif
                    <br>

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <!-- <th>Num BAC</th> -->
                            <th>Nom & prénoms</th>
                            <?php
                            $devv = explode(",",  str_replace('Devoir ', 'N°',  $liste_devoir[0]->evaluation));
                            ksort($devv);
                            ?>

                            @foreach($devv as $devoir)
                            <th>{{ $devoir }}</th>
                            @endforeach

                            <th>Coef</th>
                            <th>Moy /20</th>
                            <th>Taux</th>
                            <th>Moy</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php  $fusion = array(); ?>

                        @foreach($note as $n => $d)
                        <tr>
                            <td>{{ $d->nom." ".$d->prenoms }}</td>

                            <?php
                                $notes = explode(",", $d->note);
                                $dev = explode(",", str_replace(' ', '',  $d->evaluation));

                                $merge= array(array_combine($dev, $notes));
                                ksort($merge);

                                $devoir = explode(",",  str_replace(' ', '',  $liste_devoir[0]->evaluation));
                                ksort($devoir);

                                $nb = count($merge[0]);
                                $row = $nbr->nombre - $nb;
                            ?>

                            @foreach($devoir as $vaa)
                            <td> {{ (array_key_exists($vaa, $merge[0])) ? (floor(($merge[0][$vaa]*100))/100) : "" }} </td>
                            @endforeach

                            <td>{{ $d->coef }}</td>
                            <td>{{ ($d->moy1 !="") ? floor(($d->moy1*100))/100 : "" }}</td>
                            <td>{{ ($d->taux !="") ? $d->taux."%" : "" }}</td>
                            <td>{{ ($d->moy2 !="") ? floor(($d->moy2*100))/100 : "" }}</td>

                            <td>
                                @if($d->moy1 =="")
                                @if($nb < $nbr->nombre)
                                <button onclick="edit_coef('{{ $matiere->code_matiere }}', '{{ $d->code_etudiant }}')" class="btn btn-warning btn-icon" title="Modifier le coef"><i class="mdi mdi-square-edit-outline"></i></button>
                                @endif
                                <button onclick="edit_note('{{ $matiere->code_matiere }}', '{{ $d->code_etudiant }}')" class="btn btn-primary btn-icon" title="Modifier les notes"><i class="mdi mdi-square-edit-outline"></i></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <br><br>

                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            @if($visible->moy1 =="")
                            <button type="button" onclick="modal_calcul_devoir()" class="btn btn-success btn-icon" title="lancer le traitement"> CALCULER LA MOYENNE DES DEVOIRS</button>
                            @endif
                        </div>
                        <div class="col-sm-4">
                            @if($visible->moy1 !="")
                            @if($pv == 0)
                            <button type="button" onclick="confirmResetNote('{{ $matiere->code_matiere }}', '{{ $spec }}')" class="btn btn-success btn-icon" title="Annuler le traitement"> REINITIALISER LE CALCUL DES MOYENNES</button>
                            @endif
                            @endif
                        </div>
                    </div>

                    <!-- <button class="float" onclick="" title="Ajouter une matiere"> <i class="ti-plus"></i></button> -->

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @endif
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.cascade')

@if(!empty($matiere))
@include('form_note')
@include('form_coef')
@include('form_calcul_note')
@endif

@include('script.js')

@endsection

