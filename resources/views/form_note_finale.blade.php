    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal1" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="title1"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form1" action="{{ url('update_mga') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input name="matieres" value="{{ $matiere->libelle }}" type="hidden">
                    <input name="specialites" value="{{ $spec }}" type="hidden">
                    <input name="id_note" type="hidden">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Moyenne</label>
                                <div class="col-sm-6">
                                    <input type="number" step="0.01" name="mga" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success" name="Enregistrer">Valider</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
