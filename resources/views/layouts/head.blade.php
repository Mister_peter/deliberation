<!-- App favicon -->
<link rel="shortcut icon" href="http://scolarite.uvci.edu.ci/assets/images/logo_cobranle.ico">

@yield('css')

 <!-- App css -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/load_wait.css') }}" rel="stylesheet" type="text/css">
