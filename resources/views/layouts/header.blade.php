   <!-- Top Bar Start -->
   <div class="topbar">

<!-- LOGO -->
<div class="topbar-left">
    <a href="index.html" class="logo">
        <span>
                <img src="{{ asset('assets/images/logo_uvci.png') }}" alt="" height="80">
            </span>
        <i>
                <img src="{{ asset('assets/images/logo_uvci.png') }}" alt="" height="50">
            </i>
    </a>
</div>

<nav class="navbar-custom">
    <ul class="navbar-right d-flex list-inline float-right mb-0">

        <li class="dropdown notification-list d-none d-md-block" style="font-weight: bold;">
            <a class="nav-link waves-effect" href="#" >
                {{-- <i class="mdi mdi-fullscreen noti-icon"></i> --}}
                <?php
                    $rentree = DB::table("rentree")->where("code_rentree", session('rentree'))->first();
                ?>
                <font color='black'>{{ $rentree->rentree }}</font> &nbsp; | NIVEAU: <font color='red'>{{ session('niveau') }} </font> - <font color='black'>{{ session('semestre') }}</font> |  ANNEE: <font color='red'>{{ session('annee_univ') }} </font> |  SESSION: <font color='red'>{{ session('session_compo') }} </font>
            </a>
        </li>

        {{-- <li class="dropdown notification-list d-none d-md-block">
            <form role="search" class="app-search">
                <div class="form-group mb-0">
                    <input type="text" class="form-control" placeholder="Recherche..">
                    <button type="button"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </li> --}}

        <!-- language-->
        <li class="dropdown notification-list d-none d-md-block">
            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{ asset('assets/images/flags/ci_flag.png') }}" class="mr-2" height="12" alt=""/> Côte d'Ivoire
            </a>
        </li>

        <!-- full screen -->
        {{-- <li class="dropdown notification-list d-none d-md-block">
            <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                <i class="mdi mdi-fullscreen noti-icon"></i>
            </a>
        </li> --}}

        <li class="dropdown notification-list">
            <div class="dropdown notification-list nav-pro-img">
                <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                     {{ session('user_prenom')." ".session('user_nom') }} &nbsp;&nbsp;
                    <img src="{{ asset('assets/images/users/unnamed.png') }}" alt="user" class="rounded-circle">
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <!-- item-->
                    <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profil</a>
                    <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5"></i> Mettre en veille</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="{{ url('logout') }}"><i class="mdi mdi-power text-danger"></i> Déconnexion</a>
                </div>
            </div>
        </li>

    </ul>

    <ul class="list-inline menu-left mb-0">
        <li class="float-left">
            <button class="button-menu-mobile open-left waves-effect">
                <i class="mdi mdi-menu"></i>
            </button>
        </li>
    </ul>

</nav>

</div>
<!-- Top Bar End -->
