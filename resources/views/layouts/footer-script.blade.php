
<!-- App's Basic Js  -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/js/waves.min.js') }}"></script>
<!-- sweetalert -->
<script src="{{ asset('assets/sweetalert/sweet-alert.js') }}"></script>

 @yield('script')

<!-- App js-->
<script src="{{ asset('assets/js/app.js') }}"></script>

@yield('script-bottom')

<!-- Pour les notification d'avertissement -->
@if($message = Session::get('error'))
<script type="text/javascript">
    $(document).ready(function() {
        swal(
            "",
            "{{ $message }}",
            "warning",
            "OK"
        );
    });
</script>
@endif

@if($message = Session::get('success'))
<script type="text/javascript">
    $(document).ready(function() {
        swal(
            "",
            "{{ $message }}",
            "success"
        );
    });
</script>
@endif

<script type="text/javascript">
    $('#loader').hide(); $('.load').hide();
    $('.stopper').hide(); $('.loading').hide();
    
</script>



