      <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">PLATEFORME DE DELIBERATION</li>
                            <li>
                                <a href="{{ url('dashboard') }}" class="waves-effect">
                                    <i class="ti-home"></i> <span> TABLEAU DE BORD </span>
                                </a>
                            </li>

                            @if(session('user_admin') == 1)
                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i> <span> PARAMETRES <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">

                                    <!--<li><a href="{{ url('septembre') }}">Synchro Septembre</a></li>
                                    <li><a href="{{ url('synchro') }}">Synchronisation</a></li>-->
                                    <li><a href="{{ url('synchros') }}">Synchronisation</a></li>

                                    {{-- @if(session('niveau') == "LICENCE 2")
                                    <li><a href="{{ url('synchro') }}">Synchronisation - L2</a></li>
                                    @endif
                                    @if(session('niveau') == "LICENCE 3")
                                    <li><a href="{{ url('synchro') }}">Synchronisation - L3</a></li>
                                    @endif --}}
                                    @if(session('niveau') == "MASTER 1")
                                    <li><a href="#">Synchro - M1</a></li>
                                    @endif
                                    @if(session('niveau') == "MASTER 2")
                                    <li><a href="#">Synchro - M2</a></li>
                                    @endif
                                    @if(session('niveau') == "DOCTORAT 1")
                                    <li><a href="#">Synchro - D1</a></li>
                                    @endif
                                    @if(session('niveau') == "DOCTORAT 2")
                                    <li><a href="#">Synchro - D2</a></li>
                                    @endif
                                    @if(session('niveau') == "DOCTORAT 3")
                                    <li><a href="#">Synchro - D3</a></li>
                                    @endif
                                    <li><a href="{{ url('global') }}">Calcul moyenne (%)</a></li>
                                    <li><a href="{{ url('delib_global') }}">Delibération automatique</a></li>
                                </ul>
                            </li>
                            @endif

                            <li>
                            <a href="javascript:void(0);" class="waves-effect {{ (isset($menu) && $menu == "niveau")?'mm-active':'' }}"><i class="ti-bookmark-alt"></i> <span> {{session('niveau')}} <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">

                                    @if(session('user_admin') == 1)
                                    <li class="{{ (isset($sous_menu) && $sous_menu == "etudiant")?'mm-active':'' }}"><a href="{{ url('etudiant') }}">Etudiant</a></li>
                                    <li class="{{ (isset($sous_menu) && $sous_menu == "matiere")?'mm-active':'' }}"><a href="{{ url('matiere') }}">Matière</a></li>
                                    {{-- <li class="{{ (isset($sous_menu) && $sous_menu == "devoir")?'mm-active':'' }}"><a href="{{ url('note') }}">Devoir + Examen</a></li> --}}

                                    @endif

                                    @if(session('user_jury') == 1)
                                    {{-- <li class="{{ (isset($sous_menu) && $sous_menu == "devoir")?'mm-active':'' }}"><a href="{{ url('note_devoir') }}">Devoirs + Examens</a></li> --}}
                                    {{-- <li class="{{ (isset($sous_menu) && $sous_menu == "examen")?'mm-active':'' }}"><a href="{{ url('note_examen') }}">Notes d'examens</a></li> --}}
                                    <li class="{{ (isset($sous_menu) && $sous_menu == "finale")?'mm-active':'' }}"><a href="{{ url('note_finale') }}">Notes</a></li>
                                    {{-- <li class="{{ (isset($sous_menu) && $sous_menu == "finale")?'mm-active':'' }}"><a href="{{ url('note_finale2') }}">Devoirs + Examens 2</a></li> --}}
                                    {{-- <li class="{{ (isset($sous_menu) && $sous_menu == "pv_final")?'mm-active':'' }}"><a href="{{ url('pv_final') }}">PV ECUE</a></li> --}}
                                    @endif

                                    <li class="{{ (isset($sous_menu) && $sous_menu == "validation")?'mm-active':'' }}"><a href="{{ url('validation') }}">Validation PV</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-bar-chart"></i> <span> STATISTIQUES <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                    <li><a href="{{ url('statistique') }}">Statistiques</a></li>
                                </ul>
                            </li>

                            @if(session('user_admin') == 1)
                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span> UTILISATEURS <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                    <li><a href="{{ url('user') }}">Utilisateur</a></li>
                                </ul>
                            </li>
                            @endif

                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
