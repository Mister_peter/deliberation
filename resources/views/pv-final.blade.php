@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">PV final</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Déliberation</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">{{session('niveau')}}</a></li>
        <li class="breadcrumb-item active">Pv</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5>Recherche</h5>

                    <form action="{{ url('show_pv_final') }}" method="get" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="modal-body">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-11">
                                            <select name="specialite" id="specialite" class="form-control" required onchange="selectMatiereExamens(this.options[this.selectedIndex].value)" >
                                                <option value="">Spécialité</option>
                                                <option value="">---------------</option>

                                                @if(!empty($specialite)) @foreach($specialite as $d)
                                                <option value="{{ $d->code_specialite }}"> {{ $d->libelle_specialite }} </option>
                                                @endforeach @endif
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <img id="loader" src="{{ asset('assets/images/loading.gif') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <select name="matiere" id="matiere" class="form-control" required >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <button type="submit" class="btn btn-success btn-icon" title="Rechercher"><i class="fas fa-search"></i></button>
                                </div>
                            </div>

                        </div>
                      </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    @if($pv > 0)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4><font color="green">{{ $spec }} </font> | ECUE: {{ $matiere->libelle }}</h4><hr>
                    <div class="load"></div>

                    <div class="row">
                        <div class="col-sm-6">
                        <?php
                            $taux_compo = ($compo*100)/$nb_etudiant; $taux_compo = round($taux_compo, 2);
                            $taux_admis = ($admis*100)/$total; $taux_admis = round($taux_admis, 2);
                            $taux_echec = (($echec)*100)/$total; $taux_echec = round($taux_echec, 2);
                        ?>
                        <p style="font-weight: bold">Etudiant: <font color="blue">{{$nbre_etudiant}} </font> | Admis: <font color="green"> {{$admis." (".$taux_admis."%)"}}</font> | Refusé: <font color="red"> {{ ($echec)." (".$taux_echec."%)" }}</font></p>
                        </div>

                    </div>
                    <br><br>

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Nom & prénoms</th>
                            <th>IdAuto</th>
                            <th>Email</th>
                            <th>Moy</th>
                            <th>Mention</th>
                            <th>Décision</th>
                        </tr>
                        </thead>

                        <tbody class="">

                        @foreach($etudiant as $d)
                        <tr>
                            <td>{{ $d->nom." ".$d->prenoms }}</td>
                            <td>{{ $d->id_auto }}</td>
                            <td>{{ $d->email }}</td>
                            <td> {{ floor(($d->mga2*100))/100 }} </td>
                            <td> {{ $d->mention }}</td>
                            <td> {{ $d->decision }} </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <!-- <button class="float" onclick="" title="Ajouter une matiere"> <i class="ti-plus"></i></button> -->

                </div>
            </div>
        </div> <!-- end col -->
    </div>

    {{-- LISTE DE PRESENCE --}}
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4><font color="black">Liste de présence: membres du jury</font></h4><hr>
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Nom & prénoms</th>
                            <th>Présence</th>
                            <th>Date</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($jury as $i => $d)
                        <?php
                        $jp = DB::table("delib_presence")->where('id_jury', $d->id_jury)->where('annee_univ', session('annee_univ'))->where('niveau', session('niveau'))
                                                        ->where('semestre', session('semestre'))->where('code_rentree', session('rentree'))->first();
                        ?>
                        @if(!empty($jp))
                        <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $d->nom_prenoms }}</td>
                            <td>
                                @if(!empty($jp))
                                <span class="badge badge-{{ ($jp->presence == 1) ? 'success':'danger' }}">{{ ($jp->presence == 1) ? 'Oui':'Non' }}</span>
                                @endif
                            </td>
                            <td class="{{$d->id_jury}}">{{ ($jp->date_enreg != "0000-00-00 00:00:00" && $jp->presence==true) ? date('Y-m-d', strtotime($jp->date_enreg)) : "" }}</td>
                        </tr>
                        @endif
                        @endforeach

                        </tbody>
                    </table>
                    <br><br>
                    <p>
                        Je soussigné <font style="font-weight: bold">Prof. KOUAME KOFFI FERNAND</font>, DAAP atteste que toutes les personnes susmentionnées ont
                        participé effectivement à la délibération du, <font color="red">{{ (!empty($jour)) ? date('d-m-Y', strtotime($jour->date)) : '.../.../.....' }}</font>
                    </p>
                </div>
            </div>
        </div>
    </div>

    @endif
    <!-- end row -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

@include('script.cascade')

@if(!empty($matiere))
@include('form_arrondissement')
@include('form_note_finale')
@endif

@include('script.js')

@endsection

