@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="col-sm-6">
    <h4 class="page-title">Liste des matières</h4>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">Déliberation</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Master 1</a></li>
        <li class="breadcrumb-item active">Matières</li>
    </ol>
</div>                          
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Catégorie</th>
                            <th>Libelle</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                            
                        @foreach($matiere as $d)
                        <tr>
                            <td>{{ $d->code_matiere }}</td>
                            <td>{{ $d->categorie }}</td>
                            <td>{{ $d->libelle }}</td>
                            <td>
                                <button onclick="" class="btn btn-primary btn-icon" title="Modifier"><i class="mdi mdi-square-edit-outline"></i></button>
                                <!-- <button type="button" class="btn btn-primary"><i class="ti plus"></i>8</button> -->
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <button class="float" onclick="" title="Ajouter une matiere"> <i class="ti-plus"></i></button>

                </div>
            </div>
        </div> <!-- end col -->
    </div> 
    <!-- end row -->      
@endsection

@section('script')      
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>  

@include('script.js')
@include('formulaire') 

@endsection

