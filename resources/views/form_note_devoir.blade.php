    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal3" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form3" action="{{ url('update_note') }} " method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input class="form-control" name="id3" id="id3" type="hidden">

                    <div class="row">
                        <div class="col-sm-12 notes_devoirs">

                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
