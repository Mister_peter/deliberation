    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal7" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="title7"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form7" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">DEVOIR</label>
                                <div class="col-sm-9">
                                    <select name="etat"  class="form-control" required >
                                        <option value="">Selectionnez un devoir</option>
                                        <option value="">----------------------</option>
                                        @for($k=1; $k<=$visible->cf; $k++)
                                        <option value="{{ "etat".$k }}"> {{ "Devoir ".$k }} </option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" onclick="" class="btn btn-primary submit7" id="BtnEnregistrer7" name="Enregistrer">Valider</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
