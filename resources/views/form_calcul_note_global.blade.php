    <!--  Modal content for the above example -->
    <div class="modal fade panel-modal4" id="sign-in-modal" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title mt-0">Calcul de moyenne (Devoir + Examen)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

            <form class="modal_form4" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Taux Devoir (%)</label>
                                <div class="col-sm-5">
                                    <input type="number" step="0.01" name="taux" class="form-control" value="40" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Taux Examen(%)</label>
                                <div class="col-sm-5">
                                    <input type="number" step="0.01" name="taux2" class="form-control" value="60" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <font color="red">NB: Le processus prendra quelques minutes, merci de patienter svp...</font>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="button" onclick="confirmCalculGlobal()" class="btn btn-success" name="Enregistrer">Valider</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
